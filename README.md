# Micro Lisp #

This is the small brother (or sister) of my Lisp written in Java. It is still work in progress.

### Using Displays ###

* (setq black (list 0 0 0)) -- Colors are represented by lists of RGB values
* (setq white (list 255 255 255))
* (defdisplay display ili9341 m5stack-esp32-basic-core) -- Now display is the ILI9341 display of a M5Stack ESP32 Basic Core
* (defdisplay display ili9341 odroid-go) -- Now display is the ILI9341 display of an Odroid Go
* (defdisplay display jd79653 m5stack-core-ink) -- Now display is the JD79653 e-paper display of a M5Stack ESP32 Core Ink
* (fill display black) -- The Display represented by display is filled with black color
* (set-pen-color display white) -- Pen color is white
* (set-position display 0 210) -- Pen position is (0, 210)
* (pen-down display) -- If the pen is moved it draws a line
* (pen-down? display) -- Returns true if the pen is down
* (label display "Franz jagt im verwahrlosten" 24) -- Draws a string at the position of the pen; currently only font sizes 12 and 24 are supported
* (pen-up display) -- If the pen is moved it will not draw a line
* (pen-up? display) -- Returns true if the pen is up
* (set-heading display 0) -- Sets the heading of the pen movement in degrees; 0 is along the x axis
* (towards display 160 120) -- Sets the heading of the pen movement to a direction from the current position of the pen towards (160, 120)
* (forward display 40) -- Moves the pen forward 40 pixels in the direction of the heading
* (right display 72) -- Changes the heading 72 degrees to the right (clockwise)
* (back display 40) -- Moves the pen against the direction of the heading 40 pixels
* (left display 90) -- Changes the heading 90 degrees to the left
* (get-bounds display) -- Returns a list with four elements: x origin, y origin, width, height
* (flush display) -- Transfers the drawing to the screen, for e.g. buffered e-paper displays
* (create-display (quote bounds)) -- Returns an in-memory display that does nothing but records the size of the drawing, for use with get-bounds

### Using GPIOs ###

* (defgpio led 2 digital-output) -- Now led represents GPIO 2 configured as digital output
* (write-gpio led t) -- Switches the GPIO represented by led to high 
* (write-gpio led nil) -- Switches the GPIO represented by led to low
* (defgpio sensor 35 analog-input) -- Now sensor represents GPIO 35 configured as analog input
* (read-gpio sensor) -- Returns the measurement of the GPIO represented by sensor as integer value
* (defgpio button 32 digital-input) -- Now button represents GPIO 32 configured as digital input
* (read-gpio button) -- Returns the state of the GPIO represented by button as boolean value (t or nil)

### Using Tasks for Cooperative Multitasking and Scheduling ###

* (now <code>) -- Puts <code> to the front of the task queue for immediate execution
* (after 3000 <code>) -- <code> will be executed after 3 seconds
* (when-then <condition> <code>) -- repeatedly checks <condition> (e.g. reading a GPIO) and when it is true, then execute <code> once
* (when-for-then <condition> 500 <code>) -- when <condition> is continuously true for half a second, then execute <code> once
* (priority-now 2 <code>) -- Enqueues <code> for immediate execution with priority 2; tasks with higher priority are preferred
* (priority-after 10 5000 <code>) -- <code> will be executed after 5 seconds, priority is 10
* (priority-when-then <prio> <condition> <code>) -- like when-then but with priority
* (priority-when-for-then <prio> <condition> <duration> <code>) -- like when-for-then but with priority
* (only-one-of <task1> <task2>) -- only the task that runs first is executed, the other task is canceled; for up to 4 tasks

### Core Language in Examples ###

* t = t
* (bound? (quote t)) = t
* (bound? (quote no-no-no)) = nil
* ((lambda (x) (times x x)) 3) = 9
* ((lambda (x y) (times 2 (times x y))) 5 6) = 60
* ((lambda (x y z) (times 3 (times x (times y z)))) 2 4 5) = 120
* ((lambda (x y) (add (times x 2) (times 3 y))) 9 1) = 21
* ((mlambda x 2) a b c) = 2
* (let ((x 2) (y 3)) (+ x y)) = 5
* (if nil 1 2) = 2
* (if t 1 2) = 1
* (if (equal? 0 0) 1 (quotient 2 0)) = 1
* (if (not (equal? 0 0)) (quotient 2 0) 2) = 2
* ((lambda (x) (prog2 (setq x 1) (setq x (add x 1)))) nil) = 2
* (or (add 3 5) nil) = 8
* (or t (quotient 3 0)) = t
* (or nil (times 1 1)) = 1
* (or nil nil) = nil
* (or nil nil (/ 9 3) nil) = 3
* (and t (+ 2 3) (+ 3 4)) = 7
* (and t (+ 2 3) nil) = nil
* (aif (+ 5 7) it bloop) = 12
* (aif nil bloop (quote ok)) = ok
* (aand (+ 1 2) (+ 3 4) (* 2 it)) = 14
* (catch nil (add 8 13)) = 21
* (add 8 (catch nil 13)) = 21
* (add 8 (catch nil (throw (quote bam) 13))) = 21
* (add (catch nil (throw (quote bam) 13)) 8) = 21
* (add 3 (add (catch nil (throw (quote bam) 13)) 5)) = 21
* (add 8 (catch nil (add 50 (throw (quote bam) 13)))) = 21
* (add 8 (catch nil (add (throw (quote bam) 13) 50))) = 21
* (catch nil (quotient 1.0 0.0)) = "arithmetic error"
* (catch (quote error) (quotient 1 0)) = "arithmetic error"
* (catch (quote crash) (times 4 (throw (quote crash) "crash"))) = "crash"
* (type-of (catch (quote error) (quotient 2 0))) = string
* (equal? (type-of (catch (quote error) (quotient 2 0))) (quote ratio)) = nil
* (equal? nil nil) = t
* (equal? (quote (a b)) nil) = nil
* (equal? (quote (a b)) (quote (a b))) = t
* (equal? (quote (b a)) (quote (a b))) = nil
* (equal? 9 3) = nil
* (equal? (quotient 9 3) 3) = t
* (equal? 13 13) = t
* (equal? -9 4) = nil
* (equal? -80.1 -80.1) = t
* (equal? "ist ein string" "ist ein string") = t
* (equal? "ist ein string" "ist ein anderer string") = nil
* (zero? 4) = nil
* (zero? 0) = t
* (even? 28) = t
* (even? 29) = nil
* (odd? 17) = t
* (odd? 16) = nil
* (less? 9.78 3) = nil
* (less? 3 9.78) = t
* (less? 13 13) = nil
* (less? -9 4) = t
* (less? -80.23 -81.645) = nil
* (less? -5.1 5) = t
* (less? -5 -4.9) = t
* (less? "B" "A") = nil
* (less? "A" "B") = t
* (less? "C" "C") = nil
* (greater? 3 2) = t
* (greater? 3 3) = nil
* (greater? 2 3) = nil
* (greater-or-equal? 3 3) = t
* (greater-or-equal? 2 3) = nil
* (less-or-equal? 3 3) = t
* (less-or-equal? 2 3) = t
* (type-of nil) = list
* (type-of (cons (quote a) (quote (b)))) = list
* (type-of (quote c)) = atom
* (type-of (gensym)) = atom
* (type-of 1) = integer
* (type-of (quotient 1 3)) = ratio
* (type-of 3.14) = float
* (type-of "abc") = string
* (type-of (lambda (x) x)) = lambda
* (list? nil) = t
* (list? (list 1)) = t
* (list? 1) = nil
* (symbol? (quote c)) = t
* (atom? (gensym)) = t
* (atom? 23) = t
* (atom? "23") = t
* (atom? (list 23)) = nil
* (integer? 1) = t
* (number? (quotient 1 3)) = t
* (number? 3.14) = t
* (string? "abc") = t
* (add 3.2 3.9) = 7.1
* (add 4 -3) = 1
* (add 3.9 -2.9) = 1.0
* (add 3 -4) = -1
* (add 2.9 -3.1) = -0.2
* (sub 3 4) = -1
* (sub 3.2 3.9) = -0.7
* (sub 4 -3) = 7
* (sub 3.9 -2.9) = 6.8
* (sub 3 -4) = 7
* (sub 2.9 -3.1) = 6.0
* (times 1.1 2.2) = 2.42
* (times 1.23 -2.34) = -2.8782
* (times -3.21 -4.32) = 13.8672
* (quotient 9 3) = 3
* (quotient 1.21 1.1) = 1.1
* (quotient 13 -2.0) = -6.5
* (quotient -8 4) = -2
* (quotient -81 -9) = 9
* (quotient 4 -2) = -2
* (floor 3) = 3
* (floor (quotient 9 4)) = 2
* (floor 1.57) = 1
* (less? (random 10) 10) = t
* (+) = 0
* (+ 2) = 2
* (+ 8 1) = 9
* (+ 4 1 4) = 9
* (- 1) = -1
* (- 11 1 1 1) = 8
* (*) = 1
* (* 2) = 2
* (* 2 3) = 6
* (* 2 3 4) = 24
* (/ 2) = 0.5
* (/ 2 2) = 1
* (/ 2 2 2) = 0.5
* (abs -2) = 2
* (abs 4) = 4
* (min 2 4 -1 3) = -1
* (max 2 4 -1 3) = 4
* (intern "a") = a
* (char-code "ABC") = 65
* (code-char 66) = "B"
* (concatenate) = ""
* (concatenate "ABC" "DEF") = "ABCDEF"
* (concatenate "Ah" "Be" "Ce" "De") = "AhBeCeDe"
* (substring "abcdef" 3 2) = "cd"
* (string-length "abcd") = 4
* (read-from-string "(a b c)") = (a b c)
* (type-of (first (read-from-string "(a b c)"))) = atom
* (read-from-string "3.1415") = 3.1415
* (type-of (read-from-string "3.1415")) = float
* (write-to-string (quote (a b nil))) = "(a b nil)"
* (write-to-string 1) = "1"
* (write-to-string (/ 2)) = "0.5"
* (write-to-string 3.1415) = "3.1415"
* (write-to-string "juhuu") = "\"juhuu\""
* (min "b" "a") = "a"
* (max "b" "a") = "b"
* (cons (quote a) nil) = (a)
* (cons (quote a) (quote (b))) = (a b)
* (cons (quote a) (cons (quote b) nil)) = (a b)
* (list 1 2 3 4) = (1 2 3 4)
* (append (list 1 2) (list 2 3)) = (1 2 2 3)
* (append (list 1 2) (list 2 3) (list 3 4)) = (1 2 2 3 3 4)
* (first (quote (a b c))) = a
* (rest (quote (a b c))) = (b c)
* (list-length nil) = 0
* (list-length (quote (a b c d))) = 4
* (null? (quote (a))) = nil
* (null? nil) = t
* (single? nil) = nil
* (single? (quote (a))) = t
* (single? (quote (a b))) = nil
* (tail? (list 3 4) (list 1 2 3 4)) = t
* (tail? (list 3 4) (list 1 2 3 5)) = nil
* (first (quote (1 2 3 4 5 6 7 8 9 10))) = 1
* (second (quote (1 2 3 4 5 6 7 8 9 10))) = 2
* (third (quote (1 2 3 4 5 6 7 8 9 10))) = 3
* (fourth (quote (1 2 3 4 5 6 7 8 9 10))) = 4
* (fifth (quote (1 2 3 4 5 6 7 8 9 10))) = 5
* (sixth (quote (1 2 3 4 5 6 7 8 9 10))) = 6
* (seventh (quote (1 2 3 4 5 6 7 8 9 10))) = 7
* (eighth (quote (1 2 3 4 5 6 7 8 9 10))) = 8
* (ninth (quote (1 2 3 4 5 6 7 8 9 10))) = 9
* (tenth (quote (1 2 3 4 5 6 7 8 9 10))) = 10
* (nth (range 1 5) 0) = 1
* (nth (range 1 5) 1) = 2
* (nth (range 1 5) 2) = 3
* (nthrest (list 1 2 3 4) 0) = (1 2 3 4)
* (nthrest (list 1 2 3 4) 1) = (2 3 4)
* (nthrest (list 1 2 3 4) 2) = (3 4)
* (remove 3 (range 1 5)) = (1 2 4)
* (last (list 3 4)) = (4)
* (last (list 1)) = (1)
* (butlast (quote (a b c d))) = (a b c)
* (butlast (quote (a))) = nil
* (range 1 5) = (1 2 3 4)
* (reverse nil) = nil
* (reverse (list 1)) = (1)
* (reverse (range 1 5)) = (4 3 2 1)
* (sort nil less?) = nil
* (sort (list 1) (lambda (x y) (throw (quote bom) (quote bom))))) = (1)
* (catch nil (sort (list 4 2 1 3) (lambda (x y) (throw (quote bom) (quote bom))))) = bom
* (sort (list 7 4 5 2 1 9 8 6 3) less?) = (1 2 3 4 5 6 7 8 9)
* (map-with (lambda (x) (* 2 x)) (quote (1 2 3))) = (2 4 6)
* (zip-with (lambda (x y) (+ x y)) (list 2 3) (list 1 7)) = (3 10)
* (find-if odd? (list 2 4 5 7)) = 5
* (find-if odd? (list 2 4 8 16)) = nil
* (select-if odd? (range 1 10)) = (1 3 5 7 9)
* (remove-if odd? (range 1 10)) = (2 4 6 8)
* (some? even? nil) = nil
* (some? even? (range 1 10)) = 2
* (some? even? (list 1 3 5)) = nil
* (every? even? nil) = t
* (every? even? (range 1 10)) = nil
* (every? odd? (list 1 3 5)) = t
* (member? 4 (list 2 4 5 6)) = (4 5 6)
* (member? 77 (list 2 4 5 6)) = nil
* (member-if? odd? (list 2 4 5 6)) = (5 6)
* (member-if? odd? (list 2 4 6 8)) = nil
* (adjoin 2 (list 3 4 5)) = (2 3 4 5)
* (adjoin 4 (list 3 4 5)) = (3 4 5)
* (union (list 2 3 4) (list 3 4 5)) = (2 3 4 5)
* (intersection (list 2 3 4) (list 3 4 5)) = (3 4)
* (set-difference (list 2 3 4) (list 3 4 5)) = (2)
* (set-exclusive-or (list 2 3 4) (list 3 4 5)) = (2 5)
* (subset? (list 2 3 4) (list 3 4 5)) = nil
* (subset? (list 3 4) (list 3 4 5)) = t
* (disjoint? (list 2 3 4) (list 3 4 5)) = nil
* (disjoint? (list 1 2) (list 5 6)) = t
