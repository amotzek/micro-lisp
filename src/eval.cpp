#include <new>
#include <iostream>
#include "eval.h"
#include "combinator.h"
#include "lambda.h"
#include "macro.h"

namespace lisp {

class PrepareApplyAction : public Action {
    public:
        PrepareApplyAction(List* args, Environment* env) { arguments = Copy(args); environment = Copy(env); }
        virtual ~PrepareApplyAction() { Delete(arguments); Delete(environment); }
        virtual bool Perform(Action* &cstack, List* &sstack);
    private:
        List* arguments;
        Environment* environment;
};

class ApplyCombinatorAction : public Action {
    public:
        ApplyCombinatorAction(Combinator* c, Environment* env) { combinator = Copy(c); environment = Copy(env); }
        virtual ~ApplyCombinatorAction() { Delete(combinator); Delete(environment); }
        virtual bool Perform(Action* &cstack, List* &sstack) { return combinator->Perform(cstack, sstack, environment); }
    private:
        Combinator* combinator;
        Environment* environment;
};

class ApplyLambdaAction : public Action {
    public:
        ApplyLambdaAction(Lambda* l) { lambda = Copy(l); }
        virtual ~ApplyLambdaAction() { Delete(lambda); }
        virtual bool Perform(Action* &cstack, List* &sstack) {
            auto environment = new(std::nothrow) SmallEnvironment(lambda->GetEnvironment());
            if (environment) {
                auto parameters = lambda->GetParameters();
                while (parameters) {
                    auto key = (Symbol*) parameters->GetFirst();
                    parameters = parameters->GetRest();
                    auto value = Pop(sstack);
                    auto success = environment->AddPair(key, value);
                    Delete(value);
                    if (!success) {
                        Delete(environment);
                        return false;
                    }
                }
                auto success = Push(new(std::nothrow) EvalAction(lambda->GetBody(), environment), cstack);
                Delete(environment);
                return success;
            }
            return false;
         }
    private:
        Lambda* lambda;
};

class ApplyMacroAction : public Action {
    public:
        ApplyMacroAction(Environment* e) { environment = Copy(e); }
        virtual ~ApplyMacroAction() { Delete(environment); }
        virtual bool Perform(Action* &cstack, List* &sstack) {
            if (sstack) {
                auto macroexpansion = Pop(sstack);
                auto success = Push(new(std::nothrow) EvalAction(macroexpansion, environment), cstack);
                Delete(macroexpansion);
                return success;
            }
            return false;
        }
    private:
        Environment* environment;
};

bool EvalAction::Perform(Action* &cstack, List* &sstack) {
    if (expression) {
        switch (expression->GetType()) {
            case ExpressionType::LIST: {
                auto list = (List*) expression;
                return Push(new(std::nothrow) PrepareApplyAction(list->GetRest(), environment), cstack)
                    && Push(new(std::nothrow) EvalAction(list->GetFirst(), environment), cstack);
            }
            case ExpressionType::SYMBOL: {
                auto key = (Symbol*) expression;
                Expression* value;
                if (environment->Lookup(key, value)) {
                    return Push(value, sstack);
                }
                // TODO throw not-bound
                std::cout << "not-bound " << key->GetName() << std::endl;
                return false;
            }
            default: {
                return Push(expression, sstack);
            }
        }
    }
    return Push(nullptr, sstack);
}

Action* FindCatch(Symbol* symbol, Action* action) {
    while (action) {
        if (action->Catches(symbol)) {
            return action;
        }
        action = action->GetNext();
    }
    return nullptr;
}

bool ThrowAction::Perform(Action* &cstack, List* &sstack) {
    Action* action = FindCatch(symbol, cstack);
    if (action) {
        Action* next_cstack = action->GetNext();
        List* next_sstack = Copy(((CatchAction*) action)->GetNextSStack());
        if (Push(value, next_sstack)) {
            action->SetNext(nullptr);
            Delete(cstack); // Löscht auch action
            Delete(sstack);
            cstack = next_cstack;
            sstack = next_sstack;
            return true;
        }
    }
    return false;
}

bool PushCombinatorArguments(Combinator* combinator, List* arguments, Environment* environment, Action* &cstack) {
    if (Length(arguments) == combinator->GetParameterCount()) {
        if (Push(new(std::nothrow) ApplyCombinatorAction(combinator, environment), cstack)) {
            Copy(arguments); // Pop ist destruktiv
            for (int argument_index = 0; argument_index < combinator->GetParameterCount(); argument_index++) {
                auto argument = Pop(arguments);
                auto success = (combinator->MustBeQuoted(argument_index)) ? Push(new(std::nothrow) QuoteAction(argument), cstack) : Push(new(std::nothrow) EvalAction(argument, environment), cstack);
                Delete(argument);
                if (!success) {
                    Delete(arguments);
                    return false;
                }
            }
            return true;
        }
    }
    // TODO else throw wrong-argument-count
    std::cout << "wrong-argument-count" << std::endl;
    return false;
}

bool PushLambdaArguments(Lambda* lambda, List* arguments, Environment* environment, Action* &cstack) {
    if (Length(arguments) == lambda->GetParameterCount()) {
        if (Push(new(std::nothrow) ApplyLambdaAction(lambda), cstack)) {
            Copy(arguments); // Pop ist destruktiv
            for (int argument_index = 0; argument_index < lambda->GetParameterCount(); argument_index++) {
                auto argument = Pop(arguments);
                auto success = Push(new(std::nothrow) EvalAction(argument, environment), cstack);
                Delete(argument);
                if (!success) {
                    Delete(arguments);
                    return false;
                }
            }
            return true;
        }
    }
    // TODO else throw wrong-argument-count
    std::cout << "wrong-argument-count" << std::endl;
    return false;
}

bool PushMacroArgument(Macro* macro, List* arguments, Environment* environment, Action* &cstack) {
    auto parent = macro->GetEnvironment();
    auto child = new(std::nothrow) SmallEnvironment(parent);
    bool success = false;
    if (child) {
        if (child->AddPair(macro->GetParameter(), arguments)) {
            success = Push(new(std::nothrow) ApplyMacroAction(environment), cstack) && Push(new(std::nothrow) EvalAction(macro->GetBody(), child), cstack);
        }
        Delete(child);
    }
    return success;
}

bool PrepareApplyAction::Perform(Action* &cstack, List* &sstack) {
    if (sstack) {
        auto function = Pop(sstack);
        if (function) {
            switch (function->GetType()) {
                case ExpressionType::COMBINATOR: {
                    auto success = PushCombinatorArguments((Combinator*) function, arguments, environment, cstack);
                    Delete(function);
                    return success;
                }
                case ExpressionType::LAMBDA: {
                    auto success = PushLambdaArguments((Lambda*) function, arguments, environment, cstack);
                    Delete(function);
                    return success;
                }
                case ExpressionType::MACRO: {
                    auto success = PushMacroArgument((Macro*) function, arguments, environment, cstack);
                    Delete(function);
                    return success;
                }
                default: {
                    // TODO throw not-a-function
                    std::cout << "not-a-function" << std::endl;
                    Delete(function);
                    return false;
                }
            }
        }
        // TODO throw not-a-function
        return false;
    }
    return false;
}

bool InnerEval(Expression* expression, Environment* environment, Expression* &value, ThrowAction* &throw_action) {
    Action* cstack = new(std::nothrow) EvalAction(expression, environment);
    List* sstack = nullptr;
    while (cstack) {
        auto action = Pop(cstack);
        if (action->Perform(cstack, sstack)) {
            Delete(action);
            continue;
        }
        if (action->Throws()) {
            throw_action = (ThrowAction*) action;
        } else {
            Delete(action);
        }
        Delete(cstack);
        Delete(sstack);
        return false;
    }
    if (sstack) {
        value = Pop(sstack);
        Delete(sstack);
        return true;
    }
    return false;
}

bool Eval(Expression* expression, Environment* environment, Expression* &value) {
    ThrowAction* throw_action = nullptr;
    auto success = InnerEval(expression, environment, value, throw_action);
    Delete(throw_action);
    return success;
}

}
