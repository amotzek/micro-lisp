#include <new>
#include <string>
#include "combinator.h"
#include "abstraction_combinator.h"
#include "control_combinator.h"
#include "list_combinator.h"
#include "arithmetic_combinator.h"
#include "string_combinator.h"
#include "comparison_combinator.h"
#include "task_combinator.h"
#include "file_combinator.h"
#include "display_combinator.h"
#include "gpio_combinator.h"
#include "wlan_combinator.h"
#include "socket_combinator.h"
#include "broker_combinator.h"
#include "core.h"
#include "loader.h"

namespace lisp {

void AddCombinator(Environment* &environment, const std::string &name, Combinator* value) {
    if (environment) {
        bool success = false;
        if (value) {
            auto key = Intern(name);
            if (key) {
                success = environment->AddPair(key, value);
                Delete(key); // Intern und AddPair kopieren
            }
            Delete(value); // AddPair kopiert
        }
        if (!success) {
            Delete(environment);
            environment = nullptr;
        }
    }
}

static Environment* root = nullptr;

Environment* GetRootEnvironment() {
    if (!root) {
        root = new(std::nothrow) LargeEnvironment(nullptr);
        AddCombinator(root, "quote", new(std::nothrow) Quote1());
        AddCombinator(root, "setq", new(std::nothrow) Setq2());
        AddCombinator(root, "unbind", new(std::nothrow) Unbind1());
        AddCombinator(root, "bound?", new(std::nothrow) IsBound1());
        AddCombinator(root, "lambda", new(std::nothrow) Lambda2());
        AddCombinator(root, "mlambda", new(std::nothrow) Macro2());
        AddCombinator(root, "not", new(std::nothrow) IsNull1());
        AddCombinator(root, "if", new(std::nothrow) If3());
        AddCombinator(root, "or2", new(std::nothrow) Or2());
        AddCombinator(root, "prog2", new(std::nothrow) Prog2());
        AddCombinator(root, "catch", new(std::nothrow) Catch2());
        AddCombinator(root, "throw", new(std::nothrow) Throw2());
        AddCombinator(root, "type-of", new(std::nothrow) TypeOf1());
        AddCombinator(root, "gensym", new(std::nothrow) GenSym0());
        AddCombinator(root, "cons", new(std::nothrow) Cons2());
        AddCombinator(root, "first", new(std::nothrow) First1());
        AddCombinator(root, "rest", new(std::nothrow) Rest1());
        AddCombinator(root, "null?", new(std::nothrow) IsNull1());
        AddCombinator(root, "list-length", new(std::nothrow) ListLength1());
        AddCombinator(root, "reverse", new(std::nothrow) Reverse1());
        AddCombinator(root, "sort", new(std::nothrow) Sort2());
        AddCombinator(root, "add", new(std::nothrow) Add2());
        AddCombinator(root, "sub", new(std::nothrow) Sub2());
        AddCombinator(root, "times", new(std::nothrow) Times2());
        AddCombinator(root, "quotient", new(std::nothrow) Quotient2());
        AddCombinator(root, "floor", new(std::nothrow) Floor1());
        AddCombinator(root, "random", new(std::nothrow) Random1());
        AddCombinator(root, "intern", new(std::nothrow) Intern1());
        AddCombinator(root, "char-code", new(std::nothrow) CharCode1());
        AddCombinator(root, "code-char", new(std::nothrow) CodeChar1());
        AddCombinator(root, "concatenate2", new(std::nothrow) Concatenate2());
        AddCombinator(root, "substring", new(std::nothrow) Substring3());
        AddCombinator(root, "string-length", new(std::nothrow) StringLength1());
        AddCombinator(root, "read-from-string", new(std::nothrow) ReadFromString1());
        AddCombinator(root, "write-to-string", new(std::nothrow) WriteToString1());
        AddCombinator(root, "parse-json", new(std::nothrow) ParseJson1());
        AddCombinator(root, "equal?", new(std::nothrow) IsEqual2());
        AddCombinator(root, "less?", new(std::nothrow) IsLess2());
        AddCombinator(root, "get-monotonic-time", new(std::nothrow) GetMonotonicTime0());
        AddCombinator(root, "now", new(std::nothrow) Now1());
        AddCombinator(root, "after", new(std::nothrow) After2());
        AddCombinator(root, "when-then", new(std::nothrow) WhenThen2());
        AddCombinator(root, "when-for-then", new(std::nothrow) WhenForThen3());
        AddCombinator(root, "priority-now", new(std::nothrow) PriorityNow2());
        AddCombinator(root, "priority-after", new(std::nothrow) PriorityAfter3());
        AddCombinator(root, "priority-when-then", new(std::nothrow) PriorityWhenThen3());
        AddCombinator(root, "priority-when-for-then", new(std::nothrow) PriorityWhenForThen4());
        AddCombinator(root, "only-one-of-list", new(std::nothrow) OnlyOneOfList1());
        AddCombinator(root, "run-tasks", new(std::nothrow) RunTasks1());
        AddCombinator(root, "drain-tasks", new(std::nothrow) DrainTasks0());
        AddCombinator(root, "open", new(std::nothrow) Open1());
        AddCombinator(root, "write-char", new(std::nothrow) WriteChar2());
        AddCombinator(root, "close", new(std::nothrow) Close1());
        AddCombinator(root, "load", new(std::nothrow) Load1());
        AddCombinator(root, "delete-file", new(std::nothrow) DeleteFile1());
        AddCombinator(root, "create-display", new(std::nothrow) CreateDisplay2());
        AddCombinator(root, "get-bounds", new(std::nothrow) GetBounds1());
        AddCombinator(root, "fill", new(std::nothrow) Fill2());
        AddCombinator(root, "get-pixel", new(std::nothrow) GetPixel3());
        AddCombinator(root, "set-pixel", new(std::nothrow) SetPixel4());
        AddCombinator(root, "pen-up?", new(std::nothrow) IsPenUp1());
        AddCombinator(root, "pen-down?", new(std::nothrow) IsPenDown1());
        AddCombinator(root, "pen-up", new(std::nothrow) PenUp1());
        AddCombinator(root, "pen-down", new(std::nothrow) PenDown1());
        AddCombinator(root, "set-pen-color", new(std::nothrow) SetPenColor2());
        AddCombinator(root, "set-heading", new(std::nothrow) SetHeading2());
        AddCombinator(root, "towards", new(std::nothrow) Towards3());
        AddCombinator(root, "left", new(std::nothrow) Left2());
        AddCombinator(root, "right", new(std::nothrow) Right2());
        AddCombinator(root, "set-position", new(std::nothrow) SetPosition3());
        AddCombinator(root, "forward", new(std::nothrow) Forward2());
        AddCombinator(root, "back", new(std::nothrow) Back2());
        AddCombinator(root, "label", new(std::nothrow) Label3());
        AddCombinator(root, "flush", new(std::nothrow) Flush1());
        AddCombinator(root, "create-gpio", new(std::nothrow) CreateGpio2());
        AddCombinator(root, "read-gpio", new(std::nothrow) ReadGpio1());
        AddCombinator(root, "write-gpio", new(std::nothrow) WriteGpio2());
        AddCombinator(root, "create-wlan", new(std::nothrow) CreateWlan2());
        AddCombinator(root, "connect-wlan", new(std::nothrow) ConnectWlan1());
        AddCombinator(root, "connected-wlan?", new(std::nothrow) IsConnectedWlan1());
        AddCombinator(root, "get-env", new(std::nothrow) GetEnv1());
        AddCombinator(root, "create-socket", new(std::nothrow) CreateSocket2());
        AddCombinator(root, "create-broker", new(std::nothrow) CreateBroker4());
        AddCombinator(root, "connected-broker?", new(std::nothrow) IsConnectedBroker1());
        AddCombinator(root, "publish", new(std::nothrow) Publish4());
        AddCombinator(root, "subscribe", new(std::nothrow) Subscribe4());
        AddCombinator(root, "unsubscribe", new(std::nothrow) Unsubscribe2());
        AddCombinator(root, "ping", new(std::nothrow) Ping1());
        auto core = CreateCore();
        Load(core, root);
        delete core;
    }
    return root;
}

}
