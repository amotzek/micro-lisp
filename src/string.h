#ifndef STRING_H
#define STRING_H
#include <string>
#include "expression.h"

namespace lisp {

class String : public Expression {
    public:
        String(const std::string &v) { value = v; }
        virtual ~String() { }
        virtual ExpressionType GetType() { return ExpressionType::STRING; }
        std::string GetString() { return value; }
    private:
        std::string value;
};

}

#endif
