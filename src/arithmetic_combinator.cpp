#include <cmath>
#include "arithmetic_combinator.h"
#include "rational.h"
#include "float.h"
#include "string.h"

namespace lisp {

std::size_t BitLength(long value) {
    if (value < 0) {
        return BitLength(-value);
    }
    std::size_t length = 0;
    while (value > 0) {
        length++;
        value >>= 1;
    }
    return length;
}

bool IsSmallBitLength(std::size_t length) {
    return length < 8 * sizeof(long);
}

bool HasSmallNumeratorAndDenominator(Expression* v1, Expression* v2) {
    if (IsRational(v1) && IsRational(v2)) {
         auto r1 = (Rational*) v1;
         auto r2 = (Rational*) v2;
         auto bln1 = BitLength(r1->GetNumerator());
         auto bln2 = BitLength(r2->GetNumerator());
         auto bld1 = BitLength(r1->GetDenominator());
         auto bld2 = BitLength(r2->GetDenominator());
         return IsSmallBitLength(bln1 + bld2) && IsSmallBitLength(bln2 + bld1) && IsSmallBitLength(bld1 + bld2);
    }
    return false;
}

bool PushThrow(Action* &cstack) {
    auto error = Intern("error");
    auto message = new(std::nothrow) String("arithmetic error");
    auto success = Push(new(std::nothrow) ThrowAction(error, message), cstack);
    Delete(error);
    Delete(message);
    return success;
}

bool ArithmeticCombinator::ProcessValues(Expression* v1, Expression* v2, Action* &cstack, List* &sstack, Environment* env) {
    if (v1 && v2 && IsNumber(v1) && IsNumber(v2)) {
        if (HasSmallNumeratorAndDenominator(v1, v2)) {
            auto r1 = (Rational*) v1;
            auto r2 = (Rational*) v2;
            auto n3 = Operate(r1->GetNumerator(), r1->GetDenominator(), r2->GetNumerator(), r2->GetDenominator());
            if (n3) {
                if (Push(n3, sstack)) {
                    Delete(n3); // Push kopiert
                    return true;
                }
                Delete(n3);
            } else {
                return PushThrow(cstack);
            }
            return false;
        }
        auto n1 = (Number*) v1;
        auto n2 = (Number*) v2;
        auto n3 = Operate(n1->GetDouble(), n2->GetDouble());
        if (n3) {
            if (Push(n3, sstack)) {
                Delete(n3); // Push kopiert
                return true;
            }
            Delete(n3);
        } else {
            return PushThrow(cstack);
        }
    }
    return false;
}

Number* Add2::Operate(long n1, long d1, long n2, long d2) {
    return new(std::nothrow) Rational(n1 * d2 + n2 * d1, d1 * d2);
}

Number* Add2::Operate(double d1, double d2) {
    return new(std::nothrow) Float(d1 + d2);
}

Number* Sub2::Operate(long n1, long d1, long n2, long d2) {
    return new(std::nothrow) Rational(n1 * d2 - n2 * d1, d1 * d2);
}

Number* Sub2::Operate(double d1, double d2) {
    return new(std::nothrow) Float(d1 - d2);
}

Number* Times2::Operate(long n1, long d1, long n2, long d2) {
    return new(std::nothrow) Rational(n1 * n2, d1 * d2);
}

Number* Times2::Operate(double d1, double d2) {
    return new(std::nothrow) Float(d1 * d2);
}

Number* Quotient2::Operate(long n1, long d1, long n2, long d2) {
    if (n2 == 0l) {
        return nullptr;
    }
    return new(std::nothrow) Rational(n1 * d2, n2 * d1);
}

Number* Quotient2::Operate(double d1, double d2) {
    if (d2 == 0.0d) {
        return nullptr;
    }
    return new(std::nothrow) Float(d1 / d2);
}

bool Floor1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(expr)) {
        return Push(expr, sstack);
    }
    if (IsRational(expr)) {
        auto r1 = (Rational*) expr;
        auto q = r1->GetNumerator() / r1->GetDenominator();
        auto r2 = new(std::nothrow) Rational(q, 1l);
        auto success = Push(r2, sstack);
        Delete(r2);
        return success;
    }
    if (IsFloat(expr)) {
        auto f = (Float*) expr;
        auto d = floor(f->GetDouble());
        auto r = new(std::nothrow) Rational((long) d, 1l);
        auto success = Push(r, sstack);
        Delete(r);
        return success;
    }
    return false;
}

bool Random1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(expr)) {
        auto r1 = (Rational*) expr;
        auto n1 = r1->GetNumerator();
        if (n1 > 0) {
            std::uniform_int_distribution<long> distribution(0, n1 - 1);
            long n2 = distribution(generator);
            auto r2 = new(std::nothrow) Rational(n2, 1l);
            auto success = Push(r2, sstack);
            Delete(r2);
            return success;
        }
    }
    return false;
}

}
