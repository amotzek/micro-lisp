#ifndef COMBINATOR_H
#define COMBINATOR_H
#include "action.h"
#include "environment.h"

namespace lisp {

class Combinator : public Expression {
    public:
        Combinator() { }
        virtual ~Combinator() { }
        virtual ExpressionType GetType() { return ExpressionType::COMBINATOR; }
        virtual int GetParameterCount() = 0;
        virtual bool MustBeQuoted(int argument_index) = 0;
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env) = 0;
};

Environment* GetRootEnvironment();

}

#endif
