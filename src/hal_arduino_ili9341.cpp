#ifdef ARDUINO
#include <list>
#include <Arduino.h>
#include <SPI.h>
#include "ili9341.h"

namespace ili9341 {

void WriteCommand(int cs, int dc, byte command, std::list<byte> arguments = { }) {
    digitalWrite(dc, LOW);
    digitalWrite(cs, LOW);
    SPI.transfer(command);
    digitalWrite(cs, HIGH);
    if (arguments.size() > 0) {
        digitalWrite(dc, HIGH);
        digitalWrite(cs, LOW);
        for (byte data : arguments) {
            SPI.transfer(data);
        }
        digitalWrite(cs, HIGH);
    }
}

void ILI9341::Init() {
    SPI.begin(sck, miso, mosi); // siehe https://github.com/espressif/arduino-esp32/blob/master/libraries/SPI/src/SPI.h
    pinMode(cs, OUTPUT);
    pinMode(dc, OUTPUT);
    pinMode(bl, OUTPUT);
    // Reset
    if (rst >= 0) {
        pinMode(rst, OUTPUT);
        digitalWrite(cs, HIGH);
        digitalWrite(dc, LOW);
        digitalWrite(rst, LOW);
        delay(50);
        digitalWrite(rst, HIGH);
        delay(50);
    } else {
        WriteCommand(cs, dc, 0x01); // Software Reset
        delay(10);
    }
    // Setup
    WriteCommand(cs, dc, 0xef, {0x03, 0x80, 0x02});
    WriteCommand(cs, dc, 0xcf, {0x00, 0xc1, 0x30});
    WriteCommand(cs, dc, 0xed, {0x64, 0x03, 0x12, 0x81});
    WriteCommand(cs, dc, 0xe8, {0x85, 0x00, 0x78});
    WriteCommand(cs, dc, 0xcb, {0x39, 0x2c, 0x00, 0x34, 0x02});
    WriteCommand(cs, dc, 0xf7, {0x20});
    WriteCommand(cs, dc, 0xea, {0x00, 0x00});
    WriteCommand(cs, dc, 0xc0, {0x23}); // Power Control 1, VRH[5:0]
    WriteCommand(cs, dc, 0xc1, {0x10}); // Power Control 2, SAP[2:0], BT[3:0]
    WriteCommand(cs, dc, 0xc5, {0x3e, 0x28}); // # VCM Control 1
    WriteCommand(cs, dc, 0xc7, {0x86}); // VCM Control 2
    WriteCommand(cs, dc, 0x36, {0x48}); // Memory Access Control
    WriteCommand(cs, dc, 0x3a, {0x55}); // Pixel Format
    WriteCommand(cs, dc, 0xb1, {0x00, 0x18}); // FRMCTR1
    WriteCommand(cs, dc, 0xb6, {0x08, 0x82, 0x27}); // Display Function Control
    WriteCommand(cs, dc, 0xf2, {0x00}); // Gamma Function Disable
    WriteCommand(cs, dc, 0x26, {0x01}); // Gamma Curve Selected
    WriteCommand(cs, dc, 0xe0, {0x0f, 0x31, 0x2b, 0x0c, 0x0e, 0x08, 0x4e, 0xf1, 0x37, 0x07, 0x10, 0x03, 0x0e, 0x09, 0x00}); // Set Gamma
    WriteCommand(cs, dc, 0xe1, {0x00, 0x0e, 0x14, 0x03, 0x11, 0x07, 0x31, 0xc1, 0x48, 0x08, 0x0f, 0x0c, 0x31, 0x36, 0x0f});
    // Invert
    if (inverted) {
        WriteCommand(cs, dc, 0x21);
    }
    // Wake
    WriteCommand(cs, dc, 0x11);
    delay(120);
    // On
    WriteCommand(cs, dc, 0x29);
    // Back Light
    digitalWrite(bl, HIGH);
}

int Color565(int r, int g, int b) {
    return (r & 0xf8) << 8 | (g & 0xfc) << 3 | (b & 0xff) >> 3;
}

bool ILI9341::SetPixel(int x, int y, int r, int g, int b) {
    if (IsInside(x, y)) {
        if (mirror_axis) {
            x = GetWidth() - x - 1;
            y = GetHeight() - y - 1;
        }
        if (mirror_diagonal) {
            int s = x;
            x = y;
            y = s;
        }
        int color = Color565(r, g, b);
        byte x_msb = x >> 8;
        byte x_lsb = x & 255;
        byte y_msb = y >> 8;
        byte y_lsb = y & 255;
        byte color_msb = color >> 8;
        byte color_lsb = color & 255;
        WriteCommand(cs, dc, 0x2a, {x_msb, x_lsb, x_msb, x_lsb}); // Column Set
        WriteCommand(cs, dc, 0x2b, {y_msb, y_lsb, y_msb, y_lsb}); // Page Set
        WriteCommand(cs, dc, 0x2c, {color_msb, color_lsb}); // RAM Write
        return true;
    }
    return false;
}

bool ILI9341::Fill(int r, int g, int b) {
    int color = Color565(r, g, b);
    byte color_msb = color >> 8;
    byte color_lsb = color & 255;
    int w = GetWidth();
    int h = GetHeight();
    if (mirror_diagonal) {
        int s = w;
        w = h;
        h = s;
    }
    for (int x = 0; x < w; x++) {
        int y = h - 1;
        byte x_msb = x >> 8;
        byte x_lsb = x & 255;
        byte y_msb = y >> 8;
        byte y_lsb = y & 255;
        WriteCommand(cs, dc, 0x2a, {x_msb, x_lsb, x_msb, x_lsb}); // Column Set
        WriteCommand(cs, dc, 0x2b, {0, 0, y_msb, y_lsb}); // Page Set
        WriteCommand(cs, dc, 0x2c); // RAM Write
        digitalWrite(dc, HIGH);
        digitalWrite(cs, LOW);
        for (y = 0; y < h; y++) {
            SPI.transfer(color_msb);
            SPI.transfer(color_lsb);
        }
        digitalWrite(cs, HIGH);
    }
    return true;
}

}

#endif
