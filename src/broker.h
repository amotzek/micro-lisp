#ifndef BROKER_H
#define BROKER_H
#include <string>
#include <list>
#include "environment.h"
#include "socket.h"

namespace lisp {

class Subscription {
    public:
        Subscription(std::string topic_filter, uint16_t packet_id, long priority, Symbol* parameter, Expression* continuation, Environment* environment);
        ~Subscription();
        void SetPacketId(uint16_t i) { packet_id = i; }
        bool HasPacketId(uint16_t i) const { return packet_id == i; }
        bool MatchesTopic(std::string t) const { return topic_filer.compare(t) == 0; } // TODO Filter korrekt berücksichtigen
        bool PushTask(Expression* value) const;
    private:
        std::string topic_filer;
        uint16_t packet_id;
        long priority;
        Symbol* parameter;
        Expression* continuation;
        Environment* environment;
};

class Broker : public Expression {
    public:
        Broker(Socket* s);
        virtual ~Broker();
        virtual ExpressionType GetType() { return ExpressionType::BROKER; }
        Socket* GetSocket() const { return socket; }
        void SetDead() { alive = false; }
        void SetConnected() { connected = true; }
        bool IsAlive() const { return alive; }
        bool IsAliveAndConnected() const { return alive && connected; }
        uint16_t NextPacketId();
        bool AddSubscription(std::string topic_filter, uint16_t packet_id, long priority, Symbol* parameter, Expression* continuation, Environment* environment);
        void RemoveSubscription(uint16_t i);
        Subscription* FindSubscription(std::string topic_name) const;
        bool HasSubscriptions() const { return subscriptions.size() > 0; }
    private:
        Socket* socket;
        bool alive;
        bool connected;
        uint16_t packet_id;
        std::list<Subscription*> subscriptions;
};

}

#endif
