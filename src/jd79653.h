#ifndef JD79653_H
#define JD79653_H
#include <cstdint>
#include "pixel_map.h"

namespace jd79653 {

class JD79653 : public lisp::FixedSizePixelMap {
    public:
        JD79653(int s, int o, int c, int d, int r, int b) : FixedSizePixelMap(200, 200) { sck = s; mosi = o; cs = c; dc = d; rst = r; busy = b; }
        virtual ~JD79653() { }
        virtual bool GetPixel(int x, int y, int &r, int &g, int &b) const;
        virtual bool SetPixel(int x, int y, int r, int g, int b);
        virtual bool Fill(int r, int g, int b);
        virtual void Init();
        virtual bool Flush();
    private:
        int sck;
        int mosi;
        int cs;
        int dc;
        int rst;
        int busy;
        static const int map_size = (200 * 200) / 8;
        uint8_t last_map[map_size];
        uint8_t current_map[map_size];
        bool WaitBusy(long duration);
        bool WakeUp();
        void WriteMaps();
        void Clear();
        void SleepDeep();
};

}

#endif
