#ifdef ARDUINO
#include <list>
#include <algorithm>
#include <Arduino.h>
#include <SPI.h>
#include "jd79653.h"

namespace jd79653 {

void WriteCommand(int cs, int dc, byte command, std::list<byte> arguments = { }) {
    digitalWrite(dc, LOW);
    digitalWrite(cs, LOW);
    SPI.transfer(command);
    digitalWrite(cs, HIGH);
    if (arguments.size() > 0) {
        digitalWrite(dc, HIGH);
        digitalWrite(cs, LOW);
        for (byte data : arguments) {
            SPI.transfer(data);
        }
        digitalWrite(cs, HIGH);
    }
}

void WriteMap(int cs, int dc, byte command, int map_size, uint8_t map[]) {
    digitalWrite(dc, LOW);
    digitalWrite(cs, LOW);
    SPI.transfer(command);
    digitalWrite(cs, HIGH);
    digitalWrite(dc, HIGH);
    digitalWrite(cs, LOW);
    for (int i = 0; i < map_size; i++) {
        SPI.transfer((byte) map[i]);
    }
    digitalWrite(cs, HIGH);
}

void JD79653::Init() {
    SPI.begin(sck, -1, mosi); // siehe https://github.com/espressif/arduino-esp32/blob/master/libraries/SPI/src/SPI.h
    pinMode(cs, OUTPUT);
    pinMode(dc, OUTPUT);
    pinMode(rst, OUTPUT);
    pinMode(busy, INPUT);
    digitalWrite(cs, HIGH);
    digitalWrite(dc, HIGH);
    if (WakeUp()) {
        Clear();
    }
    SleepDeep();
}

bool JD79653::WaitBusy(long duration) {
    while (duration > 0) {
        if (digitalRead(busy) == HIGH) {
            return true;
        }
        if (duration >= 30) {
            delay(30);
            duration -= 30;
        } else {
            delay(duration);
            duration = 0;
        }
    }
    return false;
}

bool JD79653::WakeUp() {
    digitalWrite(rst, HIGH);
    delay(10);
    digitalWrite(rst, LOW);
    delay(100);
    digitalWrite(rst, HIGH);
    delay(100);
    WaitBusy(1000);
    WriteCommand(cs, dc, 0x00, {0xdf, 0x0e});
    WriteCommand(cs, dc, 0x4d, {0x55});
    WriteCommand(cs, dc, 0xaa, {0x0f});
    WriteCommand(cs, dc, 0xe9, {0x02});
    WriteCommand(cs, dc, 0xb6, {0x11});
    WriteCommand(cs, dc, 0xf3, {0x0a});
    WriteCommand(cs, dc, 0x61, {0xc8, 0x00, 0xc8});
    WriteCommand(cs, dc, 0x60, {0x00});
    WriteCommand(cs, dc, 0x50, {0xd7});
    WriteCommand(cs, dc, 0xe3, {0x00});
    WriteCommand(cs, dc, 0x04); // Power on
    delay(100);
    return WaitBusy(1000);
}

void JD79653::WriteMaps() {
    WriteMap(cs, dc, 0x10, map_size, last_map);
    delay(2);
    WriteMap(cs, dc, 0x13, map_size, current_map);
    delay(2);
    WriteCommand(cs, dc, 0x12);
    WaitBusy(1000);
    std::copy_n(current_map, map_size, last_map);
}

void JD79653::Clear() {
    std::fill_n(last_map, map_size, 0xff);
    std::fill_n(current_map, map_size, 0x00);
    WriteMaps();
    std::fill_n(last_map, map_size, 0x00);
    std::fill_n(current_map, map_size, 0xff);
    WriteMaps();
}

void JD79653::SleepDeep() {
    WriteCommand(cs, dc, 0x50, {0xf7});
    WriteCommand(cs, dc, 0x02); // Power off
    WaitBusy(5000);
    WriteCommand(cs, dc, 0x07, {0xa5}); // Deep sleep
}

bool JD79653::SetPixel(int x, int y, int r, int g, int b) {
    if (IsInside(x, y)) {
        int index = x + GetWidth() * (GetHeight() - y - 1);
        uint8_t mask = 0x80 >> (index & 7);
        if (r > 0 || g > 0 || b > 0) {
            current_map[index >> 3] |= mask;
        } else {
            current_map[index >> 3] &= ~mask;
        }
        return true;
    }
    return false;
}

bool JD79653::GetPixel(int x, int y, int &r, int &g, int &b) const {
    if (IsInside(x, y)) {
        int index = x + GetWidth() * (GetHeight() - y - 1);
        uint8_t data = current_map[index >> 3];
        uint8_t mask = 0x80 >> (index & 7);
        if (data & mask) {
            r = 255;
            g = 255;
            b = 255;
        } else {
            r = 0;
            g = 0;
            b = 0;
        }
        return true;
    }
    return false;
}

bool JD79653::Fill(int r, int g, int b) {
    std::fill_n(current_map, map_size, (r > 0 || g > 0 || b > 0) ? 0xff : 0x00);
    return true;
}

bool JD79653::Flush() {
    if (WakeUp()) {
        WriteMaps();
        SleepDeep();
        return true;
    }
    //
    SleepDeep();
    return false;
}

}

#endif
