#ifndef SOCKET_COMBINATOR_H
#define SOCKET_COMBINATOR_H
#include <string>
#include "binary_combinator.h"

namespace lisp {

class CreateSocket2 : public BinaryCombinator {
    public:
        CreateSocket2() { }
        virtual ~CreateSocket2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
