#ifndef TEXT_FILE_STREAM_H
#define TEXT_FILE_STREAM_H
#include <string>
#include "expression.h"

namespace lisp {

class TextFileStream : public Expression {
    public:
        TextFileStream(const std::string &n) { name = n; buffer = ""; }
        virtual ~TextFileStream() { Close(); }
        virtual ExpressionType GetType() { return ExpressionType::STREAM; }
        bool WriteChar(char c);
        bool WriteString(const std::string &str);
        bool Flush();
        bool Close();
    private:
        std::string name;
        std::string buffer;
};

}

#endif
