#ifndef LIST_COMBINATOR_H
#define LIST_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"

namespace lisp {

class Cons2 : public BinaryCombinator {
    public:
        Cons2() { }
        virtual ~Cons2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class First1 : public UnaryCombinator {
    public:
        First1() { }
        virtual ~First1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Rest1 : public UnaryCombinator {
    public:
        Rest1() { }
        virtual ~Rest1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class IsNull1 : public UnaryCombinator {
    public:
        IsNull1() { }
        virtual ~IsNull1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class ListLength1 : public UnaryCombinator {
    public:
        ListLength1() { }
        virtual ~ListLength1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Reverse1 : public UnaryCombinator {
    public:
        Reverse1() { }
        virtual ~Reverse1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Sort2 : public BinaryCombinator {
    public:
        Sort2() { }
        virtual ~Sort2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
