// #include <typeinfo>
// #include <iostream>
#include "reference_counted.h"

namespace lisp {

long ReferenceCounted::total_count = 0l;

long ReferenceCounted::DeleteReference() {
    total_count--;
    return --count;
}

void Delete(ReferenceCounted* instance) {
    if (instance) {
        long count = instance->DeleteReference();
        if (count <= 0) {
            // std::cout << "delete " << typeid(*instance).name() << "[" << count << "] " << instance << std::endl;
            delete instance;
        }
    }
}

}
