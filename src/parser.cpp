#include <new>
#include "hal.h"
#include "parser.h"
#include "symbol.h"
#include "rational.h"
#include "float.h"
#include "string.h"

namespace lisp {

void Parser::Scan() {
    if (!stream) {
        state = ParserState::ERROR;
        return;
    }
    state = ParserState::START;
    while (true) {
        char c = stream->peek();
        if (!stream->good()) {
            break;
        }
        switch (state) {
            case ParserState::START: {
                if (c <= ' ') {
                    stream->ignore();
                    break;
                }
                if (c == '"') {
                    state = ParserState::STRING;
                    stream->ignore();
                    break;
                }
                if (c == '(') {
                    state = ParserState::OPENING_BRACKET;
                    stream->ignore();
                    return;
                }
                if (c == ')') {
                    state = ParserState::CLOSING_BRACKET;
                    stream->ignore();
                    return;
                }
                if ((c >= '0' && c <= '9')) {
                    state = ParserState::INTEGER;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                if (c == '-' || c == '+') {
                    state = ParserState::INTEGER_OR_SYMBOL;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                state = ParserState::SYMBOL;
                stream->ignore();
                token.push_back(c);
                break;
            }
            case ParserState::INTEGER_OR_SYMBOL: {
                if (c >= '0' && c <= '9') {
                    state = ParserState::INTEGER;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                state = ParserState::SYMBOL;
            }
            // FALLTHRU
            case ParserState::SYMBOL: {
                if (c != '(' && c != ')' && c > ' ' && c <= 'z') {
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                return;
            }
            case ParserState::INTEGER: {
                if (c >= '0' && c <= '9') {
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                if (c == '.') {
                    state = ParserState::DECIMAL_PLACES;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                if (c == 'e' || c == 'E') {
                    state = ParserState::EXPONENT;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                return;
            }
            case ParserState::DECIMAL_PLACES: {
                if (c >= '0' && c <= '9') {
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                if (c == 'e' || c == 'E') {
                    state = ParserState::EXPONENT;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                state = ParserState::FLOAT;
                return;
            }
            case ParserState::EXPONENT: {
                if ((c >= '0' && c <= '9') || c == '-' || c == '+') {
                    state = ParserState::FLOAT;
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                state = ParserState::ERROR;
                return;
            }
            case ParserState::FLOAT: {
                if (c >= '0' && c <= '9') {
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                return;
            }
            case ParserState::STRING: {
                if (c == '\\') {
                    state = ParserState::STRING_ESCAPE;
                    stream->ignore();
                    break;
                }
                if (c != '"') {
                    stream->ignore();
                    token.push_back(c);
                    break;
                }
                stream->ignore();
                return;
            }
            case ParserState::STRING_ESCAPE: {
                state = ParserState::STRING;
                stream->ignore();
                token.push_back(c);
                break;
            }
            default: {
                return; // Unerreichbarer Code
            }
        }
    }
}

const std::string NIL = "nil";

Expression* Parser::ReadExpression() {
    Expression* expression = nullptr;
    Scan();
    switch (state) {
        case ParserState::SYMBOL: {
            if (token.compare(NIL) != 0) {
                expression = Intern(token);
            }
            break;
        }
        case ParserState::INTEGER: {
            expression = new(std::nothrow) Rational(stol(token), 1l);
            break;
        }
        case ParserState::DECIMAL_PLACES:
        case ParserState::FLOAT: {
            expression = new(std::nothrow) Float(stod(token));
            break;
        }
        case ParserState::STRING: {
            expression = new(std::nothrow) String(token);
            break;
        }
        case ParserState::OPENING_BRACKET: {
            return ReadList(); // tail call
        }
        case ParserState::CLOSING_BRACKET: {
            break;
        }
        default: {
            state = ParserState::ERROR;
            break;
        }
    }
    token.clear();
    return expression;
}

List* Parser::ReadList() {
    List* list = nullptr;
    while (true) {
        auto first = ReadExpression();
        if (state == ParserState::ERROR) {
            Delete(list);
            return nullptr;
        }
        if (state != ParserState::CLOSING_BRACKET) {
            if (!Push(first, list)) {
                Delete(first);
                Delete(list);
                state = ParserState::ERROR;
                return nullptr;
            }
            Delete(first);
            continue;
        }
        break;
    }
    List* reversed_list;
    auto success = Reverse(list, reversed_list);
    Delete(list);
    if (success) {
        state = ParserState::START;
        return reversed_list;
    }
    state = ParserState::ERROR;
    return nullptr;
}

}
