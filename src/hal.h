#ifndef HAL_H
#define HAL_H
#include <string>
#include <istream>
#include <list>
#include <tuple>
#include "reference_counted.h"

namespace lisp {

std::string GetTypeName(ReferenceCounted* instance);

}

namespace hal {

long GetMonotonicTime();

void Sleep(long ms);

std::istream* CreateFileStream(std::string filename);

bool AppendTextFile(std::string filename, std::string text);

bool DeleteFile(std::string filename);

void PinModeInput(int pin);

void PinModeInputPullup(int pin);

void PinModeOutput(int pin);

int AnalogRead(int pin);

bool DigitalRead(int pin);

void DigitalWrite(int pin, bool value);

void SetupWlan();

bool SetHostname(std::string hostname);

bool ConnectWlan(std::list<std::tuple<std::string, std::string>>* credentials);

bool IsWlanConnected();

int CreateSocket(std::string hostname, uint16_t port);

bool WriteSocket(int fd, uint8_t data);

bool WriteSocket(int fd, const uint8_t *buffer, size_t size);

bool AvailableReadSocket(int fd, int size);

int ReadSocket(int fd);

bool ReadSocket(int fd, uint8_t *buffer, size_t size);

void CloseSocket(int fd);

std::string GetEnv(std::string name);

}

#ifdef ARDUINO

long stol(std::string s);

double stod(std::string s);

namespace std {

string to_string(long a);

}

#endif
#endif
