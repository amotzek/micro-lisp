#ifndef ARDUINO
#include "tests.h"

int main() {
    if (lisp::RunTests()) {
        return 0;
    }
    return 1;
}

#endif
