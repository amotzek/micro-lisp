#ifndef ASSERT_H
#define ASSERT_H
#include <string>
#include <list>
#include <tuple>

namespace lisp {

std::tuple<std::string, std::string> MakePair(const std::string &expression, const std::string &value);

bool AssertEval(std::list<std::tuple<std::string, std::string>> pairs);

}

#endif
