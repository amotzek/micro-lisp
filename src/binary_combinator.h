#ifndef BINARY_COMBINATOR_H
#define BINARY_COMBINATOR_H
#include "combinator.h"

namespace lisp {

class BinaryCombinator : public Combinator {
    public:
        BinaryCombinator() { }
        virtual ~BinaryCombinator() { }
        virtual int GetParameterCount() { return 2; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) = 0;
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env) {
            bool success = false;
            if (sstack) {
                auto v1 = Pop(sstack);
                if (sstack) {
                    auto v2 = Pop(sstack);
                    success = ProcessValues(v1, v2, cstack, sstack, env);
                    Delete(v2);
                }
                Delete(v1);
            }
            return success;
        }
};

}

#endif
