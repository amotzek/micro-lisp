#ifndef PARSER_H
#define PARSER_H
#include <new>
#include <sstream>
#include <istream>
#include <string>
#include "list.h"

namespace lisp {

enum class ParserState {
    START,
    OPENING_BRACKET,
    CLOSING_BRACKET,
    SYMBOL,
    INTEGER_OR_SYMBOL,
    INTEGER,
    FLOAT,
    DECIMAL_PLACES,
    EXPONENT,
    STRING,
    STRING_ESCAPE,
    ERROR
};

class Parser {
    public:
        Parser(const std::string s) : Parser(new(std::nothrow) std::istringstream(s)) { }
        Parser(std::istream* s) { stream = s; state = ParserState::START; token = std::string(); }
        Expression* ReadExpression();
        ParserState GetState() { return state; }
        void DeleteStream() { delete stream; stream = nullptr; }
    private:
        void Scan();
        List* ReadList();
        std::istream* stream;
        ParserState state;
        std::string token;
};

}

#endif
