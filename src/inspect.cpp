#ifndef ARDUINO
#include <iostream>
#include "inspect.h"
#include "hal.h"
#include "list.h"
#include "symbol.h"
#include "rational.h"
#include "float.h"
#include "string.h"
#include "lambda.h"
#include "macro.h"

namespace lisp {

void InspectExpression(Expression* expression, int level);

void Inspect(Expression* expression) {
    InspectExpression(expression, 0);
}

void InspectNil(int level) {
    while (level > 0) {
        std::cout << " ";
        level--;
    }
    std::cout << "*Nil";
}

void InspectReferenceCounted(ReferenceCounted* reference_counted, int level) {
    while (level > 0) {
        std::cout << " ";
        level--;
    }
    std::cout << GetTypeName(reference_counted) << "[" << reference_counted->GetReferenceCount() << "] " << reference_counted << " ";
}

void InspectSymbol(Symbol* expression, int level) {
    InspectReferenceCounted(expression, level);
    std::cout << expression->GetName();
}

void InspectRational(Rational* expression, int level) {
    InspectReferenceCounted(expression, level);
    std::cout << expression->GetNumerator() << "/" << expression->GetDenominator();
}

void InspectFloat(Float* expression, int level) {
    InspectReferenceCounted(expression, level);
    std::cout << expression->GetDouble();
}

void InspectString(String* expression, int level) {
    InspectReferenceCounted(expression, level);
    std::cout << expression->GetString();
}

void NewLine() {
    std::cout << std::endl;
}

void InspectLambda(Lambda* expression, int level) {
    InspectReferenceCounted(expression, level);
    NewLine();
    InspectExpression(expression->GetParameters(), level + 1);
    InspectExpression(expression->GetBody(), level + 1);
    InspectReferenceCounted(expression->GetEnvironment(), level + 1);
}

void InspectMacro(Macro* expression, int level) {
    InspectReferenceCounted(expression, level);
    NewLine();
    InspectExpression(expression->GetParameter(), level + 1);
    InspectExpression(expression->GetBody(), level + 1);
    InspectReferenceCounted(expression->GetEnvironment(), level + 1);
}

void InspectList(List* expression, int level) {
    InspectReferenceCounted(expression, level);
    NewLine();
    level++;
    while (expression) {
        Expression* first = expression->GetFirst();
        expression = expression->GetRest();
        InspectExpression(first, level);
        if (expression) {
            InspectReferenceCounted(expression, level);
            NewLine();
        } else {
            InspectNil(level);
        }
    }
}

void InspectExpression(Expression* expression, int level) {
    if (expression) {
        switch (expression->GetType()) {
            case ExpressionType::LIST: {
                InspectList((List*) expression, level);
                break;
            }
            case ExpressionType::SYMBOL: {
                InspectSymbol((Symbol*) expression, level);
                break;
            }
            case ExpressionType::INTEGER:
            case ExpressionType::RATIO: {
                InspectRational((Rational*) expression, level);
                break;
            }
            case ExpressionType::FLOAT: {
                InspectFloat((Float*) expression, level);
                break;
            }
            case ExpressionType::STRING: {
                InspectString((String*) expression, level);
                break;
            }
            case ExpressionType::LAMBDA: {
                InspectLambda((Lambda*) expression, level);
                break;
            }
            case ExpressionType::MACRO: {
                InspectMacro((Macro*) expression, level);
                break;
            }
            default: {
                InspectReferenceCounted((ReferenceCounted*) expression, level);
                break;
            }
        }
    } else {
        InspectNil(level);
    }
    NewLine();
}

}

#endif
