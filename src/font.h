#ifndef FONT_H
#define FONT_H
#include <string>
#include "display.h"

namespace lisp {

class Font {
    public:
        Font(int h, const int* g, int m) { height = h; glyphs = g; magnification = m; }
        virtual ~Font() { }
        int GetHeight() { return height; }
        int BitBlit(char c, Display* display, int x, int y, int r, int g, int b);
    private:
        int height;
        int magnification;
        const int* glyphs;
        const int* FindGlyph(char c);
};

Font* CreateFont(int fontsize);

void Delete(Font* font);

}

#endif
