#include "socket.h"
#include "hal.h"

namespace lisp {

Socket::~Socket() {
    hal::CloseSocket(fd);
}

}
