#ifdef ARDUINO
#include <iostream>
#include <sstream>
#include "Arduino.h"

namespace hal {

class SerialBuf : public std::stringbuf {
    public:
        virtual int sync() {
            for (char c : this->str()) {
                Serial.write(c);
                Serial.flush();
            }
            this->str("");
            return 0;
        }
};

auto new_buf = new SerialBuf();

auto old_buf = std::cout.rdbuf(new_buf);

}

#endif
