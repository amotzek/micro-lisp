#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include "symbol.h"
#include "list.h"

namespace lisp {

class Environment : public ReferenceCounted {
    public:
        Environment(Environment* p) { parent = Copy(p); }
        virtual ~Environment() { Delete(parent); }
        bool AddPair(Symbol* key, Expression* value);
        bool ModifyOrAddPair(Symbol* key, Expression* value);
        bool RemovePair(Symbol* key);
        bool Lookup(Symbol* key, Expression* &value);
    protected:
        virtual List* GetBucket(Symbol* key) = 0;
        virtual void SetBucket(Symbol* key, List* bucket) = 0;
        Environment* parent;
};

class SmallEnvironment : public Environment {
    public:
        SmallEnvironment(Environment* p) : Environment(p) { for (int i = 0; i < 4; i++) buckets[i] = nullptr; }
        virtual ~SmallEnvironment() { for (int i = 0; i < 4; i++) Delete(buckets[i]); }
    protected:
        virtual List* GetBucket(Symbol* key) { return buckets[key->GetHash() & 3]; }
        virtual void SetBucket(Symbol* key, List* bucket) { buckets[key->GetHash() & 3] = bucket; }
    private:
        List* buckets[4];
};

class LargeEnvironment : public Environment {
    public:
        LargeEnvironment(Environment* p) : Environment(p) { for (int i = 0; i < 32; i++) buckets[i] = nullptr; }
        virtual ~LargeEnvironment() { for (int i = 0; i < 32; i++) Delete(buckets[i]); }
    protected:
        virtual List* GetBucket(Symbol* key) { return buckets[key->GetHash() & 31]; }
        virtual void SetBucket(Symbol* key, List* bucket) { buckets[key->GetHash() & 31] = bucket; }
    private:
        List* buckets[32];
};

}

#endif
