#ifndef STRING_COMBINATOR_H
#define STRING_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"
#include "ternary_combinator.h"

namespace lisp {

class Intern1 : public UnaryCombinator {
    public:
        Intern1() { }
        virtual ~Intern1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class CharCode1 : public UnaryCombinator {
    public:
        CharCode1() { }
        virtual ~CharCode1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class CodeChar1 : public UnaryCombinator {
    public:
        CodeChar1() { }
        virtual ~CodeChar1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Concatenate2 : public BinaryCombinator {
    public:
        Concatenate2() { }
        virtual ~Concatenate2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Substring3 : public TernaryCombinator {
    public:
        Substring3() { }
        virtual ~Substring3() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class StringLength1 : public UnaryCombinator {
    public:
        StringLength1() { }
        virtual ~StringLength1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class ReadFromString1 : public UnaryCombinator {
    public:
        ReadFromString1() { }
        virtual ~ReadFromString1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class WriteToString1 : public UnaryCombinator {
    public:
        WriteToString1() { }
        virtual ~WriteToString1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class GetEnv1 : public UnaryCombinator {
    public:
        GetEnv1() { }
        virtual ~GetEnv1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class ParseJson1 : public UnaryCombinator {
    public:
        ParseJson1() { }
        virtual ~ParseJson1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
