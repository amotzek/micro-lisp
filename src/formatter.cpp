#include "formatter.h"
#include "hal.h"
#include "list.h"
#include "symbol.h"
#include "number.h"
#include "string.h"

namespace lisp {

std::ostream& operator<<(std::ostream &ostream, Expression* expr) {
    if (expr) {
        switch(expr->GetType()) {
            case ExpressionType::LIST: {
                auto list = (List*) expr;
                ostream << '(';
                while (true) {
                    ostream << list->GetFirst();
                    list = list->GetRest();
                    if (list) {
                        ostream << ' ';
                    } else {
                        break;
                    }
                }
                ostream << ')';
                break;
            }
            case ExpressionType::SYMBOL: {
                auto symbol = (Symbol*) expr;
                ostream << symbol->GetName();
                break;
            }
            case ExpressionType::INTEGER:
            case ExpressionType::RATIO:
            case ExpressionType::FLOAT: {
                auto number = (Number*) expr;
                ostream << number->GetDouble();
                break;
            }
            case ExpressionType::STRING: {
                auto string = (String*) expr;
                ostream << '"';
                for (char c : string->GetString()) {
                    if (c == '"') {
                        ostream << "\\\"";
                    } else if (c == '\\') {
                        ostream << "\\\\";
                    } else {
                        ostream << c;
                    }
                }
                ostream << '"';
                break;
            }
            default: {
                // TODO Read Time Evaluation implementieren
                ostream << "#.(throw (quote error) \"instances of ";
                ostream << GetTypeName(expr);
                ostream << " cannot be read or written\")";
            }
        }
    } else {
        ostream << "nil";
    }
    return ostream;
}

}
