#ifndef LAMBDA_H
#define LAMBDA_H
#include "environment.h"

namespace lisp {

class Lambda : public Expression {
    public:
        Lambda(List* p, Expression* b, Environment* e) { parameters = Copy(p); body = Copy(b); environment = Copy(e); }
        virtual ~Lambda() { Delete(parameters); Delete(body); Delete(environment); }
        virtual ExpressionType GetType() { return ExpressionType::LAMBDA; }
        List* GetParameters() { return parameters; }
        int GetParameterCount() { return Length(parameters); }
        Expression* GetBody() { return body; }
        Environment* GetEnvironment() { return environment; }
    private:
        List* parameters;
        Expression* body;
        Environment* environment;
};

}

#endif
