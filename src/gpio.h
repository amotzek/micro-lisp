#ifndef GPIO_H
#define GPIO_H
#include "expression.h"

namespace lisp {

enum class GpioMode {
    ANALOG_INPUT,
    DIGITAL_INPUT,
    DIGITAL_INPUT_PULLUP,
    ANALOG_OUTPUT,
    DIGITAL_OUTPUT
};

class Gpio : public Expression {
    public:
        Gpio(int p, GpioMode m) { pin = p; mode = m; }
        virtual ~Gpio() { }
        virtual ExpressionType GetType() { return ExpressionType::GPIO; }
        int GetPin() const { return pin; }
        GpioMode GetMode() const { return mode; }
    private:
        int pin;
        GpioMode mode;
};

}

#endif
