#ifdef ARDUINO
#include "Arduino.h"

namespace hal {

long GetMonotonicTime() {
    return millis();
}

void Sleep(long ms) {
    delay(ms);
}

}

#endif
