#ifndef ARDUINO

namespace hal {

void PinModeInput(int pin) { }

void PinModeInputPullup(int pin) { }

void PinModeOutput(int pin) { }

int AnalogRead(int pin) { return 0; }

bool DigitalRead(int pin) { return false; }

void DigitalWrite(int pin, bool value) { }

}

#endif
