#include <iostream>
#include "loader.h"
#include "parser.h"
#include "eval.h"
#include "formatter.h"

namespace lisp {

bool Load(std::istream* istream, Environment* environment, bool verbose) {
    auto success = true;
    auto parser = Parser(istream);
    while (istream->peek() && istream->good() && parser.GetState() != ParserState::ERROR) {
        auto expression = parser.ReadExpression();
        if (parser.GetState() == ParserState::ERROR) {
            if (verbose) {
                std::cout << "cannot parse" << std::endl;
            }
            success = false;
            break;
        }
        Expression* result = nullptr;
        if (Eval(expression, environment, result)) {
            if (verbose) {
                std::cout << result << std::endl;
            }
        } else {
            if (verbose) {
                std::cout << "cannot eval" << std::endl;
            }
            success = false;
        }
        Delete(result);
        Delete(expression);
    }
    return success;
}

}
