#ifndef CONTROL_COMBINATOR_H
#define CONTROL_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"
#include "ternary_combinator.h"

namespace lisp {

class If3 : public TernaryCombinator {
    public:
        If3() { }
        virtual ~If3() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index > 0; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class Or2 : public BinaryCombinator {
    public:
        Or2() { }
        virtual ~Or2() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index > 0; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Prog2 : public BinaryCombinator {
    public:
        Prog2() { }
        virtual ~Prog2() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index > 0; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Catch2 : public BinaryCombinator {
    public:
        Catch2() { }
        virtual ~Catch2() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index > 0; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Throw2 : public BinaryCombinator {
    public:
        Throw2() { }
        virtual ~Throw2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class TypeOf1 : public UnaryCombinator {
    public:
        TypeOf1() { }
        virtual ~TypeOf1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class GenSym0 : public Combinator {
    public:
        GenSym0() { }
        virtual ~GenSym0() { }
        virtual int GetParameterCount() { return 0; }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
