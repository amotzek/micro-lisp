#include <new>
#include "mqtt.h"
#include "hal.h"

namespace mqtt {

void BaseRequest::AppendByte(uint8_t b) {
    size += 1;
    content.push_back([b](int fd) { return hal::WriteSocket(fd, b); });
}

void BaseRequest::AppendShort(uint16_t s) {
    size += 2;
    content.push_back([s](int fd) { return hal::WriteSocket(fd, (uint8_t) (s >> 8)) && hal::WriteSocket(fd, (uint8_t) (s & 255)); });
}

void BaseRequest::AppendString(const std::string &s) {
    size += s.length();
    content.push_back([s](int fd) { return hal::WriteSocket(fd, (const uint8_t*) s.c_str(), s.length()); }); // By-Reference Capture führt zu Fehler bei GCC 10.2
}

bool BaseRequest::WriteToSocket(int fd) {
    uint8_t meta[4];
    meta[0] = (packet_type << 4) | (flags & 15);
    int meta_size = 1;
    while (true) {
        uint8_t digit = size & 127;
        size >>= 7;
        if (size > 0) {
            meta[meta_size] = digit | 128;
            meta_size++;
        } else {
            meta[meta_size] = digit;
            meta_size++;
            break;
        }
    }
    if (!hal::WriteSocket(fd, meta, meta_size)) {
        content.clear();
        return false;
    }
    for (auto writer : content) {
        if (!writer(fd)) {
            content.clear();
            return false;
        }
    }
    content.clear();
    return true;
}

const std::string mqtt = "MQTT";

int BaseResponse::ReadByte(int fd) {
    return hal::ReadSocket(fd);
}

int BaseResponse::ReadShort(int fd) {
    uint8_t data[4];
    if (hal::ReadSocket(fd, data, 2)) {
        return data[0] << 8 | data[1];
    }
    return -1;
}

int BaseResponse::ReadString(int fd, std::string &str, int len) {
    str.clear();
    if (len > 0) {
        char data[32];
        int rest = len;
        str.reserve(len);
        while (rest >= 32) {
            if (!hal::ReadSocket(fd, (uint8_t *) data, 32)) {
                return -1;
            }
            str.append(data, 32);
            rest -= 32;
        }
        if (rest > 0) {
            if (!hal::ReadSocket(fd, (uint8_t *) data, (size_t) rest)) {
                return -1;
            }
            str.append(data, rest);
        }
    }
    return len;
}

int BaseResponse::ReadLenString(int fd, std::string &str) {
    int len = ReadShort(fd);
    return ReadString(fd, str, len);
}

BaseResponse* BaseResponse::ReadFromSocket(int fd) {
    int first_byte = ReadByte(fd);
    if (first_byte < 0) {
        return nullptr;
    }
    int flags = first_byte & 15;
    int packet_type = first_byte >> 4;
    int size = 0;
    int multiplier = 1;
    while (true) {
        int digit = ReadByte(fd);
        if (digit < 0) {
            return nullptr;
        }
        size += (digit & 127) * multiplier;
        multiplier <<= 7;
        if ((digit & 128) == 0) {
            break;
        }
    }
    switch (packet_type) {
        case 2: {
            if (size == 2) {
                return new(std::nothrow) ConnectAcknowledgement(packet_type, flags, fd);
            }
            break;
        }
        case 3: {
            if (size >= 2) {
                return new(std::nothrow) PublishNotification(packet_type, flags, size, fd);
            }
            break;
        }
        case 9: {
            if (size == 3) {
                return new(std::nothrow) SubscribeAcknowledgement(packet_type, flags, fd);
            }
            break;
        }
        case 11: {
            if (size == 2) {
                return new(std::nothrow) UnsubscribeAcknowledgement(packet_type, flags, fd);
            }
            break;
        }
        case 13: {
            if (size == 0) {
                return new(std::nothrow) PingResponse(packet_type, flags);
            }
            break;
        }
    }
    while (size > 0 && ReadByte(fd) >= 0) {
        size--;
    }
    return nullptr;
}

ConnectAcknowledgement::ConnectAcknowledgement(int p, int f, int fd) : BaseResponse(p, f) {
    session_present = ReadByte(fd);
    return_code = ReadByte(fd);
}

PublishNotification::PublishNotification(int p, int f, int size, int fd) : BaseResponse(p, f) {
    int topic_name_len = ReadLenString(fd, topic_name);
    if (topic_name_len >= 0) {
        if ((flags & 6) > 0) {
            packet_id = ReadShort(fd);
            if (packet_id >= 0) {
                ReadString(fd, payload, size - topic_name_len - 4);
            }
        } else {
            packet_id = -1;
            ReadString(fd, payload, size - topic_name_len - 2);
        }
    }
}

SubscribeAcknowledgement::SubscribeAcknowledgement(int p, int f, int fd) : BaseResponse(p, f) {
    packet_id = ReadShort(fd);
    return_code = ReadByte(fd);
}

UnsubscribeAcknowledgement::UnsubscribeAcknowledgement(int p, int f, int fd) : BaseResponse(p, f) {
    packet_id = ReadShort(fd);
}

void Delete(BaseRequest* request) {
    delete request;
}

void Delete(BaseResponse* response) {
    delete response;
}

}
