#ifndef UNARY_COMBINATOR_H
#define UNARY_COMBINATOR_H
#include "combinator.h"

namespace lisp {

class UnaryCombinator : public Combinator {
    public:
        UnaryCombinator() { }
        virtual ~UnaryCombinator() { }
        virtual int GetParameterCount() { return 1; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) = 0;
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env) {
            bool success = false;
            if (sstack) {
                auto v1 = Pop(sstack);
                success = ProcessValue(v1, cstack, sstack, env);
                Delete(v1);
            }
            return success;
        }
};

}

#endif
