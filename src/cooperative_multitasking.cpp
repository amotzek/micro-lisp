#include <new>
#include "cooperative_multitasking.h"
#include "eval.h"
#include "hal.h"

namespace lisp {

class LispTask : public Task {
    public:
        LispTask(long p, Expression* c, Expression* g, Environment* e, long w, long d) : Task(p, w, d) {
            continuation = Copy(c);
            guard = Copy(g);
            environment = Copy(e);
        }
        virtual ~LispTask() {
            Delete(continuation);
            Delete(guard);
            Delete(environment);
        }
        virtual long GetCycle() {
            return 30l;
        }
        virtual bool HasNoGuard() {
            return !guard;
        }
        virtual bool EvalGuard() {
            Expression* result = nullptr;
            auto success = Eval(guard, environment, result) && result;
            Delete(result);
            return success;
        }
        virtual void Execute() {
            Expression* result = nullptr;
            if (Eval(continuation, environment, result)) {
                Delete(result);
            }
        }
    private:
        Expression* continuation;
        Expression* guard;
        Environment* environment;
};

Task* Tasks::Now(long p, Expression* c, Environment* e) {
    return Push(new(std::nothrow) LispTask(p, c, nullptr, e, current, 0l));
}

Task* Tasks::After(long p, long d, Expression* c, Environment* e) {
    return Push(new(std::nothrow) LispTask(p, c, nullptr, e, current + d, 0l));
}

Task* Tasks::WhenForThen(long p, Expression* g, long d, Expression* c, Environment* e) {
    return Push(new(std::nothrow) LispTask(p, c, g, e, current, d));
}

bool Tasks::OnlyOneOf(Task* task1, Task* task2, Task* task3, Task* task4) {
    if (task1) {
        task1->AddSibling(task2);
        task1->AddSibling(task3);
        task1->AddSibling(task4);
    }
    if (task2) {
        task2->AddSibling(task1);
        task2->AddSibling(task3);
        task2->AddSibling(task4);
    }
    if (task3) {
        task3->AddSibling(task1);
        task3->AddSibling(task2);
        task3->AddSibling(task4);
    }
    if (task4) {
        task4->AddSibling(task1);
        task4->AddSibling(task2);
        task4->AddSibling(task3);
    }
    return true;
}

long TimeDiff(long ms1, long ms2) {
    return ms1 - ms2;
}

long TimeSum(long ms1, long ms2) {
   return ms1 + ms2;
}

long TimeMax(long ms1, long ms2) {
    if (TimeDiff(ms1, ms2) >= 0l) {
        return ms1;
    }
    return ms2;
}

long Tasks::GetCurrentTime() {
    current = TimeMax(current, hal::GetMonotonicTime());
    return current;
}

bool Tasks::Wait() {
    if (queue.empty()) {
        return false;
    }
    current = TimeMax(current, hal::GetMonotonicTime());
    auto task = queue.top();
    if (TimeDiff(current, task->GetWhen()) < 0l) {
        GarbageCollectSymbols();
        hal::Sleep(TimeDiff(task->GetWhen(), current));
        current = TimeMax(task->GetWhen(), hal::GetMonotonicTime());
    }
    return true;
}

void Tasks::MarkDueTasks() {
    std::vector<Task*> due_tasks;
    while (!queue.empty()) {
        auto task = queue.top();
        if (TimeDiff(current, task->GetWhen()) >= 0) {
            task->SetWhen(current); // alle fälligen Tasks bekommen den gleichen Timestamp
            due_tasks.push_back(task);
            queue.pop();
        } else {
            break;
        }
    }
    for (auto task : due_tasks) {
        queue.push(task); // damit sind die fälligen Tasks nach Priorität sortiert
    }
}

Task* Tasks::ExtractReadyTask() {
    auto task = queue.top();
    queue.pop();
    if (task->HasNoGuard()) {
        return task;
    }
    long cycle = task->GetCycle();
    if (task->EvalGuard()) {
        if (task->IsRemainingLessThan(cycle)) {
            return task;
        }
        task->DecreaseRemaining(cycle);
    } else {
        task->ResetRemaining();
    }
    task->SetWhen(TimeSum(current, cycle));
    queue.push(task);
    return nullptr;
}

void Tasks::CancelSiblings(Task* task) {
    CancelTask(task->GetSibling1());
    CancelTask(task->GetSibling2());
    CancelTask(task->GetSibling3());
}

void Tasks::CancelTask(Task* task) {
    if (task) {
        queue.remove(task);
        Delete(task);
    }
}

bool Tasks::HasTask(long d) {
    if (queue.empty()) {
        return false;
    }
    current = TimeMax(current, hal::GetMonotonicTime());
    auto task = queue.top();
    return TimeDiff(task->GetWhen(), current) < d;
}

bool Tasks::Run() {
    if (Wait()) {
        MarkDueTasks();
        auto task = ExtractReadyTask();
        if (task) {
            CancelSiblings(task);
            task->Execute();
            Delete(task);
        }
        return true;
    }
    return false;
}

int Tasks::Drain() {
    int count = 0;
    while (!queue.empty()) {
        Delete(queue.top());
        queue.pop();
        count++;
    }
    return count;
}

Tasks tasks;

}
