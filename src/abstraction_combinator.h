#ifndef ABSTRACTION_COMBINATOR_H
#define ABSTRACTION_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"

namespace lisp {

class Quote1 : public Combinator {
    public:
        Quote1() { }
        virtual ~Quote1() { }
        virtual int GetParameterCount() { return 1; }
        virtual bool MustBeQuoted(int argument_index) { return true; };
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env) { return true; }
};

class Setq2 : public BinaryCombinator {
    public:
        Setq2() { }
        virtual ~Setq2() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index == 0; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Unbind1 : public UnaryCombinator {
    public:
        Unbind1() { }
        virtual ~Unbind1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class IsBound1 : public UnaryCombinator {
    public:
        IsBound1() { }
        virtual ~IsBound1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Lambda2 : public BinaryCombinator {
    public:
        Lambda2() { }
        virtual ~Lambda2() { }
        virtual bool MustBeQuoted(int argument_index) { return true; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Macro2 : public BinaryCombinator {
    public:
        Macro2() { }
        virtual ~Macro2() { }
        virtual bool MustBeQuoted(int argument_index) { return true; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
