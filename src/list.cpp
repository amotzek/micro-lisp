#include <new> // std::nothrow
#include "list.h"

namespace lisp {

List::~List() {
    Delete(first);
    auto r = rest;
    while (r) {
        if (r->DeleteReference() > 0) {
            break;
        }
        auto rr = r->rest;
        r->rest = nullptr;
        delete r;
        r = rr;
    }
}

bool Cons(Expression* first, List* rest, List* &list) {
    list = new(std::nothrow) List(first, rest);
    return list;
}

bool Pair(Expression* first, Expression* second, List* &list) {
    List* i;
    auto success = Cons(second, nullptr, i);
    if (success) {
        success = Cons(first, i, list);
    }
    Delete(i);
    return success;
}

bool Push(Expression* first, List* &sstack) {
    List* i;
    if (Cons(first, sstack, i)) {
        Delete(sstack);
        sstack = i;
        return true;
    }
    return false;
}

Expression* Pop(List* &sstack) {
    auto f = Copy(sstack->GetFirst());
    auto r = Copy(sstack->GetRest());
    Delete(sstack);
    sstack = r;
    return f;
}

bool Reverse(List* list, List* &reversed_list) {
    reversed_list = nullptr;
    while (list) {
        List* i;
        if (!Cons(list->GetFirst(), reversed_list, i)) {
            Delete(reversed_list);
            reversed_list = nullptr;
            return false;
        }
        Delete(reversed_list);
        reversed_list = i;
        list = list->GetRest();
    }
    return true;
}

int Length(List* list) {
    int length = 0;
    while (list) {
        length++;
        list = list->GetRest();
    }
    return length;
}

}
