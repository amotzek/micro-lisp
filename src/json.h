#ifndef JSON_H
#define JSON_H
#include <string>
#include "expression.h"

namespace json {

bool ToJson(lisp::Expression* expr, std::string &str);

bool FromJson(const std::string &str, lisp::Expression* &expr);

}

#endif
