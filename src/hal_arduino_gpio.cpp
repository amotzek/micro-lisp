#ifdef ARDUINO
#include "Arduino.h"

namespace hal {

void PinModeInput(int pin) {
    pinMode(pin, INPUT);
}

void PinModeInputPullup(int pin) {
    pinMode(pin, INPUT_PULLUP);
}

void PinModeOutput(int pin) {
    pinMode(pin, OUTPUT);
}

int AnalogRead(int pin) {
    return analogRead(pin);
}

bool DigitalRead(int pin) {
    return digitalRead(pin) == HIGH;
}

void DigitalWrite(int pin, bool value) {
    digitalWrite(pin, value ? HIGH : LOW);
}

}

#endif
