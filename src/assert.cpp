#ifndef ARDUINO
#include <iostream>
#include "assert.h"
#include "parser.h"
#include "comparison_combinator.h"
#include "eval.h"
#include "inspect.h"

namespace lisp {

std::tuple<std::string, std::string> MakePair(const std::string &expression, const std::string &value) {
    return std::make_tuple(expression, value);
}

Expression* Parse(const std::string &string) {
    Parser parser = Parser(string);
    auto expression = parser.ReadExpression();
    parser.DeleteStream();
    if (parser.GetState() == ParserState::ERROR) {
        return nullptr;
    }
    return expression;
}

bool AssertEval(const std::string &expression, const std::string &value) {
    std::cout << "* " << expression << " = " << value << std::endl;
    long before_count = ReferenceCounted::GetTotalReferenceCount();
    auto expr = Parse(expression);
    auto val = Parse(value);
    auto env = GetRootEnvironment();
    Expression* res = nullptr;
    auto success = false;
    if (expr && Eval(expr, env, res)) {
        if (IsEqual(val, res)) {
            success = true;
        } else {
            std::cout << "assertion failed" << std::endl;
            Inspect(expr);
            std::cout << "expected value" << std::endl;
            Inspect(val);
            std::cout << "actual value" << std::endl;
            Inspect(res);
        }
    } else {
        std::cout << "error" << std::endl;
        Inspect(expr);
    }
    Delete(expr);
    Delete(val);
    Delete(res);
    GarbageCollectSymbols();
    long after_count = ReferenceCounted::GetTotalReferenceCount();
    if (before_count < after_count) {
        std::cout << "references leaked" << std::endl;
        std::cout << after_count - before_count << std::endl;
    } else if (before_count > after_count) {
        std::cout << "references reclaimed" << std::endl;
        std::cout << before_count - after_count << std::endl;
    }
    return success;
}

bool AssertEval(std::list<std::tuple<std::string, std::string>> pairs) {
    for (auto pair : pairs) {
        if (!AssertEval(std::get<0>(pair), std::get<1>(pair))) {
            return false;
        }
    }
    return true;
}

}

#endif
