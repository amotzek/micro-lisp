#ifndef ARDUINO
#include "jd79653.h"

namespace jd79653 {

void JD79653::Init() { }

bool JD79653::SetPixel(int x, int y, int r, int g, int b) { return false; }

bool JD79653::GetPixel(int x, int y, int &r, int &g, int &b) const { return false; }

bool JD79653::Fill(int r, int g, int b) { return false; }

bool JD79653::Flush() { return false; }

bool JD79653::WaitBusy(long duration) { return true; }

bool JD79653::WakeUp() { return true; }

void JD79653::WriteMaps() { }

void JD79653::Clear() { }

void JD79653::SleepDeep() { }

}

#endif
