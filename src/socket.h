#ifndef SOCKET_H
#define SOCKET_H
#include "expression.h"

namespace lisp {

class Socket : public Expression {
    public:
        Socket(int f) { fd = f; }
        virtual ~Socket();
        virtual ExpressionType GetType() { return ExpressionType::SOCKET; }
        int GetFd() const { return fd; }
    private:
        int fd;
};

}

#endif
