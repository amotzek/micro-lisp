#ifndef TERNARY_COMBINATOR_H
#define TERNARY_COMBINATOR_H
#include "combinator.h"

namespace lisp {

class TernaryCombinator : public Combinator {
    public:
        TernaryCombinator() { }
        virtual ~TernaryCombinator() { }
        virtual int GetParameterCount() { return 3; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env) = 0;
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env) {
            bool success = false;
            if (sstack) {
                auto v1 = Pop(sstack);
                if (sstack) {
                    auto v2 = Pop(sstack);
                    if (sstack) {
                        auto v3 = Pop(sstack);
                        success = ProcessValues(v1, v2, v3, cstack, sstack, env);
                        Delete(v3);
                    }
                    Delete(v2);
                }
                Delete(v1);
            }
            return success;
        }
};

}

#endif
