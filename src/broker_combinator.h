#ifndef BROKER_COMBINATOR_H
#define BROKER_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"
#include "quaternary_combinator.h"

namespace lisp {

class CreateBroker4 : public QuaternaryCombinator {
    public:
        CreateBroker4() { }
        virtual ~CreateBroker4() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env);
};

class IsConnectedBroker1 : public UnaryCombinator {
   public:
       IsConnectedBroker1() { }
       virtual ~IsConnectedBroker1() { }
       virtual bool MustBeQuoted(int argument_index) { return false; }
       virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Publish4 : public QuaternaryCombinator {
    public:
        Publish4() { }
        virtual ~Publish4() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env);
};

class Subscribe4 : public QuaternaryCombinator {
    public:
        Subscribe4() { }
        virtual ~Subscribe4() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index >= 2; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env);
};

class Unsubscribe2 : public BinaryCombinator {
    public:
        Unsubscribe2() { }
        virtual ~Unsubscribe2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Ping1 : public UnaryCombinator {
   public:
       Ping1() { }
       virtual ~Ping1() { }
       virtual bool MustBeQuoted(int argument_index) { return false; }
       virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
