#ifndef REFERENCE_COUNTED_H
#define REFERENCE_COUNTED_H

namespace lisp {

class ReferenceCounted {
    public:
        ReferenceCounted() { count = 1; total_count++; }
        virtual ~ReferenceCounted() { } // https://stackoverflow.com/questions/294927/does-delete-work-with-pointers-to-base-class
        void AddReference() { count++; total_count++; }
        long GetReferenceCount() { return count; }
        long DeleteReference();
        static long GetTotalReferenceCount() { return total_count; }
    private:
        long count;
        static long total_count;
};

template<class T> T* Copy(T* instance) {
    if (instance) {
        auto counted = (ReferenceCounted*) instance;
        counted->AddReference();
    }
    return instance;
}

void Delete(ReferenceCounted* instance);

}

#endif
