#include "control_combinator.h"
#include "hal.h"

namespace lisp {

bool If3::ProcessValues(Expression* condition, Expression* then, Expression* otherwise, Action* &cstack, List* &sstack, Environment* env) {
    if (condition) {
        return Push(new(std::nothrow) EvalAction(then, env), cstack);
    }
    return Push(new(std::nothrow) EvalAction(otherwise, env), cstack);
}

bool Or2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (expr1) {
        return Push(expr1, sstack);
    }
    return Push(new(std::nothrow) EvalAction(expr2, env), cstack);
}

bool Prog2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    return Push(new(std::nothrow) EvalAction(expr2, env), cstack);
}

bool Catch2::ProcessValues(Expression* symb, Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsNilOrSymbol(symb)) {
        return Push(new(std::nothrow) CatchAction((Symbol*) symb, sstack), cstack) && Push(new(std::nothrow) EvalAction(expr, env), cstack);
    }
    return false;
}

bool Throw2::ProcessValues(Expression* symb, Expression* val, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbol(symb)) {
        return Push(new(std::nothrow) ThrowAction((Symbol*) symb, val), cstack);
    }
    return false;
}

bool TypeOf1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    Symbol* value;
    if (expr) {
        switch (expr->GetType()) {
            case ExpressionType::LIST: {
                value = Intern("list");
                break;
            }
            case ExpressionType::SYMBOL: {
                value = Intern("atom");
                break;
            }
            case ExpressionType::INTEGER: {
                value = Intern("integer");
                break;
            }
            case ExpressionType::RATIO: {
                value = Intern("ratio");
                break;
            }
            case ExpressionType::FLOAT: {
                value = Intern("float");
                break;
            }
            case ExpressionType::STRING: {
                value = Intern("string");
                break;
            }
            case ExpressionType::LAMBDA: {
                value = Intern("lambda");
                break;
            }
            case ExpressionType::MACRO: {
                value = Intern("mlambda");
                break;
            }
            case ExpressionType::TASK: {
                value = Intern("task");
                break;
            }
            case ExpressionType::STREAM: {
                value = Intern("stream");
                break;
            }
            case ExpressionType::DISPLAY: {
                value = Intern("display");
                break;
            }
            case ExpressionType::GPIO: {
                value = Intern("gpio");
                break;
            }
            case ExpressionType::WLAN: {
                value = Intern("wlan");
                break;
            }
            case ExpressionType::SOCKET: {
                value = Intern("socket");
                break;
            }
            case ExpressionType::BROKER: {
                value = Intern("broker");
                break;
            }
            default: {
                value = nullptr;
            }
        }
    }
    else {
        value = Intern("list");
    }
    auto success = Push(value, sstack);
    Delete(value);
    return success;
}

static long current_number = 1;

bool GenSym0::Perform(Action* &cstack, List* &sstack, Environment* env) {
    std::string name = std::to_string(current_number++);
    while (name.length() < 4) {
        name.insert(0, "0");
    }
    name.insert(0, "g");
    auto value = Intern(name);
    auto success = Push(value, sstack);
    Delete(value);
    return success;
}

}
