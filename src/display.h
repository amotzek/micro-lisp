#ifndef DISPLAY_H
#define DISPLAY_H
#include <string>
#include "expression.h"
#include "pixel_map.h"

namespace lisp {

class Display : public Expression {
    public:
        Display(PixelMap* p) { pixels = p; pen_down = false; x_src = 0; y_src = 0; red = 0; green = 0; blue = 0; heading = 0.0d; }
        virtual ~Display() { delete pixels; }
        virtual ExpressionType GetType() { return ExpressionType::DISPLAY; }
        void GetOrigin(int &x, int &y) { pixels->GetOrigin(x, y); }
        int GetWidth() { return pixels->GetWidth(); }
        int GetHeight() { return pixels->GetHeight(); }
        bool GetPixel(int x, int y, int &r, int &g, int &b) const { return pixels->GetPixel(x, y, r, g, b); }
        bool SetPixel(int x, int y, int r, int g, int b) { return pixels->SetPixel(x, y, r, g, b); }
        bool Fill(int r, int g, int b) { return pixels->Fill(r, g, b); }
        bool IsPenDown() const { return pen_down; }
        bool IsPenUp() const { return !pen_down; }
        void PenDown() { pen_down = true; }
        void PenUp() { pen_down = false; }
        void SetPenColor(int r, int g, int b) { red = r; green = g; blue = b; }
        void SetHeading(double h) { heading = h; }
        bool Towards(int x, int y);
        void Left(double a) { heading += a; }
        void Right(double a) { heading -= a; }
        bool SetPosition(int x, int y);
        bool Forward(double d);
        bool Back(double d) { return Forward(-d); }
        bool Label(std::string s, int fs);
        bool Flush() { return pixels->Flush(); }
    private:
        PixelMap* pixels;
        bool pen_down;
        int x_src, y_src;
        int red, green, blue;
        double heading;
};

}

#endif
