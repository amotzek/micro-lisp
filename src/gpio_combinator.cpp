#include <new>
#include "gpio_combinator.h"
#include "gpio.h"
#include "rational.h"
#include "symbol.h"
#include "boolean.h"
#include "hal.h"

namespace lisp {

bool CreateGpio2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(expr1) && IsSymbol(expr2)) {
        auto pin = (Rational*) expr1;
        auto mode = (Symbol*) expr2;
        Gpio* gpio = nullptr;
        if (mode->HasName("analog-input")) {
            hal::PinModeInput(pin->GetNumerator());
            gpio = new(std::nothrow) Gpio(pin->GetNumerator(), GpioMode::ANALOG_INPUT);
        } else if (mode->HasName("digital-input")) {
            hal::PinModeInput(pin->GetNumerator());
            gpio = new(std::nothrow) Gpio(pin->GetNumerator(), GpioMode::DIGITAL_INPUT);
        } else if (mode->HasName("digital-input-pullup")) {
            hal::PinModeInputPullup(pin->GetNumerator());
            gpio = new(std::nothrow) Gpio(pin->GetNumerator(), GpioMode::DIGITAL_INPUT_PULLUP);
        } else if (mode->HasName("analog-output")) {
            hal::PinModeOutput(pin->GetNumerator());
            gpio = new(std::nothrow) Gpio(pin->GetNumerator(), GpioMode::ANALOG_OUTPUT);
        } else if (mode->HasName("digital-output")) {
            hal::PinModeOutput(pin->GetNumerator());
            gpio = new(std::nothrow) Gpio(pin->GetNumerator(), GpioMode::DIGITAL_OUTPUT);
        }
        if (gpio) {
            auto success = Push(gpio, sstack);
            Delete(gpio);
            return success;
        }
    }
    return false;
}

bool ReadGpio1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsGpio(expr)) {
        auto gpio = (Gpio*) expr;
        auto success = false;
        switch (gpio->GetMode()) {
            case GpioMode::ANALOG_INPUT: {
                auto value = new(std::nothrow) Rational(hal::AnalogRead(gpio->GetPin()), 1l);
                success = value && Push(value, sstack);
                Delete(value);
                break;
            }
            case GpioMode::DIGITAL_INPUT:
            case GpioMode::DIGITAL_INPUT_PULLUP: {
                if (hal::DigitalRead(gpio->GetPin())) {
                    success = PushTrue(sstack);
                } else {
                    success = PushFalse(sstack);
                }
                break;
            }
            case GpioMode::ANALOG_OUTPUT:
            case GpioMode::DIGITAL_OUTPUT: {
                success = PushFalse(sstack);
                break;
            }
        }
        return success;
    }
    return false;
}

bool WriteGpio2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsGpio(expr1)) {
        auto gpio = (Gpio*) expr1;
        auto success = false;
        switch (gpio->GetMode()) {
            case GpioMode::ANALOG_INPUT:
            case GpioMode::DIGITAL_INPUT:
            case GpioMode::DIGITAL_INPUT_PULLUP:
            case GpioMode::ANALOG_OUTPUT: { // Analog Output bei ESP32 derzeit nicht unterstützt
                success = PushFalse(sstack);
                break;
            }
            case GpioMode::DIGITAL_OUTPUT: {
                hal::DigitalWrite(gpio->GetPin(), expr2);
                success = Push(gpio, sstack);
                break;
            }
        }
        return success;
    }
    return false;
}

}
