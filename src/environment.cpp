#include "environment.h"

namespace lisp {

bool Environment::AddPair(Symbol* key, Expression* value) {
    List* p;
    auto pl = GetBucket(key);
    bool success = false;
    if (Pair(key, value, p)) {
        if (Push(p, pl)) {
            SetBucket(key, pl);
            success = true;
        }
        Delete(p);
    }
    return success;
}

bool Environment::ModifyOrAddPair(Symbol* key, Expression* value) {
    auto e = this;
    Environment* root = nullptr;
    while (e) {
        root = e;
        auto pl = e->GetBucket(key);
        while (pl) {
            auto p = (List*) pl->GetFirst();
            auto k = (Symbol*) p->GetFirst();
            if (key == k) {
                auto r = p->GetRest();
                r->SetFirst(value);
                return true;
            }
            pl = pl->GetRest();
        }
        e = e->parent;
    }
    return root->AddPair(key, value);
}

bool Environment::RemovePair(Symbol* key) {
    auto e = this;
    while (e) {
        auto pl = e->GetBucket(key);
        if (pl) {
            auto p = (List*) pl->GetFirst();
            auto k = (Symbol*) p->GetFirst();
            if (key == k) {
                auto r = Copy(pl->GetRest());
                Delete(pl);
                e->SetBucket(key, r);
                return true;
            }
        }
        auto last_pl = pl;
        pl = pl->GetRest();
        while (pl) {
            auto p = (List*) pl->GetFirst();
            auto k = (Symbol*) p->GetFirst();
            if (key == k) {
                last_pl->SetRest(pl->GetRest());
                return true;
            }
            last_pl = pl;
            pl = pl->GetRest();
        }
        e = e->parent;
    }
    return false;
}

bool Environment::Lookup(Symbol* key, Expression* &value) {
    auto e = this;
    while (e) {
        auto pl = e->GetBucket(key);
        while (pl) {
            auto p = (List*) pl->GetFirst();
            auto k = (Symbol*) p->GetFirst();
            if (key == k) {
                auto r = p->GetRest();
                value = r->GetFirst();
                return true;
            }
            pl = pl->GetRest();
        }
        e = e->parent;
    }
    return false;
}

}
