#ifndef WLAN_H
#define WLAN_H
#include <string>
#include <list>
#include <tuple>
#include "expression.h"

namespace lisp {

class Wlan : public Expression {
    public:
        Wlan(std::string h, std::list<std::tuple<std::string, std::string>>* c) { hostname = h; credentials = c; }
        virtual ~Wlan() { delete credentials; }
        virtual ExpressionType GetType() { return ExpressionType::WLAN; }
        std::string GetHostname() { return hostname; }
        std::list<std::tuple<std::string, std::string>>* GetCredentials() { return credentials; }
    private:
        std::string hostname;
        std::list<std::tuple<std::string, std::string>>* credentials;
};

}

#endif
