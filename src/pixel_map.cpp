#include "pixel_map.h"

namespace lisp {

bool PixelMap::Fill(int r, int g, int b) {
    for (int x = 0; x < GetWidth(); x++) {
        for (int y = 0; y < GetHeight(); y++) {
            if (!SetPixel(x, y, r, g, b)) {
                return false;
            }
        }
    }
    return true;
}

bool PixelMap::Flush() {
    if (next) {
        for (int x = 0; x < GetWidth(); x++) {
            for (int y = 0; y < GetHeight(); y++) {
                int r, g, b;
                if (!GetPixel(x, y, r, g, b) || !next->SetPixel(x, y, r, g, b)) {
                    return false;
                }
            }
        }
    }
    return true;
}

bool MonochromePixelMap::GetPixel(int x, int y, int &r, int &g, int &b) const {
    if (pixels && IsInside(x, y)) {
        if ((*pixels)[x][y]) {
            r = 255;
            g = 255;
            b = 255;
        } else {
            r = 0;
            g = 0;
            b = 0;
        }
        return true;
    }
    return false;
}

bool MonochromePixelMap::SetPixel(int x, int y, int r, int g, int b) {
    if (pixels && IsInside(x, y)) {
        if (r > 0 || g > 0 || b > 0) {
            (*pixels)[x][y] = true;
        } else {
            (*pixels)[x][y] = false;
        }
        return true;
    }
    return false;
}

}
