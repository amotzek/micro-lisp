#ifndef WLAN_COMBINATOR_H
#define WLAN_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"

namespace lisp {

class CreateWlan2 : public BinaryCombinator {
    public:
        CreateWlan2() { }
        virtual ~CreateWlan2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class ConnectWlan1 : public UnaryCombinator {
    public:
        ConnectWlan1() { }
        virtual ~ConnectWlan1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class IsConnectedWlan1 : public UnaryCombinator {
    public:
        IsConnectedWlan1() { }
        virtual ~IsConnectedWlan1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
