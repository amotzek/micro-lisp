#ifndef CORE_H
#define CORE_H
#include <istream>

namespace lisp {

std::istream* CreateCore();

}

#endif
