#ifndef MACRO_H
#define MACRO_H
#include "environment.h"

namespace lisp {

class Macro : public Expression {
    public:
        Macro(Symbol* p, Expression* b, Environment* e) { parameter = Copy(p); body = Copy(b); environment = Copy(e); }
        virtual ~Macro() { Delete(parameter); Delete(body); Delete(environment); }
        virtual ExpressionType GetType() { return ExpressionType::MACRO; }
        Symbol* GetParameter() { return parameter; }
        Expression* GetBody() { return body; }
        Environment* GetEnvironment() { return environment; }
    private:
        Symbol* parameter;
        Expression* body;
        Environment* environment;
};

}

#endif
