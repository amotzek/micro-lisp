#include <new>
#include "socket_combinator.h"
#include "string.h"
#include "rational.h"
#include "socket.h"
#include "boolean.h"
#include "hal.h"

namespace lisp {

bool CreateSocket2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr1) && IsInteger(expr2)) {
        auto hostname = (String*) expr1;
        auto port = ((Rational*) expr2)->GetNumerator();
        if (hal::IsWlanConnected() && port > 0 && port < 65536) {
            int fd = hal::CreateSocket(hostname->GetString(), port);
            if (fd >= 0) {
                auto socket = new(std::nothrow) Socket(fd);
                if (socket) {
                    auto success = Push(socket, sstack);
                    Delete(socket);
                    return success;
                } else {
                    hal::CloseSocket(fd);
                }
            } else {
                return PushFalse(sstack);
            }
        } else {
            return PushFalse(sstack);
        }
    }
    return false;
}

}
