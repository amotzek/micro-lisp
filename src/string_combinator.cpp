#include <new>
#include <sstream>
#include "string_combinator.h"
#include "string.h"
#include "symbol.h"
#include "rational.h"
#include "parser.h"
#include "formatter.h"
#include "json.h"
#include "boolean.h"
#include "hal.h"

namespace lisp {

bool Intern1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
       auto string = ((String*) expr)->GetString();
       if (string.length() > 0) {
           auto symbol = Intern(string);
           auto success = symbol && Push(symbol, sstack);
           Delete(symbol);
           return success;
       }
    }
    return false;
}

bool CharCode1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
       auto string = ((String*) expr)->GetString();
       if (string.length() > 0) {
           auto integer = new(std::nothrow) Rational(string[0], 1l);
           auto success = integer && Push(integer, sstack);
           Delete(integer);
           return success;
       }
    }
    return false;
}

bool CodeChar1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(expr)) {
        std::string string1;
        string1.push_back((char) ((Rational*) expr)->GetNumerator());
        auto string2 = new(std::nothrow) String(string1);
        auto success = string2 && Push(string2, sstack);
        Delete(string2);
        return success;
    }
    return false;
}

bool Concatenate2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr1) && IsString(expr2)) {
        auto string1 = ((String*) expr1)->GetString();
        auto string2 = ((String*) expr2)->GetString();
        auto concstring = new(std::nothrow) String(string1 + string2);
        auto success = concstring && Push(concstring, sstack);
        Delete(concstring);
        return success;
    }
    return false;
}

bool Substring3::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr1) && IsInteger(expr2) && IsInteger(expr3)) {
        auto string = ((String*) expr1)->GetString();
        auto pos = ((Rational*) expr2)->GetNumerator();
        auto len = ((Rational*) expr3)->GetNumerator();
        if (pos > 0 && len >= 0 && (ulong) pos + (ulong) len < string.length() + 1) {
            auto substring = new(std::nothrow) String(string.substr(pos - 1, len));
            auto success = substring && Push(substring, sstack);
            Delete(substring);
            return success;
        }
    }
    return false;
}

bool StringLength1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto string = ((String*) expr)->GetString();
        auto integer = new(std::nothrow) Rational(string.length(), 1l);
        auto success = integer && Push(integer, sstack);
        Delete(integer);
        return success;
    }
    return false;
}

bool ReadFromString1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto string = ((String*) expr)->GetString();
        auto istream = new(std::nothrow) std::istringstream(string);
        auto success = false;
        if (istream) {
            auto parser = Parser(istream);
            auto expr = parser.ReadExpression();
            success = parser.GetState() != ParserState::ERROR && Push(expr, sstack);
            Delete(expr);
            delete istream;
        }
        return success;
    }
    return false;
}

bool WriteToString1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    auto ostream = new(std::nothrow) std::ostringstream();
    if (ostream) {
        *ostream << expr;
        auto string = new(std::nothrow) String(ostream->str());
        auto success = string && Push(string, sstack);
        Delete(string);
        delete ostream;
        return success;
    }
    return false;
}

bool GetEnv1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto name = (String*) expr;
        auto value = new(std::nothrow) String(hal::GetEnv(name->GetString()));
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool ParseJson1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto string = ((String*) expr)->GetString();
        Expression* value = nullptr;
        if (json::FromJson(string, value)) {
            auto success = Push(value, sstack);
            Delete(value);
            return success;
        }
        return PushFalse(sstack);
    }
    return false;
}

}
