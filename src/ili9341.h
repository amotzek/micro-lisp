#ifndef ILI9341_H
#define ILI9341_H
#include "pixel_map.h"

namespace ili9341 {

class ILI9341 : public lisp::FixedSizePixelMap {
    public:
        ILI9341(int w, int h, bool ma, bool md, int s, int i, int o, int c, int d, int r, int b, bool v) : FixedSizePixelMap(w, h) { mirror_axis = ma; mirror_diagonal = md; sck = s; miso = i; mosi = o; cs = c; dc = d; rst = r; bl = b; inverted = v; }
        virtual ~ILI9341() { }
        virtual bool GetPixel(int x, int y, int &r, int &g, int &b) const { return false; }
        virtual bool SetPixel(int x, int y, int r, int g, int b);
        virtual bool Fill(int r, int g, int b);
        virtual void Init();
    private:
        bool mirror_axis;
        bool mirror_diagonal;
        int sck;
        int miso;
        int mosi;
        int cs;
        int dc;
        int rst;
        int bl;
        bool inverted;
};

}

#endif
