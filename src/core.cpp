#include <new>
#include <streambuf>
#include <cstring>
#include "core.h"

namespace lisp {

const char* core = R"#(

(setq t (quote t))

(setq list2 (lambda (e1 e2) (cons e1 (cons e2 nil))))
(setq list3 (lambda (e1 e2 e3) (cons e1 (list2 e2 e3))))
(setq list4 (lambda (e1 e2 e3 e4) (cons e1 (list3 e2 e3 e4))))
(setq second (lambda (l) (first (rest l))))
(setq third (lambda (l) (first (rest (rest l)))))

(setq deffn
  (mlambda args
    (if
      (equal? (list-length args) 3)
      (list3
        setq
        (first args)
        (list3 lambda (second args) (third args)))
      (throw (quote error) "wrong number of arguments for deffn"))))

(deffn fourth (l) (first (rest (rest (rest l)))))
(deffn fifth (l) (first (rest (rest (rest (rest l))))))
(deffn sixth (l) (first (rest (rest (rest (rest (rest l)))))))
(deffn seventh (l) (first (rest (rest (rest (rest (rest (rest l))))))))
(deffn eighth (l) (first (rest (rest (rest (rest (rest (rest (rest l)))))))))
(deffn ninth (l) (first (rest (rest (rest (rest (rest (rest (rest (rest l))))))))))
(deffn tenth (l) (first (rest (rest (rest (rest (rest (rest (rest (rest (rest l)))))))))))

(deffn map-with (f l)
  (if
    (null? l)
    nil
    (cons
      (f (first l))
      (map-with f (rest l)))))

(setq defmacro
  (mlambda args
    (if
      (equal? (list-length args) 3)
      (list3
        setq
        (first args)
        (list3 mlambda (second args) (third args)))
      (throw (quote error) "wrong number of arguments for defmacro"))))

(defmacro let args
  (cons
    (list3
      lambda
      (map-with first (first args))
      (second args))
	(map-with second (first args))))

(defmacro list args
  (let
    ((length (list-length args)))
    (if
      (equal? length 0)
      nil
      (if
        (equal? length 1)
        (list3 cons (first args) nil)
        (if
          (equal? length 2)
          (list3 list2 (first args) (second args))
          (if
            (equal? length 3)
            (list4 list3 (first args) (second args) (third args))
            (list3 cons (first args) (cons (quote list) (rest args)))))))))

(defmacro or args
  (if
    (null? args)
    nil
    (list3
      or2
      (first args)
      (cons (quote or) (rest args)))))

(deffn single? (l) (if l (null? (rest l)) nil))

(defmacro and args
  (if
    (null? args)
    (quote t)
    (if
      (single? args)
      (first args)
      (list4
        if
        (first args)
        (cons (quote and) (rest args))
        nil))))

(defmacro cond args
  (if
    (null? args)
    nil
    (if
      (single? (first args))
      (list3
        or2
        (first (first args))
        (cons (quote cond) (rest args)))
      (list4
        if
        (first (first args))
        (second (first args))
        (cons (quote cond) (rest args))))))

(defmacro - args
  (cond
    ((null? args) 0)
    ((single? args) (list3 (quote sub) 0 (first args)))
    ((list3
      (quote sub)
      (first args)
      (cons (quote +) (rest args))))))

(defmacro / args
  (cond
    ((null? args) 1)
    ((single? args) (list3 (quote quotient) 1 (first args)))
    ((list3
      (quote quotient)
      (first args)
      (cons (quote *) (rest args))))))

(deffn list? (x) (equal? (type-of x) (quote list)))
(deffn atom? (x) (or (null? x) (not (list? x))))
(deffn integer? (x) (equal? (type-of x) (quote integer)))
(deffn number? (x) (or (equal? (type-of x) (quote integer)) (equal? (type-of x) (quote ratio)) (equal? (type-of x) (quote float))))
(deffn string? (x) (equal? (type-of x) (quote string)))
(deffn symbol? (x) (equal? (type-of x) (quote atom)))
(deffn greater? (x y) (less? y x))
(deffn greater-or-equal? (x y) (not (less? x y)))
(deffn less-or-equal? (x y) (not (less? y x)))

(deffn abs (x) (if (less? x 0) (sub 0 x) x))
(deffn zero? (x) (equal? x 0))
(deffn even? (n) (integer? (quotient n 2)))
(deffn odd? (n) (not (even? n)))

(deffn zip-with (f l r)
  (cond
    ((null? l) nil)
    ((null? r) nil)
    ((cons
      (f (first l) (first r))
      (zip-with f (rest l) (rest r))))))

(deffn member? (x l)
  (cond
    ((null? l) nil)
    ((equal? x (first l)) l)
    ((member? x (rest l)))))

(deffn member-if? (p l)
  (cond
    ((null? l) nil)
    ((p (first l)) l)
    ((member-if? p (rest l)))))

(deffn find-if (p l)
  (let
    ((m (member-if? p l)))
    (and m (first m))))

(deffn adjoin (x l)
  (if (member? x l) l (cons x l)))

(deffn union (u v)
  (if
    (null? u)
    v
    (union (rest u) (adjoin (first u) v))))

(deffn intersection (u v)
  (cond
    ((null? u) nil)
    ((member? (first u) v)
      (cons (first u) (intersection (rest u) v)))
    ((intersection (rest u) v))))

(deffn set-difference (u v)
  (remove-if
    (lambda (x) (member? x v))
    u))

(deffn set-exclusive-or (u v)
  (union
    (set-difference u v)
    (set-difference v u)))

(deffn subset? (u v)
  (cond
    ((null? u) t)
    ((member? (first u) v)
     (subset? (rest u) v))))

(deffn disjoint? (u v)
  (not
    (member-if?
      (lambda (element) (member? element v))
      u)))

(deffn append2 (l1 l2)
  (cond
    ((null? l1) l2)
    ((null? l2) l1)
    ((cons
      (first l1)
      (append2 (rest l1) l2)))))

(deffn nthrest (l n)
  (if
    (equal? n 0)
    l
    (nthrest (rest l) (sub n 1))))

(deffn nth (l n)
  (if
    (equal? n 0)
    (first l)
    (nth (rest l) (sub n 1))))

(deffn range (from to)
  (if
    (less? from to)
    (cons from (range (add 1 from) to))
    nil))

(deffn select-if (p l)
  (if
    (null? l)
    nil
    (if
      (p (first l))
      (cons
        (first l)
        (select-if p (rest l)))
      (select-if p (rest l)))))

(deffn remove-if (p l)
  (cond
    ((null? l) nil)
    ((p (first l)) (remove-if p (rest l)))
    ((cons
       (first l)
       (remove-if p (rest l))))))

(deffn remove (x l)
  (remove-if (lambda (y) (equal? x y)) l))

(deffn tail? (sh lo)
  (cond
    ((equal? sh lo))
    ((null? lo) nil)
    ((tail? sh (rest lo)))))

(deffn some? (fn l)
  (cond
    ((null? l) nil)
    ((fn (first l)) (first l))
    ((some? fn (rest l)))))

(deffn every? (fn l)
  (cond
    ((null? l))
    ((not (fn (first l))) nil)
    ((every? fn (rest l)))))

(deffn last (l)
  (if  (null? (rest l)) l (last (rest l))))

(deffn butlast (l)
  (reverse
    (rest
      (reverse l))))

(setq quasi-quote
  (let
    ((quasi-quote-list
      (lambda (elem)
        (list2
          (quote quasi-quote)
          elem))))
    (mlambda args
      (let
        ((expr (first args)))
        (if
          (null? expr)
          nil
          (if
            (list? expr)
            (if
              (equal? (first expr) (quote unquote))
              (second expr)
              (cons
                list
                (map-with quasi-quote-list expr)))
            (list2 quote expr)))))))

(defmacro aif args
  (quasi-quote
    (let
      ((it (unquote (first args))))
      (if
        it
        (unquote (second args))
        (unquote (third args))))))

(defmacro aand args
  (cond
    ((null? args) t)
    ((single? args) (first args))
    (t
      (list4
        aif
        (first args)
        (cons aand (rest args))
        nil))))

(defmacro deffoldr outer-args
  (let
    ((f (gensym)))
    (quasi-quote
      (setq
        (unquote (first outer-args))
        (let
          (((unquote f) (unquote (second outer-args))))
          (mlambda args
            (cond
              ((null? args)
                (unquote (third outer-args)))
              ((single? args)
                (first args))
              ((single? (rest args))
                (list3
                  (unquote f)
                  (first args)
                  (second args)))
              (t
                (list3
                  (unquote f)
                  (first args)
                  (cons
                    (quote (unquote (first outer-args)))
                    (rest args)))))))))))

(deffoldr append append2 nil)

(deffoldr + add 0)

(deffoldr * times 1)

(deffoldr concatenate concatenate2 "")

(deffoldr min (lambda (x y) (if (less? x y) x y)) (throw (quote error) "min needs at least one argument"))

(deffoldr max (lambda (x y) (if (greater? x y) x y)) (throw (quote error) "max needs at least one argument"))

(deffoldr progn prog2 nil)

(defmacro defproc args
  (if
    (greater-or-equal? (list-length args) 3)
    (list3
      setq
      (first args)
      (list3 lambda (second args) (cons progn (rest (rest args)))))
    (throw (quote error) "too few arguments for defproc")))

(defmacro when args
  (list4
    if
    (first args)
    (cons progn (rest args))
    nil))

(defmacro awhen args
  (list4
    aif
    (first args)
    (cons progn (rest args))
    nil))

(defmacro unless args
  (list4
    if
    (first args)
    nil
    (cons progn (rest args))))

(defmacro only-one-of args
  (list2
    only-one-of-list
    (cons list args)))

(defmacro defdisplay args
  (list3
     setq
     (first args)
     (cons create-display
       (cons (list quote (second args))
          (cons (list quote (rest (rest args)))
             nil)))))

(defmacro defgpio args
  (list3
     setq
     (first args)
     (cons create-gpio
       (cons (second args)
         (cons (list2 quote (third args))
           (rest (rest (rest args))))))))

(defmacro defwlan args
  (list3
     setq
     (first args)
     (list3 create-wlan (second args) (third args))))

)#";

class CharPBuf : public std::streambuf {
    public:
        CharPBuf(const char* charp, size_t len) {
            char* p(const_cast<char*>(charp));
            this->setg(p, p, p + len);
        }
        virtual ~CharPBuf() { this->setg(nullptr, nullptr, nullptr); }
};

class ICharPStream : virtual CharPBuf, public std::istream {
    public:
        ICharPStream(const char* charp, size_t len) : CharPBuf(charp, len), std::istream(static_cast<std::streambuf*>(this)) { }
};

std::istream* CreateCore() {
    return new(std::nothrow) ICharPStream(core, strlen(core));
}

}
