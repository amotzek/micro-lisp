#ifndef SYMBOL_H
#define SYMBOL_H
#include <string>
#include "expression.h"

namespace lisp {

class Symbol : public Expression {
    public:
        Symbol(const std::string &n, int h) { name = n; hash = h; }
        virtual ~Symbol() { }
        virtual ExpressionType GetType() { return ExpressionType::SYMBOL; }
        bool HasName(const std::string &n) { return name.compare(n) == 0; }
        std::string GetName() { return name; }
        int GetHash() { return hash; }
    private:
        std::string name;
        int hash;
};

Symbol* Intern(const std::string &name);

void GarbageCollectSymbols();

}

#endif
