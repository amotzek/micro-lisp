#ifdef ARDUINO
#include <new>
#include <list>
#include <tuple>
#include "WiFi.h"

namespace hal {

std::list<std::tuple<int, WiFiClient*>> clients_by_fd;

int CreateSocket(std::string hostname, uint16_t port) {
    auto client = new(std::nothrow) WiFiClient();
    if (client) {
        if (client->connect(hostname.c_str(), port, 3000)) {
            int fd = client->fd();
            clients_by_fd.push_back(std::make_tuple(fd, client));
            return fd;
        }
        delete client;
    }
    return -1;
}

WiFiClient* GetClient(int fd) {
    for (auto pair : clients_by_fd) {
        if (fd == std::get<0>(pair)) {
            return std::get<1>(pair);
        }
    }
    return nullptr;
}

bool WriteSocket(int fd, const uint8_t *buffer, size_t size) {
    auto client = GetClient(fd);
    if (client) {
        auto res = client->write(buffer, size);
        return res == size;
    }
    return false;
}

bool WriteSocket(int fd, uint8_t data) {
    return WriteSocket(fd, &data, 1);
}

bool AvailableReadSocket(int fd, int size) {
    auto client = GetClient(fd);
    if (client) {
        auto res = client->available();
        return res >= size;
    }
    return false;
}

bool ReadSocket(int fd, uint8_t *buffer, size_t size) {
    auto client = GetClient(fd);
    if (client) {
        off_t offset = 0;
        while (size > 0) {
            auto res = client->read(buffer + offset, size);
            if (res <= 0) {
                return false;
            }
            size -= res;
            offset += res;
        }
        return true;
    }
    return false;
}

int ReadSocket(int fd) {
    uint8_t data;
    if (ReadSocket(fd, &data, 1)) {
        return data;
    }
    return -1;
}

void CloseSocket(int fd) {
    clients_by_fd.remove_if([fd](std::tuple<int, WiFiClient*> pair) {
        if (fd == std::get<0>(pair)) {
            delete std::get<1>(pair);
            return true;
        }
        return false;
    });
}

}

#endif
