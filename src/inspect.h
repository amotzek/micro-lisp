#ifndef INSPECT_H
#define INSPECT_H
#include "expression.h"

namespace lisp {

void Inspect(Expression* expression);

}

#endif
