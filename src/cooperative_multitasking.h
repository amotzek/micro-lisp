#ifndef COOPERATIVE_MULTITASKING_H
#define COOPERATIVE_MULTITASKING_H
#include <queue>
#include <vector>
#include <algorithm>
#include "environment.h"

namespace lisp {

class Task : public Expression {
    public:
        Task(long p, long w, long d) {
            priority = p;
            when = w;
            duration = d;
            remaining = d;
            sibling1 = nullptr;
            sibling2 = nullptr;
            sibling3 = nullptr;
        }
        virtual ~Task() { }
        virtual ExpressionType GetType() { return ExpressionType::TASK; }
        virtual long GetCycle() = 0;
        virtual bool HasNoGuard() = 0;
        virtual bool EvalGuard() = 0;
        virtual void Execute() = 0;
        long GetWhen() const { return when; }
        long GetPriority() const { return priority; }
        bool IsRemainingLessThan(long r) const { return remaining < r; }
        void DecreaseRemaining(long r) { remaining -= r; }
        void ResetRemaining() { remaining = duration; }
        void SetWhen(long w) { when = w; }
        void AddSibling(Task* s) {
            if (!sibling1) {
                sibling1 = s;
                return;
            }
            if (!sibling2) {
                sibling2 = s;
                return;
            }
            if (!sibling3) {
                sibling3 = s;
            }
        }
        Task* GetSibling1() const { return sibling1; }
        Task* GetSibling2() const { return sibling2; }
        Task* GetSibling3() const { return sibling3; }
    private:
        long priority;
        Task* sibling1;
        Task* sibling2;
        Task* sibling3;
        long when;
        long duration;
        long remaining;
};

long TimeDiff(long ms1, long ms2);

long TimeSum(long ms1, long ms2);

struct TaskComparator {
    bool operator() (const Task* task1, const Task* task2) {
        return TimeDiff(task1->GetWhen(), task2->GetWhen()) > 0 || (task1->GetWhen() == task2->GetWhen() && task1->GetPriority() < task2->GetPriority());
    }
};

class TaskQueue : public std::priority_queue<Task*, std::vector<Task*>, TaskComparator> {
    public:
        bool contains(Task* task) {
            auto iterator = std::find(this->c.begin(), this->c.end(), task);
            return iterator != this->c.end();
        }
        bool remove(Task* task) {
            auto iterator = std::find(this->c.begin(), this->c.end(), task);
            if (iterator == this->c.end()) {
                return false;
            }
            this->c.erase(iterator);
            std::make_heap(this->c.begin(), this->c.end(), this->comp);
            return true;
       }
};

class Tasks {
    public:
        Tasks() { current = 0l; }
        ~Tasks() { Drain(); }
        Task* Push(Task* task) {
            if (task) {
                queue.push(Copy(task));
            }
            return task;
        }
        Task* Now(long p, Expression* c, Environment* e);
        Task* After(long p, long d, Expression* c, Environment* e);
        Task* WhenForThen(long p, Expression* g, long d, Expression* c, Environment* e);
        Task* WhenThen(long p, Expression* g, Expression* c, Environment* e) { return WhenForThen(p, g, 0, c, e); }
        bool Contains(Task* task) { return queue.contains(task); }
        bool OnlyOneOf(Task* task1, Task* task2, Task* task3, Task* task4);
        long GetCurrentTime();
        bool HasTask(long d);
        bool Run();
        int Drain();
    private:
        bool Wait();
        void MarkDueTasks();
        Task* ExtractReadyTask();
        void CancelSiblings(Task* task);
        void CancelTask(Task* task);
        TaskQueue queue;
        long current;
};

extern Tasks tasks;

}

#endif
