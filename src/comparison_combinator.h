#ifndef COMPARISON_COMBINATOR_H
#define COMPARISON_COMBINATOR_H
#include "binary_combinator.h"

namespace lisp {

bool IsEqual(Expression* e1, Expression* e2);

class IsEqual2 : public BinaryCombinator {
    public:
        IsEqual2() { }
        virtual ~IsEqual2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class IsLess2 : public BinaryCombinator {
    public:
        IsLess2() { }
        virtual ~IsLess2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
