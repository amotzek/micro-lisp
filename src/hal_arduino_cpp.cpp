#ifdef ARDUINO
#include <string>
#include "stdlib_noniso.h"
#include "reference_counted.h"

namespace lisp {

std::string GetTypeName(ReferenceCounted* instance) {
    return "this class"; // keine RTTI
}

}

namespace std {

string to_string(long a) {
    char buffer[20];
    ltoa(a, buffer, 10);
    return string(buffer);
}

}

long stol(std::string s) {
    return atol(s.data());
}

double stod(std::string s) {
    return atof(s.data());
}

#endif
