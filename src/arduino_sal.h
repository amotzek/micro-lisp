#ifdef ARDUINO

void HardwareSetup();

void LoadSetup();

void ReadEvalPrintConsumeChar(char c);

bool HasTask(long d);

void RunTask();

#endif