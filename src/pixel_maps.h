#ifndef PIXEL_MAPS_H
#define PIXEL_MAPS_H
#include "pixel_map.h"
#include "expression.h"

namespace ili9341 {

lisp::PixelMap* CreatePixelMap(lisp::Expression* expr);

}

namespace jd79653 {

lisp::PixelMap* CreatePixelMap(lisp::Expression* expr);

}

#endif
