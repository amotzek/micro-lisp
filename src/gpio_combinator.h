#ifndef GPIO_COMBINATOR_H
#define GPIO_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"

namespace lisp {

class CreateGpio2 : public BinaryCombinator {
    public:
        CreateGpio2() { }
        virtual ~CreateGpio2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class ReadGpio1 : public UnaryCombinator {
    public:
        ReadGpio1() { }
        virtual ~ReadGpio1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class WriteGpio2 : public BinaryCombinator {
    public:
        WriteGpio2() { }
        virtual ~WriteGpio2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
