#include "expression.h"

namespace lisp {

bool IsNilOrList(Expression* expr) {
    if (expr) {
        return expr->HasType(ExpressionType::LIST);
    }
    return true;
}

bool IsList(Expression* expr) {
    return expr && expr->HasType(ExpressionType::LIST);
}

bool IsNilOrSymbol(Expression* expr) {
    if (expr) {
        return expr->HasType(ExpressionType::SYMBOL);
    }
    return true;
}

bool IsSymbol(Expression* expr) {
    return expr && expr->HasType(ExpressionType::SYMBOL);
}

bool IsInteger(Expression* expr) {
    return expr && expr->HasType(ExpressionType::INTEGER);
}

bool IsRational(Expression* expr) {
    return expr && (expr->HasType(ExpressionType::INTEGER) || expr->HasType(ExpressionType::RATIO));
}

bool IsFloat(Expression* expr) {
    return expr && expr->HasType(ExpressionType::FLOAT);
}

bool IsNumber(Expression* expr) {
    return IsRational(expr) || IsFloat(expr);
}

bool IsString(Expression* expr) {
    return expr && expr->HasType(ExpressionType::STRING);
}

bool IsFunction(Expression* expr) {
    return expr && (expr->HasType(ExpressionType::LAMBDA) || expr->HasType(ExpressionType::MACRO) || expr->HasType(ExpressionType::COMBINATOR));
}

bool IsTask(Expression* expr) {
    return expr && expr->HasType(ExpressionType::TASK);
}

bool IsStream(Expression* expr) {
    return expr && expr->HasType(ExpressionType::STREAM);
}

bool IsGpio(Expression* expr) {
    return expr && expr->HasType(ExpressionType::GPIO);
}

bool IsDisplay(Expression* expr) {
    return expr && expr->HasType(ExpressionType::DISPLAY);
}

bool IsWlan(Expression* expr) {
    return expr && expr->HasType(ExpressionType::WLAN);
}

bool IsSocket(Expression* expr) {
    return expr && expr->HasType(ExpressionType::SOCKET);
}

bool IsBroker(Expression* expr) {
    return expr && expr->HasType(ExpressionType::BROKER);
}

}
