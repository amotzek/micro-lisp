#ifndef ARITHMETIC_COMBINATOR_H
#define ARITHMETIC_COMBINATOR_H
#include <random>
#include "unary_combinator.h"
#include "binary_combinator.h"
#include "number.h"

namespace lisp {

class ArithmeticCombinator : public BinaryCombinator {
   public:
       ArithmeticCombinator() { }
       virtual ~ArithmeticCombinator() { }
       virtual bool MustBeQuoted(int argument_index) { return false; }
       virtual Number* Operate(long n1, long d1, long n2, long d2) = 0;
       virtual Number* Operate(double d1, double d2) = 0;
       virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Add2 : public ArithmeticCombinator {
   public:
       Add2() { }
       virtual ~Add2() { }
       virtual Number* Operate(long n1, long d1, long n2, long d2);
       virtual Number* Operate(double d1, double d2);
};

class Sub2 : public ArithmeticCombinator {
   public:
       Sub2() { }
       virtual ~Sub2() { }
       virtual Number* Operate(long n1, long d1, long n2, long d2);
       virtual Number* Operate(double d1, double d2);
};

class Times2 : public ArithmeticCombinator {
   public:
       Times2() { }
       virtual ~Times2() { }
       virtual Number* Operate(long n1, long d1, long n2, long d2);
       virtual Number* Operate(double d1, double d2);
};

class Quotient2 : public ArithmeticCombinator {
   public:
       Quotient2() { }
       virtual ~Quotient2() { }
       virtual Number* Operate(long n1, long d1, long n2, long d2);
       virtual Number* Operate(double d1, double d2);
};

class Floor1 : public UnaryCombinator {
   public:
       Floor1() { }
       virtual ~Floor1() { }
       virtual bool MustBeQuoted(int argument_index) { return false; }
       virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Random1 : public UnaryCombinator {
   public:
       Random1() { }
       virtual ~Random1() { }
       virtual bool MustBeQuoted(int argument_index) { return false; }
       virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
   private:
       std::shuffle_order_engine<std::minstd_rand0, 61> generator;
};

}

#endif
