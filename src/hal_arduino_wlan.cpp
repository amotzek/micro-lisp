#ifdef ARDUINO
#include <string>
#include <list>
#include <tuple>
#include "WiFi.h"

namespace hal {

void SetupWlan() {
    WiFi.persistent(false);
    WiFi.enableAP(false);
    WiFi.enableSTA(false);
}

bool SetHostname(std::string hostname) {
    WiFi.setHostname(hostname.c_str());
    return true;
}

bool ConnectWlan(std::list<std::tuple<std::string, std::string>>* credentials) {
    WiFi.enableSTA(true);
    int count = WiFi.scanNetworks();
    for (int i = 0; i < count; i++) {
        auto ssid1 = std::string(WiFi.SSID(i).c_str());
        for (auto credential : *credentials) {
            auto ssid2 = std::get<0>(credential);
            if (ssid2.compare(ssid1) == 0) {
                auto password = std::get<1>(credential);
                WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE); // siehe https://github.com/espressif/arduino-esp32/issues/806
                WiFi.begin(ssid2.c_str(), password.c_str());
                return true;
            }
        }
    }
    return false;
}

bool IsWlanConnected() {
    return WiFi.isConnected();
}

}

#endif
