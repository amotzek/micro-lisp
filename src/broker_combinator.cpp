#include <new>
#include <string>
#include "broker_combinator.h"
#include "list.h"
#include "broker.h"
#include "string.h"
#include "mqtt.h"
#include "cooperative_multitasking.h"
#include "boolean.h"
#include "json.h"
#include "parser.h"
#include "hal.h"

namespace lisp {

bool IsSocketOrNil(Expression* expr) {
    return !expr || IsSocket(expr);
}

bool IsSingletonSymbolList(Expression* expr) {
    if (!IsList(expr)) {
        return false;
    }
    auto list = (List*) expr;
    return IsSymbol(list->GetFirst()) && !list->GetRest();
}

bool IsEmpty(std::string str) {
    return str.length() == 0 || str.compare("{}") == 0;
}

bool IsJson(std::string str) {
    char c = str[0];
    return c == '{' || c == '[';
}

class AwaitResponse : public Task {
    public:
        AwaitResponse(Broker* b, long w) : Task(0l, w, 0l) { broker = Copy(b); }
        virtual ~AwaitResponse() { Delete(broker); }
        virtual long GetCycle() { return 300l; }
        virtual bool HasNoGuard() { return false; }
        virtual bool EvalGuard() {
            if (broker->IsAlive() && hal::IsWlanConnected()) {
                auto socket = broker->GetSocket();
                auto fd = socket->GetFd();
                return hal::AvailableReadSocket(fd, 2);
            } else {
                return true;
            }
        }
        virtual void Execute() {
            if (broker->IsAlive() && hal::IsWlanConnected()) {
                auto socket = broker->GetSocket();
                auto fd = socket->GetFd();
                auto response = mqtt::BaseResponse::ReadFromSocket(fd);
                if (response) {
                    switch (response->GetPacketType()) {
                        case 2: {
                            auto acknowledgement = (mqtt::ConnectAcknowledgement*) response;
                            if (acknowledgement->IsConnectionAccepted()) {
                                broker->SetConnected();
                            }
                            break;
                        }
                        case 3: {
                            auto notification = (mqtt::PublishNotification*) response;
                            auto subscription = broker->FindSubscription(notification->GetTopicName());
                            if (subscription) {
                                std::string payload = notification->GetPayload();
                                if (IsEmpty(payload)) {
                                    subscription->PushTask(nullptr);
                                } else if (IsJson(payload)) {
                                    Expression* value = nullptr;
                                    if (json::FromJson(payload, value)) {
                                        subscription->PushTask(value);
                                    }
                                    Delete(value);
                                } else {
                                    Parser parser(payload);
                                    auto value = parser.ReadExpression();
                                    parser.DeleteStream();
                                    if (parser.GetState() != ParserState::ERROR) {
                                        subscription->PushTask(value);
                                    }
                                    Delete(value);
                                }
                                uint16_t packet_id = notification->GetPacketId();
                                if (packet_id > 0) {
                                    auto acknowledgement = new(std::nothrow) mqtt::PublishAcknowledgement(packet_id);
                                    if (acknowledgement) {
                                        acknowledgement->WriteToSocket(fd);
                                    }
                                }
                            }
                            break;
                        }
                        case 9: {
                            auto acknowledgement = (mqtt::SubscribeAcknowledgement*) response;
                            if (!acknowledgement->IsSubscriptionAccepted()) {
                                broker->RemoveSubscription(acknowledgement->GetPacketId());
                            }
                            break;
                        }
                        case 11: {
                            auto acknowledgement = (mqtt::UnsubscribeAcknowledgement*) response;
                            broker->RemoveSubscription(acknowledgement->GetPacketId());
                            break;
                        }
                    }
                    Delete(response);
                    if (broker->HasSubscriptions()) {
                        tasks.Push(this);
                    }
                } else {
                    broker->SetDead();
                }
            }
        }
    private:
        Broker* broker;
};

bool CreateBroker4::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSocketOrNil(expr1) && IsString(expr2) && IsString(expr3) && IsString(expr4)) {
        if (!expr1) {
            return PushFalse(sstack);
        }
        auto socket = (Socket*) expr1;
        auto clientid = (String*) expr2;
        auto username = (String*) expr3;
        auto password = (String*) expr4;
        auto broker = new(std::nothrow) Broker(socket);
        auto request = new(std::nothrow) mqtt::ConnectRequest(clientid->GetString(), username->GetString(), password->GetString());
        auto now = tasks.GetCurrentTime();
        auto task = new(std::nothrow) AwaitResponse(broker, now);
        auto success = false;
        if (broker && request && task) {
            if (request->WriteToSocket(socket->GetFd())) {
                tasks.Push(task);
                success = Push(broker, sstack);
            } else {
                broker->SetDead();
                success = PushFalse(sstack);
            }
        }
        Delete(task);
        Delete(request);
        Delete(broker);
        return success;
    }
    return false;
}

bool IsConnectedBroker1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsBroker(expr)) {
        auto broker = (Broker*) expr;
        if (broker->IsAliveAndConnected() && hal::IsWlanConnected()) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Publish4::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* retain, Action* &cstack, List* &sstack, Environment* env) {
    if (IsBroker(expr1) && IsString(expr2)) {
        auto broker = (Broker*) expr1;
        if (!broker->IsAliveAndConnected()) {
            return PushFalse(sstack);
        }
        auto socket = broker->GetSocket();
        auto topic_name = (String*) expr2;
        auto success = false;
        std::string payload;
        if (json::ToJson(expr3, payload)) {
            auto request = new(std::nothrow) mqtt::PublishRequest(topic_name->GetString(), payload, retain);
            if (request) {
                if (request->WriteToSocket(socket->GetFd())) {
                    success = Push(broker, sstack);
                } else {
                    broker->SetDead();
                    success = PushFalse(sstack);
                }
            }
            Delete(request);
        } else {
            success = PushFalse(sstack);
        }
        return success;
    }
    return false;
}

bool Subscribe4::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* continuation, Action* &cstack, List* &sstack, Environment* env) {
    if (IsBroker(expr1) && IsString(expr2) && IsSingletonSymbolList(expr3)) {
        auto broker = (Broker*) expr1;
        if (!broker->IsAliveAndConnected()) {
            return PushFalse(sstack);
        }
        auto socket = broker->GetSocket();
        auto packet_id = broker->NextPacketId();
        auto topic_filter = (String*) expr2;
        auto parameter_list = (List*) expr3;
        auto parameter = (Symbol*) parameter_list->GetFirst();
        auto request = new(std::nothrow) mqtt::SubscribeRequest(topic_filter->GetString(), packet_id, 1);
        auto now = tasks.GetCurrentTime();
        auto task = new(std::nothrow) AwaitResponse(broker, now);
        auto success = false;
        if (request && task) {
            if (request->WriteToSocket(socket->GetFd())) {
                if (!broker->HasSubscriptions()) {
                    tasks.Push(task);
                }
                success = broker->AddSubscription(topic_filter->GetString(), packet_id, 0l, parameter, continuation, env) && Push(broker, sstack);
            } else {
                broker->SetDead();
                success = PushFalse(sstack);
            }
        }
        Delete(task);
        Delete(request);
        return success;
    }
    return false;
}

bool Unsubscribe2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsBroker(expr1) && IsString(expr2)) {
        auto broker = (Broker*) expr1;
        if (!broker->IsAliveAndConnected()) {
            return PushFalse(sstack);
        }
        auto socket = broker->GetSocket();
        auto packet_id = broker->NextPacketId();
        auto topic_filter = (String*) expr2;
        auto subscription = broker->FindSubscription(topic_filter->GetString());
        auto request = new(std::nothrow) mqtt::UnsubscribeRequest(topic_filter->GetString(), packet_id);
        auto success = false;
        if (request) {
            if (subscription) {
                if (request->WriteToSocket(socket->GetFd())) {
                    subscription->SetPacketId(packet_id);
                    success = Push(broker, sstack);
                } else {
                    broker->SetDead();
                    success = PushFalse(sstack);
                }
            } else {
                success = PushFalse(sstack);
            }
        }
        Delete(request);
        return success;
    }
    return false;
}

bool Ping1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsBroker(expr)) {
        auto broker = (Broker*) expr;
        if (!broker->IsAliveAndConnected()) {
            return PushFalse(sstack);
        }
        auto socket = broker->GetSocket();
        auto request = new(std::nothrow) mqtt::PingRequest();
        auto now = tasks.GetCurrentTime();
        auto task = new(std::nothrow) AwaitResponse(broker, now);
        auto success = false;
        if (request && task) {
            if (request->WriteToSocket(socket->GetFd())) {
                if (!broker->HasSubscriptions()) {
                    tasks.Push(task);
                }
                success = Push(broker, sstack);
            } else {
                broker->SetDead();
                success = PushFalse(sstack);
            }
        }
        Delete(task);
        Delete(request);
        return success;
    }
    return false;
}

}
