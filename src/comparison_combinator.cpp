#include <cstdlib>
#include "comparison_combinator.h"
#include "hal.h"
#include "rational.h"
#include "string.h"
#include "boolean.h"

namespace lisp {

const double epsilon = 1e-15;

double Max(double d1, double d2) {
    if (d1 < d2) {
      return d2;
    }
    return d1;
}

bool IsEqual(Expression* e1, Expression* e2) {
    if (!e1) {
        return !e2;
    }
    if (!e2) {
        return false;
    }
    if (e1->GetType() != e2->GetType()) {
        if (IsRational(e1) && IsFloat(e2)) {
            auto e3 = e1;
            e1 = e2;
            e2 = e3;
        } else if (IsFloat(e1) && IsRational(e2)) {
            // pass
        } else {
            return false;
        }
    }
    switch (e1->GetType()) {
        case ExpressionType::LIST: {
            auto l1 = (List*) e1;
            auto l2 = (List*) e2;
            while (l1 && l2) {
                if (!IsEqual(l1->GetFirst(), l2->GetFirst())) {
                    return false;
                }
                l1 = l1->GetRest();
                l2 = l2->GetRest();
            }
            return !l1 && !l2;
        }
        case ExpressionType::SYMBOL: {
            return e1 == e2;
        }
        case ExpressionType::INTEGER:
        case ExpressionType::RATIO: {
            auto r1 = (Rational*) e1;
            auto r2 = (Rational*) e2;
            return r1->GetNumerator() == r2->GetNumerator() && r1->GetDenominator() == r2->GetDenominator();
        }
        case ExpressionType::FLOAT: {
            auto n1 = (Number*) e1;
            auto n2 = (Number*) e2;
            auto d1 = n1->GetDouble();
            auto d2 = n2->GetDouble();
            auto m = Max(abs(d1), abs(d2));
            if (m == 0.0d) {
                return true;
            }
            return abs(d1 - d2) / m < epsilon;
        }
        case ExpressionType::STRING: {
            auto s1 = (String*) e1;
            auto s2 = (String*) e2;
            return s1->GetString().compare(s2->GetString()) == 0;
        }
        default: {
            return e1 == e2;
        }
    }
}

bool IsEqual2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsEqual(expr1, expr2)) {
        return PushTrue(sstack);
    }
    return PushFalse(sstack);
}

bool IsLess2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (expr1 && expr2) {
        if (IsNumber(expr1) && IsNumber(expr2)) {
            auto n1 = (Number*) expr1;
            auto n2 = (Number*) expr2;
            auto d1 = n1->GetDouble();
            auto d2 = n2->GetDouble();
            if (d1 < d2) {
                return PushTrue(sstack);
            }
            return PushFalse(sstack);
        }
        if (IsString(expr1) && IsString(expr2)) {
            auto s1 = (String*) expr1;
            auto s2 = (String*) expr2;
            if (s1->GetString().compare(s2->GetString()) < 0) {
                return PushTrue(sstack);
            }
            return PushFalse(sstack);
        }
    }
    return false;
}

}
