#include <sstream>
#include "json.h"
#include "list.h"
#include "symbol.h"
#include "float.h"
#include "rational.h"
#include "string.h"
#include "hal.h"

namespace json {

bool IsPairList(lisp::List* list) {
    while (list) {
        auto first = list->GetFirst();
        if (!IsList(first)) {
            return false;
        }
        auto pair = (lisp::List*) first;
        if (!lisp::IsSymbol(pair->GetFirst())) {
            return false;
        }
        pair = pair->GetRest();
        if (!pair) {
            return false;
        }
        pair = pair->GetRest();
        if (pair) {
            return false;
        }
        list = list->GetRest();
    }
    return true;
}

bool ToJson(lisp::Expression* expr, std::string &str) {
    if (expr) {
        switch(expr->GetType()) {
            case lisp::ExpressionType::LIST: {
                auto list = (lisp::List*) expr;
                if (IsPairList(list)) {
                    str.push_back('{');
                    while (true) {
                        auto pair = (lisp::List*) list->GetFirst();
                        auto key = (lisp::Symbol*) pair->GetFirst();
                        pair = pair->GetRest();
                        auto value = pair->GetFirst();
                        str.push_back('"');
                        str.append(key->GetName());
                        str.append("\": ");
                        ToJson(value, str);
                        list = list->GetRest();
                        if (list) {
                            str.append(", ");
                        } else {
                            break;
                        }
                    }
                    str.push_back('}');
                } else {
                    str.push_back('[');
                    while (true) {
                        ToJson(list->GetFirst(), str);
                        list = list->GetRest();
                        if (list) {
                            str.append(", ");
                        } else {
                            break;
                        }
                    }
                    str.push_back(']');
                }
                break;
            }
            case lisp::ExpressionType::SYMBOL: {
                auto symbol = (lisp::Symbol*) expr;
                str.push_back('"');
                str.append(symbol->GetName());
                str.push_back('"');
                break;
            }
            case lisp::ExpressionType::INTEGER:
            case lisp::ExpressionType::RATIO:
            case lisp::ExpressionType::FLOAT: {
                // https://stackoverflow.com/questions/332111/how-do-i-convert-a-double-into-a-string-in-c
                auto number = (lisp::Number*) expr;
                std::ostringstream ostream;
                ostream << number->GetDouble();
                str.append(ostream.str());
                break;
            }
            case lisp::ExpressionType::STRING: {
                auto string = (lisp::String*) expr;
                str.push_back('"');
                for (char c : string->GetString()) {
                    if (c == '"') {
                        str.append("\\\"");
                    } else if (c == '\\') {
                        str.append("\\\\");
                    } else {
                        str.push_back(c);
                    }
                }
                str.push_back('"');
                break;
            }
            default: {
                return false;
            }
        }
    } else {
        str.append("nil");
    }
    return true;
}

bool Skip(const std::string &str, size_t &position) {
    while (position < str.length()) {
        if (str[position] > ' ') {
            break;
        }
        position++;
    }
    return position < str.length();
}

bool ParseNumber(const std::string &str, lisp::Expression* &expr, size_t &position) {
    size_t start = position;
    bool is_float = false;
    while (position < str.length()) {
        char current = str[position];
        if (current != '+' && current != '-' && current != '.' && current != 'e' && current != 'E' && (current < '0' || current > '9')) {
            break;
        }
        if (current == '.' || current == 'e' || current == 'E') {
            is_float = true;
        }
        position++;
    }
    std::string token = str.substr(start, position - start);
    if (is_float) {
        expr = new(std::nothrow) lisp::Float(stod(token));
    } else {
        expr = new(std::nothrow) lisp::Rational(stol(token), 1l);
    }
    return expr;
}

bool ParseString(const std::string &str, lisp::Expression* &expr, size_t &position) {
    std::string token;
    bool escape = false;
    while (position < str.length()) {
        char current = str[position];
        if (escape) {
            token.push_back(current);
            escape = false;
        } else {
            if (current == '"') {
                position++;
                break;
            }
            if (current == '\\') {
                escape = true;
            } else {
                token.push_back(current);
            }
        }
        position++;
    }
    expr = new(std::nothrow) lisp::String(token);
    return expr;
}

bool ParseAny(const std::string &str, lisp::Expression* &expr, size_t &position);

bool ParseList(const std::string &str, lisp::Expression* &expr, size_t &position) {
    if (!Skip(str, position)) {
        return false;
    }
    if (str[position] == ']') {
        position++;
        expr = nullptr;
        return true;
    }
    lisp::List* list = nullptr;
    while (true) {
        lisp::Expression* elem = nullptr;
        if (!ParseAny(str, elem, position)) {
            lisp::Delete(list);
            return false;
        }
        if (!lisp::Push(elem, list)) {
            lisp::Delete(elem);
            lisp::Delete(list);
            return false;
        }
        lisp::Delete(elem);
        if (!Skip(str, position)) {
            lisp::Delete(list);
            return false;
        }
        char current = str[position];
        if (current == ',') {
            position++;
            if (!Skip(str, position)) {
                lisp::Delete(list);
                return false;
            }
            continue;
        }
        if (current == ']') {
            position++;
            lisp::List* rev_list = nullptr;
            auto success = Reverse(list, rev_list);
            if (success) {
                expr = rev_list;
            }
            lisp::Delete(list);
            return success;
        }
        lisp::Delete(list);
        return false;
    }
}

bool ParseDictionary(const std::string &str, lisp::Expression* &expr, size_t &position) {
    if (!Skip(str, position)) {
        return false;
    }
    if (str[position] == '}') {
        position++;
        expr = nullptr;
        return true;
    }
    lisp::List* list = nullptr;
    while (true) {
        lisp::Expression* key = nullptr;
        lisp::Expression* value = nullptr;
        lisp::List* pair = nullptr;
        if (!ParseAny(str, key, position)) {
            lisp::Delete(list);
            return false;
        }
        if (!Skip(str, position) || str[position] != ':') {
            lisp::Delete(key);
            lisp::Delete(list);
            return false;
        }
        position++;
        if (!Skip(str, position) || !ParseAny(str, value, position)) {
            lisp::Delete(key);
            lisp::Delete(list);
            return false;
        }
        if (!lisp::Pair(key, value, pair)) {
            lisp::Delete(key);
            lisp::Delete(value);
            lisp::Delete(list);
            return false;
        }
        lisp::Delete(key);
        lisp::Delete(value);
        if (!lisp::Push(pair, list)) {
            lisp::Delete(pair);
            lisp::Delete(list);
            return false;
        }
        Delete(pair);
        if (!Skip(str, position)) {
            lisp::Delete(list);
            return false;
        }
        char current = str[position];
        if (current == ',') {
            position++;
            if (!Skip(str, position)) {
                lisp::Delete(list);
                return false;
            }
            continue;
        }
        if (current == '}') {
            position++;
            expr = list;
            return true;
        }
        lisp::Delete(list);
        return false;
    }
}

bool ParseSymbol(const std::string &str, lisp::Expression* &expr, size_t &position) {
    if (str.compare(position, 4, "null") == 0) {
        expr = nullptr;
        position += 4;
        return true;
    }
    if (str.compare(position, 5, "false") == 0) {
        expr = nullptr;
        position += 5;
        return true;
    }
    if (str.compare(position, 4, "true") == 0) {
        expr = lisp::Intern("t");
        position += 4;
        return true;
    }
    return false;
}

bool ParseAny(const std::string &str, lisp::Expression* &expr, size_t &position) {
    if (!Skip(str, position)) {
        return false;
    }
    char current = str[position];
    if (current == '-' || (current >= '0' && current <= '9')) {
        return ParseNumber(str, expr, position);
    } else if (current == '"') {
        position++;
        return ParseString(str, expr, position);
    } else if (current == '[') {
        position++;
        return ParseList(str, expr, position);
    } else if (current =='{') {
        position++;
        return ParseDictionary(str, expr, position);
    } else {
        return ParseSymbol(str, expr, position);
    }
}

bool FromJson(const std::string &str, lisp::Expression* &expr) {
    size_t position = 0;
    return ParseAny(str, expr, position);
}

}
