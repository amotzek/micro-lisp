#include <cstdlib>
#include <cmath>
#include "display.h"
#include "font.h"

namespace lisp {

const double pi = 4.0d * atan(1.0);

bool Display::Towards(int x_dest, int y_dest) {
    int dx = x_dest - x_src;
    int dy = y_dest - y_src;
    if (dx != 0 || dy != 0) {
        heading = 180.0d * atan2(dy, dx) / pi;
        return true;
    }
    return false;
}

bool Display::SetPosition(int x_dest, int y_dest) {
    auto success = true;
    if (IsPenDown()) {
        int dx = abs(x_dest - x_src);
        int dy = -abs(y_dest - y_src);
        int sx = (x_src < x_dest) ? 1 : -1;
        int sy = (y_src < y_dest) ? 1 : -1;
        int e = dx + dy;
        while (x_src != x_dest || y_src != y_dest) {
            if (!SetPixel(x_src, y_src, red, green, blue)) {
                success = false;
                break;
            }
            int e2 = e << 1;
            if (e2 > dy) {
                e += dy;
                x_src += sx;
            }
            if (e2 < dx) {
                e += dx;
                y_src += sy;
            }
        }
        if (success && !SetPixel(x_dest, y_dest, red, green, blue)) {
            success = false;
        }
    }
    x_src = x_dest;
    y_src = y_dest;
    return success;
}

bool Display::Forward(double dist) {
    while (heading < 0.0d) {
        heading += 360.0d;
    }
    while (heading > 360.0d) {
        heading -= 360.0d;
    }
    double heading_rad = pi * heading / 180.0d;
    int x_dest = round(x_src + dist * cos(heading_rad));
    int y_dest = round(y_src + dist * sin(heading_rad));
    return SetPosition(x_dest, y_dest);
}

bool Display::Label(std::string label, int fontsize) {
    if (IsPenDown()) {
        auto font = CreateFont(fontsize);
        if (font) {
            for (char c : label) {
                int width = font->BitBlit(c, this, x_src, y_src, red, green, blue); // Todo Heading berücksichtigen
                x_src += width;
                if (width > 0) {
                    x_src++;
                }
            }
            Delete(font);
            return true;
        }
        return false;
    }
    return true;
}

}
