#ifndef ACTION_H
#define ACTION_H
#include "environment.h"

namespace lisp {

class Action {
    public:
        Action() { next = nullptr; }
        virtual ~Action() {
            auto n = next;
            while (n) {
                auto nn = n->next;
                n->next = nullptr;
                delete n;
                n = nn;
            }
        } // https://stackoverflow.com/questions/294927/does-delete-work-with-pointers-to-base-class
        void SetNext(Action* n) { next = n; }
        Action* GetNext() { return next; }
        virtual bool Perform(Action* &cstack, List* &sstack) = 0;
        virtual bool Catches(Symbol* symb) { return false; }
        virtual bool Throws() { return false; }
    private:
        Action* next;
};

bool Push(Action* action, Action* &cstack);

Action* Pop(Action* &cstack);

void Delete(Action* action);

class QuoteAction : public Action {
    public:
        QuoteAction(Expression* v) { value = Copy(v); }
        virtual ~QuoteAction() { Delete(value); }
        virtual bool Perform(Action* &cstack, List* &sstack) { return Push(value, sstack); }
    private:
        Expression* value;
};

class EvalAction : public Action {
    public:
        EvalAction(Expression* expr, Environment* env) { expression = Copy(expr); environment = Copy(env); }
        virtual ~EvalAction() { Delete(expression); Delete(environment); }
        virtual bool Perform(Action* &cstack, List* &sstack);
    private:
        Expression* expression;
        Environment* environment;
};

class CatchAction : public Action {
    public:
        CatchAction(Symbol* symb, List* sstack) { symbol = Copy(symb); next_sstack = Copy(sstack); }
        virtual ~CatchAction() { Delete(symbol); Delete(next_sstack); }
        virtual bool Perform(Action* &cstack, List* &sstack) { return true; }
        virtual bool Catches(Symbol* symb) { return !symbol || symbol == symb; }
        List* GetNextSStack() { return next_sstack; }
    private:
        Symbol* symbol;
        List* next_sstack;
};

class ThrowAction : public Action {
    public:
        ThrowAction(Symbol* symb, Expression* val) { symbol = Copy(symb); value = Copy(val); }
        virtual ~ThrowAction() { Delete(symbol); Delete(value); }
        virtual bool Perform(Action* &cstack, List* &sstack);
        virtual bool Throws() { return true; }
    private:
        Symbol* symbol;
        Expression* value;
};

}

#endif
