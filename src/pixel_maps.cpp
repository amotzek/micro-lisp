#include "pixel_maps.h"
#include "ili9341.h"
#include "jd79653.h"
#include "list.h"
#include "symbol.h"

namespace ili9341 {

lisp::PixelMap* CreatePixelMap(lisp::Expression* expr) {
    if (lisp::IsList(expr)) {
        auto list = (lisp::List*) expr;
        auto first = list->GetFirst();
        if (lisp::IsSymbol(first)) {
            auto symbol = (lisp::Symbol*) first;
            if (symbol->HasName("m5stack-esp32-basic-core")) {
                return new(std::nothrow) ili9341::ILI9341(320, 240, true, false, 18, 19, 23, 14, 27, 33, 32, true); // width, height, mirror_axis, mirror_diagonal, sck, miso, mosi, cs, dc, rst, bl, invert
            } else if (symbol->HasName("odroid-go")) {
                return new(std::nothrow) ili9341::ILI9341(320, 240, true, true, 18, 19, 23, 5, 21, -1, 14, false);
            }
        }
    }
    return nullptr;
}

}

namespace jd79653 {

lisp::PixelMap* CreatePixelMap(lisp::Expression* expr) {
    if (lisp::IsList(expr)) {
        auto list = (lisp::List*) expr;
        auto first = list->GetFirst();
        if (lisp::IsSymbol(first)) {
            auto symbol = (lisp::Symbol*) first;
            if (symbol->HasName("m5stack-core-ink")) {
                return new(std::nothrow) jd79653::JD79653(18, 23, 9, 15, 0, 4); // sck, mosi, cs, dc, rst, busy
            }
        }
    }
    return nullptr;
}

}
