#ifdef ARDUINO
#include <new>
#include <string>
#include <sstream>
#include "arduino_sal.h"
#include "combinator.h"
#include "loader.h"
#include "cooperative_multitasking.h"
#include "hal.h"

void HardwareSetup() {
    hal::SetupWlan();
}

void LoadSetup() {
    auto istream = hal::CreateFileStream("setup.lisp");
    if (istream) {
        auto env = lisp::GetRootEnvironment();
        lisp::Load(istream, env, true);
        delete istream;
    }
}

std::string read_line = "";

void ReadEvalPrintConsumeChar(char c) {
    if (c == '\r' || c == '\r') {
        if (read_line.length() > 0) {
            auto istream = new(std::nothrow) std::istringstream(read_line);
            if (istream) {
                auto env = lisp::GetRootEnvironment();
                lisp::Load(istream, env, true);
                delete istream;
            }
            read_line.clear();
        }
    } else {
        read_line.push_back(c);
    }
}

bool HasTask(long d) {
    return lisp::tasks.HasTask(d);
}

void RunTask() {
    lisp::tasks.Run();
}

#endif