#include <new>
#include "symbol.h"
#include "list.h"

namespace lisp {

static const int bucket_count = 256; // muss eine Potenz von 2 sein

static List* buckets[bucket_count];

Symbol* Intern(const std::string &name) {
    int x[4] = { 0, 0, 0, 0 };
    for (auto c : name) {
        for (int i = 1; i <= 3; x[i-1] = x[i], i++) ;
        x[3] = (31 * (int) c + 29 * x[2] + 27 * x[1] + 25 * x[0]) & (bucket_count - 1);
    }
    auto bucket_index = x[3];
    auto bucket = buckets[bucket_index];
    Symbol* symbol;
    while (bucket) {
        symbol = (Symbol*) bucket->GetFirst();
        bucket = bucket->GetRest();
        if (symbol->HasName(name)) {
            return Copy(symbol);
        }
    }
    symbol = new(std::nothrow) Symbol(name, bucket_index);
    if (symbol) {
        if (Push(symbol, buckets[bucket_index])) {
            return symbol;
        }
        Delete(symbol);
    }
    return nullptr;
}

void GarbageCollectSymbols() {
    for (int bucket_index = 0; bucket_index < bucket_count; bucket_index++) {
        auto bucket = buckets[bucket_index];
        List* last_bucket = nullptr;
        while (bucket) {
            auto symbol = bucket->GetFirst();
            auto next_bucket = bucket->GetRest();
            if (last_bucket && symbol->GetReferenceCount() < 2) {
                last_bucket->SetRest(next_bucket);
            }
            last_bucket = bucket;
            bucket = next_bucket;
        }
        bucket = buckets[bucket_index];
        if (bucket) {
            auto symbol = bucket->GetFirst();
            if (symbol->GetReferenceCount() < 2) {
                buckets[bucket_index] = Copy(bucket->GetRest());
                Delete(bucket);
            }
        }
    }
}

}
