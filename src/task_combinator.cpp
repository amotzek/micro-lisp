#include <new>
#include "task_combinator.h"
#include "rational.h"
#include "cooperative_multitasking.h"
#include "boolean.h"

namespace lisp {

bool GetMonotonicTime0::Perform(Action* &cstack, List* &sstack, Environment* env) {
    auto value = new(std::nothrow) Rational(tasks.GetCurrentTime(), 1l);
    auto success = value && Push(value, sstack);
    Delete(value);
    return success;
}

bool Now1::ProcessValue(Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    auto value = tasks.Now(0l, cont, env);
    auto success = value && Push(value, sstack);
    Delete(value);
    return success;
}

bool After2::ProcessValues(Expression* dur, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(dur)) {
        auto value = tasks.After(0l, ((Rational*) dur)->GetNumerator(), cont, env);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool WhenThen2::ProcessValues(Expression* guard, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    auto value = tasks.WhenThen(0l, guard, cont, env);
    auto success = value && Push(value, sstack);
    Delete(value);
    return success;
}

bool WhenForThen3::ProcessValues(Expression* guard, Expression* dur, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(dur)) {
        auto value = tasks.WhenForThen(0l, guard, ((Rational*) dur)->GetNumerator(), cont, env);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool PriorityNow2::ProcessValues(Expression* prio, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(prio)) {
        auto value = tasks.Now(((Rational*) prio)->GetNumerator(), cont, env);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool PriorityAfter3::ProcessValues(Expression* prio, Expression* dur, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(prio) && IsInteger(dur)) {
        auto value = tasks.After(((Rational*) prio)->GetNumerator(), ((Rational*) dur)->GetNumerator(), cont, env);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool PriorityWhenThen3::ProcessValues(Expression* prio, Expression* guard, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(prio)) {
        auto value = tasks.WhenThen(((Rational*) prio)->GetNumerator(), guard, cont, env);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool PriorityWhenForThen4::ProcessValues(Expression* prio, Expression* guard, Expression* dur, Expression* cont, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(prio) && IsInteger(dur)) {
        auto value = tasks.WhenForThen(((Rational*) prio)->GetNumerator(), guard, ((Rational*) dur)->GetNumerator(), cont, env);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool IsTaskList(Expression* expr) {
    if (IsList(expr)) {
        auto list = (List*) expr;
        int len = 0;
        while (list) {
            auto first = list->GetFirst();
            if (++len > 4 || !IsTask(first) || !tasks.Contains((Task*) first)) {
                return false;
            }
            list = list->GetRest();
        }
        return len >= 2;
    }
    return false;
}

bool OnlyOneOfList1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsTaskList(expr)) {
        auto list = (List*) expr;
        auto task1 = (Task*) list->GetFirst();
        list = list->GetRest();
        auto task2 = (Task*) list->GetFirst();
        list = list->GetRest();
        Task* task3 = nullptr;
        Task* task4 = nullptr;
        if (list) {
            task3 = (Task*) list->GetFirst();
            list = list->GetRest();
        }
        if (list) {
            task4 = (Task*) list->GetFirst();
        }
        if (tasks.OnlyOneOf(task1, task2, task3, task4)) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool RunTasks1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsInteger(expr)) {
        auto timeout = (Rational*) expr;
        long remaining = timeout->GetNumerator();
        long count = 0l;
        while (tasks.HasTask(remaining)) {
            long start = tasks.GetCurrentTime();
            tasks.Run();
            long end = tasks.GetCurrentTime();
            remaining -= (end - start);
            count++;
        }
        auto value = new(std::nothrow) Rational(count, 1l);
        auto success = value && Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool DrainTasks0::Perform(Action* &cstack, List* &sstack, Environment* env) {
    long count = tasks.Drain();
    auto value = new(std::nothrow) Rational(count, 1l);
    auto success = value && Push(value, sstack);
    Delete(value);
    return success;
}

}
