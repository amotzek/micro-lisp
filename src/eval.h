#ifndef EVAL_H
#define EVAL_H
#include "expression.h"
#include "environment.h"
#include "action.h"

namespace lisp {

bool InnerEval(Expression* expression, Environment* environment, Expression* &value, ThrowAction* &throw_action);

bool Eval(Expression* expression, Environment* environment, Expression* &value);

}

#endif
