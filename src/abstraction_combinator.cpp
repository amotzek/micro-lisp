#include <new>
#include "abstraction_combinator.h"
#include "symbol.h"
#include "list.h"
#include "lambda.h"
#include "macro.h"
#include "boolean.h"

namespace lisp {

bool IsSymbolList(Expression* expr) {
    if (!expr) {
        return true;
    }
    if (expr->HasType(ExpressionType::LIST)) {
        auto list = (List*) expr;
        while (list) {
            if (!IsSymbol(list->GetFirst())) {
                return false;
            }
            list = list->GetRest();
        }
        return true;
    }
    return false;
}

bool Setq2::ProcessValues(Expression* key, Expression* value, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbol(key)) {
        return env->ModifyOrAddPair((Symbol*) key, value) && Push(value, sstack);
    }
    return false;
}

bool Unbind1::ProcessValue(Expression* key, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbol(key)) {
        if (env->RemovePair((Symbol*) key)) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool IsBound1::ProcessValue(Expression* key, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbol(key)) {
        Expression* value;
        if (env->Lookup((Symbol*) key, value)) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Lambda2::ProcessValues(Expression* parameters, Expression* body, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbolList(parameters)) {
        auto lambda = new(std::nothrow) Lambda((List*) parameters, body, env);
        auto success = lambda && Push(lambda, sstack);
        Delete(lambda);
        return success;
    }
    return false;
}

bool Macro2::ProcessValues(Expression* parameter, Expression* body, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbol(parameter)) {
        auto macro = new(std::nothrow) Macro((Symbol*) parameter, body, env);
        auto success = macro && Push(macro, sstack);
        Delete(macro);
        return success;
    }
    return false;
}

}
