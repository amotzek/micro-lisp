#include "boolean.h"
#include "symbol.h"

namespace lisp {

bool PushTrue(List* &sstack) {
    auto value = Intern("t");
    if (value) {
        auto success = Push(value, sstack);
        Delete(value);
        return success;
    }
    return false;
}

bool PushFalse(List* &sstack) {
    return Push(nullptr, sstack);
}

}
