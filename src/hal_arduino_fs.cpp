#ifdef ARDUINO
#include <new>
#include <streambuf>
#include <string>
#include <istream>
#include "SPIFFS.h"

namespace hal {

static bool spiffs_begun = false;

bool BeginSPIFFS() {
    if (spiffs_begun) {
        return true;
    }
    spiffs_begun = SPIFFS.begin(true);
    return spiffs_begun;
}

class FileBuf : public std::streambuf {
    public:
        FileBuf(const File &f) { file = f; buffer = new char[64]; }
        virtual ~FileBuf() { file.close(); delete[] buffer; }
        virtual int underflow() {
            if (gptr() == egptr()) {
                if (file.available()) {
                    auto size = file.read((byte*) buffer, 64);
                    setg(buffer, buffer, buffer + size);
                } else {
                    setg(buffer, buffer, buffer);
                }
            }
            if (gptr() == egptr()) {
                return std::char_traits<char>::eof();
            }
            return std::char_traits<char>::to_int_type(*gptr());
        }
    private:
        File file;
        char* buffer;
};

class IFileStream : virtual FileBuf, public std::istream {
    public:
        IFileStream(const File &f) : FileBuf(f), std::istream(static_cast<std::streambuf*>(this)) { }
};

std::istream* CreateFileStream(std::string filename) {
    if (BeginSPIFFS()) {
        auto file = SPIFFS.open(("/" + filename).c_str());
        if (file) {
            return new(std::nothrow) IFileStream(file);
        }
    }
    return nullptr;
}

bool AppendTextFile(std::string filename, std::string text) {
    if (BeginSPIFFS()) {
        auto success = false;
        auto file = SPIFFS.open(("/" + filename).c_str(), FILE_APPEND);
        if (file) {
            success = file.print(text.c_str());
            file.close();
        }
        return success;
    }
    return false;
}

bool DeleteFile(std::string filename) {
    return BeginSPIFFS() && SPIFFS.remove(("/" + filename).c_str());
}

}

#endif
