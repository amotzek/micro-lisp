#ifndef FORMATTER_H
#define FORMATTER_H
#include <ostream>
#include "expression.h"

namespace lisp {

std::ostream& operator<<(std::ostream &ostream, Expression* expr);

}

#endif
