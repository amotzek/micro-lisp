#include <new>
#include "file_combinator.h"
#include "string.h"
#include "text_file_stream.h"
#include "boolean.h"
#include "hal.h"
#include "loader.h"

namespace lisp {

bool Open1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto name = (String*) expr;
        auto stream = new(std::nothrow) TextFileStream(name->GetString());
        auto success = stream && Push(stream, sstack);
        Delete(stream);
        return success;
    }
    return false;
}

bool WriteChar2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsStream(expr1) && IsString(expr2)) {
        auto stream = (TextFileStream*) expr1;
        auto string = (String*) expr2;
        if (stream->WriteString(string->GetString())) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Close1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsStream(expr)) {
        auto stream = (TextFileStream*) expr;
        if (stream->Close()) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Load1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto name = (String*) expr;
        auto stream = hal::CreateFileStream(name->GetString());
        if (stream) {
            auto success = Load(stream, env);
            delete stream;
            if (success) {
                return PushTrue(sstack);
            }
            return PushFalse(sstack);
        }
    }
    return false;
}

bool DeleteFile1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr)) {
        auto name = (String*) expr;
        if (hal::DeleteFile(name->GetString())) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

}
