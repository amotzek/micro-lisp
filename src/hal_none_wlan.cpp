#ifndef ARDUINO
#include <string>
#include <list>
#include <tuple>

namespace hal {

void SetupWlan() {
}

bool SetHostname(std::string hostname) {
    return false;
}

bool ConnectWlan(std::list<std::tuple<std::string, std::string>>* credentials) {
    return false;
}

bool IsWlanConnected() {
    return true;
}

}

#endif
