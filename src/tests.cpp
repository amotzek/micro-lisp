#ifndef ARDUINO
#include <climits>
#include "tests.h"
#include "assert.h"
#include "cooperative_multitasking.h"

namespace lisp {

bool RunAbstractionTests() {
    auto pairs = {
        MakePair("(setq t (quote t))", "t"),
        MakePair("t", "t"),
        MakePair("(bound? (quote t))", "t"),
        MakePair("(bound? (quote no-no-no))", "nil"),
        MakePair("((lambda () 123))", "123"),
        MakePair("((lambda (x) (times x x)) 3)", "9"),
        MakePair("((lambda (x y) (times 2 (times x y))) 5 6)", "60"),
        MakePair("((lambda (x y z) (times 3 (times x (times y z)))) 2 4 5)", "120"),
        MakePair("((lambda (x y) (add (times x 2) (times 3 y))) 9 1)", "21"),
        MakePair("((mlambda x 2) a b c)", "2"),
        MakePair("(let ((x 2) (y 3)) (+ x y))", "5")
    };
    return AssertEval(pairs);
}

bool RunControlTests() {
    auto pairs = {
        MakePair("(if nil 1 2)", "2"),
        MakePair("(if t 1 2)", "1"),
        MakePair("(if (equal? 0 0) 1 (quotient 2 0))", "1"),
        MakePair("(if (not (equal? 0 0)) (quotient 2 0) 2)", "2"),
        MakePair("(or2 (add 3 5) nil)", "8"),
        MakePair("(or2 t (quotient 3 0))", "t"),
        MakePair("(or2 nil (times 1 1))", "1"),
        MakePair("(or2 nil nil)", "nil"),
        MakePair("(or nil nil (/ 9 3) nil)", "3"),
        MakePair("(and t (+ 2 3) (+ 3 4))", "7"),
        MakePair("(and t (+ 2 3) nil)", "nil"),
        MakePair("(aif (+ 5 7) it bloop)", "12"),
        MakePair("(aif nil bloop (quote ok))", "ok"),
        MakePair("(aand (+ 1 2) (+ 3 4) (* 2 it))", "14")
    };
    return AssertEval(pairs);
}

bool RunImperativeTests() {
    auto pairs = {
        MakePair("(progn (defproc testproc (x) (setq x 1) (setq x (add x 1))) (testproc nil))", "2"),
        MakePair("(unbind (quote testproc))", "t"),
        MakePair("(unless t 1 2)", "nil"),
        MakePair("(unless t)", "nil"),
        MakePair("(unless nil)", "nil"),
        MakePair("(unless nil 1 2 3)", "3"),
        MakePair("(when nil 1 2)", "nil"),
        MakePair("(when t)", "nil"),
        MakePair("(when nil)", "nil"),
        MakePair("(when t 1 2 3)", "3"),
        MakePair("(awhen nil 1 2)", "nil"),
        MakePair("(awhen t)", "nil"),
        MakePair("(awhen nil)", "nil"),
        MakePair("(awhen t 1 2 3)", "3"),
        MakePair("(awhen 2 (+ it 3))", "5"),
        MakePair("(awhen 2 3 (+ it 4))", "6")
    };
    return AssertEval(pairs);
}

bool RunCatchThrowTests() {
    auto pairs = {
        MakePair("(catch nil (add 8 13))", "21"),
        MakePair("(add 8 (catch nil 13))", "21"),
        MakePair("(add 8 (catch nil (throw (quote bam) 13)))", "21"),
        MakePair("(add (catch nil (throw (quote bam) 13)) 8)", "21"),
        MakePair("(add 3 (add (catch nil (throw (quote bam) 13)) 5))", "21"),
        MakePair("(add 8 (catch nil (add 50 (throw (quote bam) 13))))", "21"),
        MakePair("(add 8 (catch nil (add (throw (quote bam) 13) 50)))", "21"),
        MakePair("(catch nil (quotient 1.0 0.0))", "\"arithmetic error\""),
        MakePair("(catch (quote error) (quotient 1 0))", "\"arithmetic error\""),
        MakePair("(catch (quote crash) (times 4 (throw (quote crash) \"crash\")))", "\"crash\""),
        MakePair("(type-of (catch (quote error) (quotient 2 0)))", "string"),
        MakePair("(equal? (type-of (catch (quote error) (quotient 2 0))) (quote ratio))", "nil")
    };
    return AssertEval(pairs);
}

bool RunArithmeticTests1() {
    auto pairs = {
        MakePair("(add 3.2 3.9)", "7.1"),
        MakePair("(add 4 -3)", "1"),
        MakePair("(add 3.9 -2.9)", "1.0"),
        MakePair("(add 3 -4)", "-1"),
        MakePair("(add 2.9 -3.1)", "-0.2"),
        MakePair("(sub 3 4)", "-1"),
        MakePair("(sub 3.2 3.9)", "-0.7"),
        MakePair("(sub 4 -3)", "7"),
        MakePair("(sub 3.9 -2.9)", "6.8"),
        MakePair("(sub 3 -4)", "7"),
        MakePair("(sub 2.9 -3.1)", "6.0"),
        MakePair("(times 1.1 2.2)", "2.42"),
        MakePair("(times 1.23 -2.34)", "-2.8782"),
        MakePair("(times -3.21 -4.32)", "13.8672"),
        MakePair("(quotient 9 3)", "3"),
        MakePair("(quotient 1.21 1.1)", "1.1"),
        MakePair("(quotient 13 -2.0)", "-6.5"),
        MakePair("(quotient -8 4)", "-2"),
        MakePair("(quotient -81 -9)", "9"),
        MakePair("(quotient 4 -2)", "-2")

    };
    return AssertEval(pairs);
}

bool RunArithmeticTests2() {
    auto pairs = {
        MakePair("(+)", "0"),
        MakePair("(+ 2)", "2"),
        MakePair("(+ 8 1)", "9"),
        MakePair("(+ 4 1 4)", "9"),
        MakePair("(- 1)", "-1"),
        MakePair("(- 11 1 1 1)", "8"),
        MakePair("(*)", "1"),
        MakePair("(* 2)", "2"),
        MakePair("(* 2 3)", "6"),
        MakePair("(* 2 3 4)", "24"),
        MakePair("(/ 2)", "0.5"),
        MakePair("(/ 2 2)", "1"),
        MakePair("(/ 2 2 2)", "0.5"),
        MakePair("(abs -2)", "2"),
        MakePair("(abs 4)", "4"),
        MakePair("(min 2 4 -1 3)", "-1"),
        MakePair("(max 2 4 -1 3)", "4"),
        MakePair("(floor 3)", "3"),
        MakePair("(floor (quotient 9 4))", "2"),
        MakePair("(floor 1.57)", "1"),
        MakePair("(less? (random 10) 10)", "t")
    };
    return AssertEval(pairs);
}

bool RunStringTests() {
    auto pairs = {
        MakePair("(intern \"a\")", "a"),
        MakePair("(char-code \"ABC\")", "65"),
        MakePair("(code-char 66)", "\"B\""),
        MakePair("(concatenate2 \"ABC\" \"DEF\")", "\"ABCDEF\""),
        MakePair("(concatenate)", "\"\""),
        MakePair("(concatenate \"Ah\" \"Be\" \"Ce\" \"De\")", "\"AhBeCeDe\""),
        MakePair("(substring \"abcdef\" 3 2)", "\"cd\""),
        MakePair("(string-length \"abcd\")", "4"),
        MakePair("(read-from-string \"(a b c)\")", "(a b c)"),
        MakePair("(list-length (read-from-string \"(deffn get-dragon-bounds (n size) (get-bounds (dragon-curve (create-display (quote bounds) nil) n size)))\"))", "4"),
        MakePair("(type-of (first (read-from-string \"(a b c)\")))", "atom"),
        MakePair("(read-from-string \"3.1415\")", "3.1415"),
        MakePair("(type-of (read-from-string \"3.1415\"))", "float"),
        MakePair("(write-to-string (quote (a b nil)))", "\"(a b nil)\""),
        MakePair("(write-to-string 1)", "\"1\""),
        MakePair("(write-to-string (/ 2))", "\"0.5\""),
        MakePair("(write-to-string 3.1415)", "\"3.1415\""),
        MakePair("(write-to-string \"juhuu\")", "\"\\\"juhuu\\\"\""),
        MakePair("(min \"b\" \"a\")", "\"a\""),
        MakePair("(max \"b\" \"a\")", "\"b\"")
    };
    return AssertEval(pairs);
}

bool RunListTests1() {
    auto pairs = {
        MakePair("(cons (quote a) nil)", "(a)"),
        MakePair("(cons (quote a) (quote (b)))", "(a b)"),
        MakePair("(cons (quote a) (cons (quote b) nil))", "(a b)"),
        MakePair("(list 1)", "(1)"),
        MakePair("(list 1 2)", "(1 2)"),
        MakePair("(list 1 2 3)", "(1 2 3)"),
        MakePair("(list 1 2 3 4)", "(1 2 3 4)"),
        MakePair("(append (list 1 2) (list 2 3))", "(1 2 2 3)"),
        MakePair("(append (list 1 2) (list 2 3) (list 3 4))", "(1 2 2 3 3 4)"),
        MakePair("(first (quote (a b c)))", "a"),
        MakePair("(rest (quote (a b c)))", "(b c)"),
        MakePair("(list-length nil)", "0"),
        MakePair("(list-length (quote (a b c d)))", "4"),
        MakePair("(null? (quote (a)))", "nil"),
        MakePair("(null? nil)", "t"),
        MakePair("(single? nil)", "nil"),
        MakePair("(single? (quote (a)))", "t"),
        MakePair("(single? (quote (a b)))", "nil"),
        MakePair("(tail? (list 3 4) (list 1 2 3 4))", "t"),
        MakePair("(tail? (list 3 4) (list 1 2 3 5))", "nil")
    };
    return AssertEval(pairs);
}

bool RunListTests2() {
    auto pairs = {
        MakePair("(first (quote (1 2 3 4 5 6 7 8 9 10)))", "1"),
        MakePair("(second (quote (1 2 3 4 5 6 7 8 9 10)))", "2"),
        MakePair("(third (quote (1 2 3 4 5 6 7 8 9 10)))", "3"),
        MakePair("(fourth (quote (1 2 3 4 5 6 7 8 9 10)))", "4"),
        MakePair("(fifth (quote (1 2 3 4 5 6 7 8 9 10)))", "5"),
        MakePair("(sixth (quote (1 2 3 4 5 6 7 8 9 10)))", "6"),
        MakePair("(seventh (quote (1 2 3 4 5 6 7 8 9 10)))", "7"),
        MakePair("(eighth (quote (1 2 3 4 5 6 7 8 9 10)))", "8"),
        MakePair("(ninth (quote (1 2 3 4 5 6 7 8 9 10)))", "9"),
        MakePair("(tenth (quote (1 2 3 4 5 6 7 8 9 10)))", "10"),
        MakePair("(nth (range 1 5) 0)", "1"),
        MakePair("(nth (range 1 5) 1)", "2"),
        MakePair("(nth (range 1 5) 2)", "3")
    };
    return AssertEval(pairs);
}

bool RunListTests3() {
    auto pairs = {
        MakePair("(nthrest (list 1 2 3 4) 0)", "(1 2 3 4)"),
        MakePair("(nthrest (list 1 2 3 4) 1)", "(2 3 4)"),
        MakePair("(nthrest (list 1 2 3 4) 2)", "(3 4)"),
        MakePair("(remove 3 (range 1 5))", "(1 2 4)"),
        MakePair("(last (list 3 4))", "(4)"),
        MakePair("(last (list 1))", "(1)"),
        MakePair("(butlast (quote (a b c d)))", "(a b c)"),
        MakePair("(butlast (quote (a)))", "nil"),
        MakePair("(range 1 5)", "(1 2 3 4)"),
        MakePair("(reverse nil)", "nil"),
        MakePair("(reverse (list 1))", "(1)"),
        MakePair("(reverse (range 1 5))", "(4 3 2 1)"),
        MakePair("(sort nil less?)", "nil"),
        MakePair("(sort (list 1) (lambda (x y) (throw (quote bom) (quote bom)))))", "(1)"),
        MakePair("(catch nil (sort (list 4 2 1 3) (lambda (x y) (throw (quote bom) (quote bom)))))", "bom"),
        MakePair("(sort (list 7 4 5 2 1 9 8 6 3) less?)", "(1 2 3 4 5 6 7 8 9)")
    };
    return AssertEval(pairs);
}

bool RunListTests4() {
    auto pairs = {
        MakePair("(map-with (lambda (x) (* 2 x)) (quote (1 2 3)))", "(2 4 6)"),
        MakePair("(zip-with (lambda (x y) (+ x y)) (list 2 3) (list 1 7))", "(3 10)"),
        MakePair("(find-if odd? (list 2 4 5 7))", "5"),
        MakePair("(find-if odd? (list 2 4 8 16))", "nil"),
        MakePair("(select-if odd? (range 1 10))", "(1 3 5 7 9)"),
        MakePair("(remove-if odd? (range 1 10))", "(2 4 6 8)"),
        MakePair("(some? even? nil)", "nil"),
        MakePair("(some? even? (range 1 10))", "2"),
        MakePair("(some? even? (list 1 3 5))", "nil"),
        MakePair("(every? even? nil)", "t"),
        MakePair("(every? even? (range 1 10))", "nil"),
        MakePair("(every? odd? (list 1 3 5))", "t")
    };
    return AssertEval(pairs);
}

bool RunSetTests() {
    auto pairs = {
        MakePair("(member? 4 (list 2 4 5 6))", "(4 5 6)"),
        MakePair("(member? 77 (list 2 4 5 6))", "nil"),
        MakePair("(member-if? odd? (list 2 4 5 6))", "(5 6)"),
        MakePair("(member-if? odd? (list 2 4 6 8))", "nil"),
        MakePair("(adjoin 2 (list 3 4 5))", "(2 3 4 5)"),
        MakePair("(adjoin 4 (list 3 4 5))", "(3 4 5)"),
        MakePair("(union (list 2 3 4) (list 3 4 5))", "(2 3 4 5)"),
        MakePair("(intersection (list 2 3 4) (list 3 4 5))", "(3 4)"),
        MakePair("(set-difference (list 2 3 4) (list 3 4 5))", "(2)"),
        MakePair("(set-exclusive-or (list 2 3 4) (list 3 4 5))", "(2 5)"),
        MakePair("(subset? (list 2 3 4) (list 3 4 5))", "nil"),
        MakePair("(subset? (list 3 4) (list 3 4 5))", "t"),
        MakePair("(disjoint? (list 2 3 4) (list 3 4 5))", "nil"),
        MakePair("(disjoint? (list 1 2) (list 5 6))", "t")
    };
    return AssertEval(pairs);
}

bool RunComparisonTests1() {
    auto pairs = {
        MakePair("(equal? nil nil)", "t"),
        MakePair("(equal? (quote (a b)) nil)", "nil"),
        MakePair("(equal? (quote (a b)) (quote (a b)))", "t"),
        MakePair("(equal? (quote (b a)) (quote (a b)))", "nil"),
        MakePair("(equal? 9 3)", "nil"),
        MakePair("(equal? (quotient 9 3) 3)", "t"),
        MakePair("(equal? 13 13)", "t"),
        MakePair("(equal? -9 4)", "nil"),
        MakePair("(equal? -80.1 -80.1)", "t"),
        MakePair("(equal? \"ist ein string\" \"ist ein string\")", "t"),
        MakePair("(equal? \"ist ein string\" \"ist ein anderer string\")", "nil"),
        MakePair("(zero? 4)", "nil"),
        MakePair("(zero? 0)", "t"),
        MakePair("(even? 28)", "t"),
        MakePair("(even? 29)", "nil"),
        MakePair("(odd? 17)", "t"),
        MakePair("(odd? 16)", "nil")
    };
    return AssertEval(pairs);
}

bool RunComparisonTests2() {
    auto pairs = {
        MakePair("(less? 9.78 3)", "nil"),
        MakePair("(less? 3 9.78)", "t"),
        MakePair("(less? 13 13)", "nil"),
        MakePair("(less? -9 4)", "t"),
        MakePair("(less? -80.23 -81.645)", "nil"),
        MakePair("(less? -5.1 5)", "t"),
        MakePair("(less? -5 -4.9)", "t"),
        MakePair("(less? \"B\" \"A\")", "nil"),
        MakePair("(less? \"A\" \"B\")", "t"),
        MakePair("(less? \"C\" \"C\")", "nil"),
        MakePair("(greater? 3 2)", "t"),
        MakePair("(greater? 3 3)", "nil"),
        MakePair("(greater? 2 3)", "nil"),
        MakePair("(greater-or-equal? 3 3)", "t"),
        MakePair("(greater-or-equal? 2 3)", "nil"),
        MakePair("(less-or-equal? 3 3)", "t"),
        MakePair("(less-or-equal? 2 3)", "t")
    };
    return AssertEval(pairs);
}

bool RunTypeTests() {
    auto pairs = {
        MakePair("(type-of nil)", "list"),
        MakePair("(type-of (cons (quote a) (quote (b))))", "list"),
        MakePair("(type-of (quote c))", "atom"),
        MakePair("(type-of (gensym))", "atom"),
        MakePair("(type-of 1)", "integer"),
        MakePair("(type-of (quotient 1 3))", "ratio"),
        MakePair("(type-of 3.14)", "float"),
        MakePair("(type-of \"abc\")", "string"),
        MakePair("(type-of (lambda (x) x))", "lambda"),
        MakePair("(list? nil)", "t"),
        MakePair("(list? (list 1))", "t"),
        MakePair("(list? 1)", "nil"),
        MakePair("(symbol? (quote c))", "t"),
        MakePair("(atom? (gensym))", "t"),
        MakePair("(atom? 23)", "t"),
        MakePair("(atom? \"23\")", "t"),
        MakePair("(atom? (list 23))", "nil"),
        MakePair("(integer? 1)", "t"),
        MakePair("(number? (quotient 1 3))", "t"),
        MakePair("(number? 3.14)", "t"),
        MakePair("(string? \"abc\")", "t")
    };
    return AssertEval(pairs);
}

bool RunTimeTests1() {
    long t1 = LONG_MAX - 5;
    long t2 = TimeSum(t1, 10);
    long t3 = TimeSum(t2, 10);
    long d12 = TimeDiff(t1, t2);
    long d13 = TimeDiff(t1, t3);
    long d21 = TimeDiff(t2, t1);
    long d23 = TimeDiff(t2, t3);
    long d31 = TimeDiff(t3, t1);
    long d32 = TimeDiff(t3, t2);
    return d12 == -10
        && d13 == -20
        && d21 == 10
        && d23 == -10
        && d31 == 20
        && d32 == 10;
}

bool RunTimeTests2() {
    long t1 = -5l;
    long t2 = TimeSum(t1, 10);
    long t3 = TimeSum(t2, 10);
    long d12 = TimeDiff(t1, t2);
    long d13 = TimeDiff(t1, t3);
    long d21 = TimeDiff(t2, t1);
    long d23 = TimeDiff(t2, t3);
    long d31 = TimeDiff(t3, t1);
    long d32 = TimeDiff(t3, t2);
    return d12 == -10
        && d13 == -20
        && d21 == 10
        && d23 == -10
        && d31 == 20
        && d32 == 10;
}

bool RunTaskTests() {
    auto pairs = {
        MakePair("(greater? (get-monotonic-time) 0)", "t"),
        MakePair("(progn (now t) (run-tasks 1000))", "1"),
        MakePair("(progn (now t) (drain-tasks))", "1"),
        MakePair("(let ((start (get-monotonic-time))) (progn (after (+ 100 200) t) (run-tasks 1000) (greater? (- (get-monotonic-time) start) 299)))", "t"),
        MakePair("(progn (when-then t t) (run-tasks 1000))", "1"),
        MakePair("(let ((start (get-monotonic-time))) (progn (when-for-then t (+ 200 100) t) (run-tasks 1000) (greater? (- (get-monotonic-time) start) 299)))", "t"),
        MakePair("(progn (priority-now (+ 1 1) t) (run-tasks 1000))", "1"),
        MakePair("(let ((start (get-monotonic-time))) (progn (priority-after (+ 1 2) (+ 100 200) t) (run-tasks 1000) (greater? (- (get-monotonic-time) start) 299)))", "t"),
        MakePair("(progn (priority-when-then (+ 2 1) t t) (run-tasks 1000))", "1"),
        MakePair("(let ((start (get-monotonic-time))) (progn (priority-when-for-then (+ 2 2) t (+ 200 100) t) (run-tasks 1000) (greater? (- (get-monotonic-time) start) 299)))", "t"),
        MakePair("(let ((res nil)) (progn (only-one-of (when-then (equal? 1 2) (setq res 1)) (when-for-then (less? 1 2) 300 (setq res 2))) (run-tasks 1000) res))", "2")
    };
    return AssertEval(pairs);
}

bool RunFileTests() {
    auto pairs = {
        MakePair("(and (open \"test.txt\") (delete-file \"test.txt\"))", "t"),
        MakePair("(let ((stream (open \"test.txt\"))) (and (write-char stream \"(setq hallo \") (write-char stream \" 4)\") (close stream)))", "t"),
        MakePair("(load \"test.txt\")", "t"),
        MakePair("hallo", "4"),
        MakePair("(unbind (quote hallo))", "t"),
        MakePair("(delete-file \"test.txt\")", "t")
    };
    return AssertEval(pairs);
}

bool RunDisplayTests() {
    auto pairs = {
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (type-of disp)))", "display"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp bounds) (set-position disp 4 5) (pen-down disp) (forward disp 9) (left disp 90) (forward disp 10) (get-bounds disp)))", "(4 5 10 11)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (get-pixel disp 2 2)))", "nil"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (set-pixel disp 2 2 (list 0 0 0))))", "nil"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (fill disp (list 255 255 255))))", "t"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (fill disp (list 255 255 255)) (get-pixel disp 0 0)))", "(255 255 255)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (fill disp (list 255 255 255)) (set-pixel disp 0 0 (list 0 0 0)) (get-pixel disp 0 0)))", "(0 0 0)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (pen-up? disp)))", "t"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (pen-down? disp)))", "nil"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (pen-down disp) (pen-up? disp)))", "nil"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (pen-down disp) (pen-down? disp)))", "t"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (pen-down disp) (pen-up disp) (pen-down? disp)))", "nil"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 1 1) (fill disp (list 0 0 0)) (set-pen-color disp (list 255 255 255)) (pen-down disp) (forward disp 1) (get-pixel disp 0 0)))", "(255 255 255)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 2 2) (fill disp (list 255 255 255)) (pen-down disp) (set-heading disp 90) (forward disp 2) (get-pixel disp 0 1)))", "(0 0 0)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 2 2) (fill disp (list 255 255 255)) (pen-down disp) (towards disp 1 1) (forward disp 3) (get-pixel disp 1 1)))", "(0 0 0)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 2 2) (fill disp (list 255 255 255)) (pen-down disp) (left disp 90) (forward disp 2) (get-pixel disp 0 1)))", "(0 0 0)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 2 2) (fill disp (list 255 255 255)) (pen-down disp) (right disp -90) (back disp -2) (get-pixel disp 0 1)))", "(0 0 0)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 2 2) (fill disp (list 255 255 255)) (pen-down disp) (set-position disp 1 1) (get-pixel disp 1 1)))", "(0 0 0)"),
        MakePair("(let ((disp nil)) (progn (defdisplay disp monochrome 100 16) (fill disp (list 255 255 255)) (pen-down disp) (label disp \"Hallo\" 12) (get-pixel (flush disp) 1 1)))", "(255 255 255)")
    };
    return AssertEval(pairs);
}

bool RunGpioTests() {
    auto pairs = {
        MakePair("(type-of (create-gpio 4 (quote digital-input)))", "gpio"),
        MakePair("(let ((pin4 nil)) (progn (defgpio pin4 4 digital-input) (read-gpio pin4)))", "nil"),
        MakePair("(type-of (let ((pin4 nil)) (progn (defgpio pin4 4 digital-output) (write-gpio pin4 t))))", "gpio")
    };
    return AssertEval(pairs);
}

bool RunWlanTests() {
    auto pairs = {
        MakePair("(let ((wlan nil)) (progn (defwlan wlan \"hostname\" (list (list \"ssid\" \"password\"))) (connect-wlan wlan)))", "nil"),
        MakePair("(let ((wlan nil)) (progn (defwlan wlan \"hostname\" (list (list \"ssid\" \"password\"))) (connected-wlan? wlan)))", "t")
    };
    return AssertEval(pairs);
}

bool RunSocketTests() {
    auto pairs = {
        MakePair("(type-of (create-socket \"renewable-1609.herokuapp.com\" 80))", "socket"),
        MakePair("(create-socket \"nee.blup.schlup.de\" 443)", "nil"),
        MakePair("(create-socket \"google.de\" 65537)", "nil")
    };
    return AssertEval(pairs);
}

bool RunBrokerTests() {
    auto pairs = {
        MakePair("(type-of (setq broker (create-broker (create-socket \"simplysomethings.de\" 1883) (get-env \"CLIENT_ID\") (get-env \"BROKER_USER\") (get-env \"BROKER_PASSWORD\"))))", "broker"),
        MakePair("(greater? (run-tasks 1000) 0)", "t"),
        MakePair("(and (connected-broker? broker) t)", "t"),
        MakePair("(and (ping broker) t)", "t"),
        MakePair("(greater? (run-tasks 1000) 0)", "t"),
        MakePair("(and (publish broker \"value/7b67ab1b-ead2-4ef4-91fb-91173af1cf47/activity-count\" 1 t) t)", "t"),
        MakePair("(unbind (quote broker))", "t"),
        MakePair("(and (setq broker (create-broker (create-socket \"broker.hivemq.com\" 1883) \"micro-lisp\" \"\" \"\")) t)", "t"),
        MakePair("(greater? (run-tasks 1000) 0)", "t"),
        MakePair("(and (publish broker \"micro-lisp-topic\" \"42!\" t) t)", "t"),
        MakePair("(unbind (quote broker))", "t"),
        MakePair("(and (setq broker (create-broker (create-socket \"broker.hivemq.com\" 1883) \"micro-lisp\" \"\" \"\")) t)", "t"),
        MakePair("(greater? (run-tasks 1000) 0)", "t"),
        MakePair("(setq received-message nil)", "nil"),
        MakePair("(and (subscribe broker \"micro-lisp-topic\" (message) (setq received-message message)) t)", "t"),
        MakePair("(greater? (run-tasks 2000) 0)", "t"),
        MakePair("(equal? received-message \"42!\")", "t"),
        MakePair("(unbind (quote received-message))", "t"),
        MakePair("(unsubscribe broker \"lalelu\")", "nil"),
        MakePair("(and (unsubscribe broker \"micro-lisp-topic\") t)", "t"),
        MakePair("(greater? (run-tasks 1000) 0)", "t"),
        MakePair("(unbind (quote broker))", "t")
     };
    return AssertEval(pairs);
}

bool RunJsonTests() {
    auto pairs = {
        MakePair("(type-of (parse-json \"23\"))", "integer"),
        MakePair("(parse-json \"123\")", "123"),
        MakePair("(type-of (parse-json \"1e-2\"))", "float"),
        MakePair("(parse-json \"-3.14\")", "-3.14"),
        MakePair("(parse-json \"\\\"hallo\\\"\")", "\"hallo\""),
        MakePair("(parse-json \"tr\")", "nil"),
        MakePair("(parse-json \"true\")", "t"),
        MakePair("(parse-json \"false\")", "nil"),
        MakePair("(parse-json \"null\")", "nil"),
        MakePair("(parse-json \"[]\")", "nil"),
        MakePair("(parse-json \"[1\")", "nil"),
        MakePair("(parse-json \"[1]\")", "(1)"),
        MakePair("(parse-json \"[1, \")", "nil"),
        MakePair("(parse-json \"[1, 2\")", "nil"),
        MakePair("(parse-json \"[1, 2]\")", "(1 2)"),
        MakePair("(parse-json \"[1, 2, false]\")", "(1 2 nil)"),
        MakePair("(parse-json \"{}\")", "nil"),
        MakePair("(parse-json \"{1:\")", "nil"),
        MakePair("(parse-json \"{1: 2\")", "nil"),
        MakePair("(parse-json \"{1: 2}\")", "((1 2))"),
        MakePair("(parse-json \"{1: 2, \")", "nil"),
        MakePair("(parse-json \"{1: 2, 2: true, 3: false, 4: null}\")", "((4 nil)(3 nil)(2 t)(1 2))")
    };
    return AssertEval(pairs);
}

bool RunTests() {
    return RunAbstractionTests()
        && RunControlTests()
        && RunImperativeTests()
        && RunCatchThrowTests()
        && RunArithmeticTests1()
        && RunArithmeticTests2()
        && RunStringTests()
        && RunListTests1()
        && RunListTests2()
        && RunListTests3()
        && RunListTests4()
        && RunSetTests()
        && RunComparisonTests1()
        && RunComparisonTests2()
        && RunTypeTests()
        && RunTimeTests1()
        && RunTimeTests2()
        && RunTaskTests()
        && RunFileTests()
        && RunDisplayTests()
        && RunGpioTests()
        && RunWlanTests()
        && RunSocketTests()
        && RunBrokerTests()
        && RunJsonTests();
}

}

#endif
