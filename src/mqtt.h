#ifndef MQTT_H
#define MQTT_H
#include <string>
#include <list>
#include <functional>

namespace mqtt {

class BaseRequest {
    public:
        BaseRequest(int p, int f) { size = 0; packet_type = p; flags = f; }
        virtual ~BaseRequest() { }
        bool WriteToSocket(int fd); // muss aufgerufen werden, bevor & Strings ihre Gültigkeit verlieren
    protected:
        void AppendByte(uint8_t b);
        void AppendShort(uint16_t s);
        void AppendString(const std::string &s);
        void AppendLenString(const std::string &s) { AppendShort(s.length()); AppendString(s); }
    private:
        std::list<std::function<bool(int)>> content;
        int size;
        int packet_type;
        int flags;
};

extern const std::string mqtt;

class ConnectRequest : public BaseRequest {
    public:
        ConnectRequest(const std::string &clientid, const std::string &username, const std::string &password) : BaseRequest(1, 0) {
            AppendLenString(mqtt);
            AppendByte(4); // Level
            AppendByte(2 | 64 | 128); // Clean Session, Password, User Name
            AppendShort(600); // Keep Alive
            AppendLenString(clientid);
            AppendLenString(username);
            AppendLenString(password);
        }
        virtual ~ConnectRequest() { }
};

class PublishRequest : public BaseRequest {
    public:
        PublishRequest(const std::string &topic_name, const std::string &payload, bool retain) : BaseRequest(3, retain ? 1 : 0) {
            AppendLenString(topic_name);
            AppendString(payload);
        }
        virtual ~PublishRequest() { }
};

class SubscribeRequest : public BaseRequest {
    public:
        SubscribeRequest(const std::string &topic_filter, uint16_t packet_id, uint8_t qos = 0) : BaseRequest(8, 2) {
            AppendShort(packet_id);
            AppendLenString(topic_filter);
            AppendByte(qos);
        }
        virtual ~SubscribeRequest() { }
};

class PublishAcknowledgement : public BaseRequest {
    public:
        PublishAcknowledgement(uint16_t packet_id) : BaseRequest(4, 0) {
            AppendShort(packet_id);
        }
        virtual ~PublishAcknowledgement() { }
};

class UnsubscribeRequest : public BaseRequest {
    public:
        UnsubscribeRequest(const std::string &topic_filter, uint16_t packet_id) : BaseRequest(10, 2) {
            AppendShort(packet_id);
            AppendLenString(topic_filter);
        }
        virtual ~UnsubscribeRequest() { }
};

class PingRequest : public BaseRequest {
    public:
        PingRequest() : BaseRequest(12, 0) { }
        virtual ~PingRequest() { }
};

class DisconnectRequest : public BaseRequest {
    public:
        DisconnectRequest() : BaseRequest(14, 0) { }
        virtual ~DisconnectRequest() { }
};

class BaseResponse {
    public:
        BaseResponse(int p, int f) { packet_type = p; flags = f; }
        virtual ~BaseResponse() { }
        int GetPacketType() const { return packet_type; }
        static BaseResponse* ReadFromSocket(int fd);
    protected:
        static int ReadByte(int fd);
        static int ReadShort(int fd);
        static int ReadString(int fd, std::string &str, int len);
        static int ReadLenString(int fd, std::string &str);
        int flags;
    private:
        int packet_type;
};

class ConnectAcknowledgement : public BaseResponse {
    public:
        ConnectAcknowledgement(int p, int f, int fd);
        virtual ~ConnectAcknowledgement() { }
        bool IsConnectionAccepted() const { return return_code == 0; }
    private:
        int session_present;
        int return_code;
};

class PublishNotification : public BaseResponse {
    public:
        PublishNotification(int p, int f, int s, int fd);
        virtual ~PublishNotification() { }
        int GetPacketId() const { return packet_id; }
        std::string GetTopicName() const { return topic_name; }
        std::string GetPayload() const { return payload; }
    private:
        int packet_id;
        std::string topic_name;
        std::string payload;
};

class SubscribeAcknowledgement : public BaseResponse {
    public:
        SubscribeAcknowledgement(int p, int f, int fd);
        virtual ~SubscribeAcknowledgement() { }
        int GetPacketId() const { return packet_id; }
        bool IsSubscriptionAccepted() const { return return_code != 128; }
    private:
        int packet_id;
        int return_code;
};

class UnsubscribeAcknowledgement : public BaseResponse {
    public:
        UnsubscribeAcknowledgement(int p, int f, int fd);
        virtual ~UnsubscribeAcknowledgement() { }
        int GetPacketId() const { return packet_id; }
    private:
        int packet_id;
};

class PingResponse : public BaseResponse {
    public:
        PingResponse(int p, int f) : BaseResponse(p, f) { }
        virtual ~PingResponse() { }
};

void Delete(BaseRequest* request);

void Delete(BaseResponse* response);

}

#endif
