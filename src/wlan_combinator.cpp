#include <new>
#include "wlan_combinator.h"
#include "list.h"
#include "string.h"
#include "wlan.h"
#include "boolean.h"
#include "hal.h"

namespace lisp {

bool IsStringPairList(Expression* expr1) {
    if (IsList(expr1)) {
        auto pairlist = (List*) expr1;
        while (pairlist) {
            auto expr2 = pairlist->GetFirst();
            if (IsList(expr2)) {
                auto pair = (List*) expr2;
                if (!IsString(pair->GetFirst())) {
                    return false;
                }
                pair = pair->GetRest();
                if (!pair) {
                    return false;
                }
                if (!IsString(pair->GetFirst())) {
                    return false;
                }
            } else {
                return false;
            }
            pairlist = pairlist->GetRest();
        }
        return true;
    }
    return false;
}

bool CreateWlan2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsString(expr1) && IsStringPairList(expr2)) {
        auto hostname = (String*) expr1;
        auto pairlist = (List*) expr2;
        auto credentials = new(std::nothrow) std::list<std::tuple<std::string, std::string>>();
        if (credentials) {
            while (pairlist) {
                auto pair = (List*) pairlist->GetFirst();
                auto ssid = (String*) pair->GetFirst();
                pair = pair->GetRest();
                auto password = (String*) pair->GetFirst();
                credentials->push_back(std::tuple<std::string, std::string>(ssid->GetString(), password->GetString()));
                pairlist = pairlist->GetRest();
            }
            auto wlan = new(std::nothrow) Wlan(hostname->GetString(), credentials);
            auto success = wlan && Push(wlan, sstack);
            Delete(wlan);
            return success;
        }
    }
    return false;
}

bool ConnectWlan1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsWlan(expr)) {
        auto wlan = (Wlan*) expr;
        if (hal::ConnectWlan(wlan->GetCredentials()) && hal::SetHostname(wlan->GetHostname())) {
            return Push(wlan, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool IsConnectedWlan1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsWlan(expr)) {
        if (hal::IsWlanConnected()) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

}
