#ifndef RATIONAL_H
#define RATIONAL_H
#include "number.h"

namespace lisp {

class Rational : public Number {
    public:
        Rational(long n, long d) { auto g = Gcd(n, d); numerator = n / g; denominator = d / g; }
        virtual ~Rational() { }
        virtual ExpressionType GetType() { return denominator == 1l ? ExpressionType::INTEGER : ExpressionType::RATIO; }
        virtual double GetDouble() { return (double) numerator / (double) denominator; }
        long GetNumerator() { return numerator; }
        long GetDenominator() { return denominator; }
    private:
        long numerator;
        long denominator;
        static long Gcd(long a, long b) {
            if (a < 0  && b < 0) {
                return -Gcd(-a, -b);
            }
            if (a < 0) {
                return Gcd(-a, b);
            }
            if (b < 0) {
                return -Gcd(a, -b); // Nenner wird immer positiv
            }
            while (b != 0) {
                auto c = b;
                b = a % b;
                a = c;
            }
            return a;
        };
};

}

#endif
