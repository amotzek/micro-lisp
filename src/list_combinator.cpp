#include <queue>
#include <vector>
#include "list_combinator.h"
#include "rational.h"
#include "symbol.h"
#include "boolean.h"
#include "eval.h"

namespace lisp {

bool Cons2::ProcessValues(Expression* first, Expression* rest, Action* &cstack, List* &sstack, Environment* env) {
    if (IsNilOrList(rest)) {
        List* value;
        if (Cons(first, (List*) rest, value)) {
            auto success = Push(value, sstack);
            Delete(value);
            return success;
        }
    }
    return false;
}

bool First1::ProcessValue(Expression* list, Action* &cstack, List* &sstack, Environment* env) {
    if (IsList(list)) {
        return Push(((List*) list)->GetFirst(), sstack);
    }
    return false;
}

bool Rest1::ProcessValue(Expression* list, Action* &cstack, List* &sstack, Environment* env) {
    if (IsList(list)) {
        return Push(((List*) list)->GetRest(), sstack);
    }
    return false;
}

bool IsNull1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (expr) {
        return PushFalse(sstack);
    }
    return PushTrue(sstack);
}

bool ListLength1::ProcessValue(Expression* list, Action* &cstack, List* &sstack, Environment* env) {
    if (IsNilOrList(list)) {
        auto length = Length((List*) list);
        auto value = new(std::nothrow) Rational(length, 1l);
        if (value) {
            auto success = Push(value, sstack);
            Delete(value);
            return success;
        }
    }
    return false;
}

bool Reverse1::ProcessValue(Expression* list, Action* &cstack, List* &sstack, Environment* env) {
    if (IsNilOrList(list)) {
        List* reversed_list = nullptr;
        auto success = Reverse((List*) list, reversed_list) && Push(reversed_list, sstack);
        Delete(reversed_list);
        return success;
    }
    return false;
}

class ExpressionComparator {
    public:
        ExpressionComparator(Expression* comp, Environment* env, bool &succ, ThrowAction* &act) : success(succ), throw_action(act) { compare = comp; environment = env; }
        bool operator() (Expression* left, Expression* right) {
            if (success) {
                success = false;
                Expression* quote = Intern("quote");
                List* quoted_left = nullptr;
                List* quoted_right = nullptr;
                List* expression = nullptr;
                Expression* result = nullptr;
                if (Pair(quote, left, quoted_left)
                 && Pair(quote, right, quoted_right)
                 && Push(quoted_right, expression)
                 && Push(quoted_left, expression)
                 && Push(compare, expression)) {
                    success = InnerEval(expression, environment, result, throw_action);
                    Delete(result);
                }
                Delete(expression);
                Delete(quoted_right);
                Delete(quoted_left);
                Delete(quote);
                return success && result;
            }
            return false;
        }
    private:
        Expression* compare;
        Environment* environment;
        bool &success;
        ThrowAction* &throw_action;
};

bool Sort2::ProcessValues(Expression* list, Expression* compare, Action* &cstack, List* &sstack, Environment* env) {
    if (IsNilOrList(list) && IsFunction(compare)) {
        auto success = true;
        ThrowAction* throw_action = nullptr;
        auto comparator = ExpressionComparator(compare, env, success, throw_action);
        std::priority_queue<Expression*, std::vector<Expression*>, ExpressionComparator> queue(comparator);
        auto unsorted_list = (List*) list;
        while (unsorted_list && success) {
            queue.push(unsorted_list->GetFirst());
            unsorted_list = unsorted_list->GetRest();
        }
        List* sorted_list = nullptr;
        while (!queue.empty() && success) {
            success = Push(queue.top(), sorted_list);
            queue.pop();
        }
        if (!success) {
            Delete(sorted_list);
            if (throw_action) {
                return Push(throw_action, cstack);
            }
            return false;
        }
        success = Push(sorted_list, sstack);
        Delete(sorted_list);
        return success;
    }
    return false;
}

}
