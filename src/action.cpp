#include "action.h"

namespace lisp {

bool Push(Action* action, Action* &cstack) {
    if (action) {
        action->SetNext(cstack);
        cstack = action;
        return true;
    }
    return false;
}

Action* Pop(Action* &cstack) {
    auto t = cstack;
    cstack = cstack->GetNext();
    t->SetNext(nullptr);
    return t;
}

void Delete(Action* action) {
    delete action;
}

}
