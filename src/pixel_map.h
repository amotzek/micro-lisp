#ifndef PIXEL_MAP_H
#define PIXEL_MAP_H
#include <new>
#include <vector>

namespace lisp {

class PixelMap {
    public:
        PixelMap() { next = nullptr; }
        virtual ~PixelMap() { }
        virtual void Init() { }
        void SetNext(PixelMap* n) { next = n; }
        virtual void GetOrigin(int &x, int &y) const = 0;
        virtual int GetWidth() const = 0;
        virtual int GetHeight() const = 0;
        virtual bool GetPixel(int x, int y, int &r, int &g, int &b) const = 0;
        virtual bool SetPixel(int x, int y, int r, int g, int b) = 0;
        virtual bool Fill(int r, int g, int b);
        virtual bool Flush();
    private:
        PixelMap* next;
};

class Bounds : public PixelMap {
    public:
        Bounds() : PixelMap() { first_pixel = true; x_min = 0; y_min = 0; x_max = 0; y_max = 0; }
        virtual ~Bounds() { }
        virtual void GetOrigin(int &x, int &y) const { x = x_min; y = y_min; }
        virtual int GetWidth() const { return x_max - x_min + 1; }
        virtual int GetHeight() const { return y_max - y_min + 1; }
        virtual bool GetPixel(int x, int y, int &r, int &g, int &b) const { return false; }
        virtual bool SetPixel(int x, int y, int r, int g, int b) {
            if (first_pixel) {
                x_min = x;
                y_min = y;
                x_max = x;
                y_max = y;
                first_pixel = false;
                return true;
            }
            if (x < x_min) {
                x_min = x;
            }
            if (y < y_min) {
                y_min = y;
            }
            if (x > x_max) {
                x_max = x;
            }
            if (y > y_max) {
                y_max = y;
            }
            return true;
        }
        virtual bool Fill(int r, int g, int b) { return false; }
        virtual bool Flush() { return true; }
    private:
        bool first_pixel;
        int x_min;
        int y_min;
        int x_max;
        int y_max;
};

class FixedSizePixelMap : public PixelMap {
    public:
        FixedSizePixelMap(int w, int h) : PixelMap() { width = w; height = h; }
        virtual ~FixedSizePixelMap() { }
        virtual void GetOrigin(int &x, int &y) const { x = 0; y = 0; }
        virtual int GetWidth() const { return width; };
        virtual int GetHeight() const { return height; };
    protected:
        bool IsInside(int x, int y) const { return x >= 0 && y >= 0 && x < width && y < height; }
    private:
        int width;
        int height;
};

class MonochromePixelMap : public FixedSizePixelMap {
    public:
        MonochromePixelMap(int w, int h) : FixedSizePixelMap(w, h) { pixels = new(std::nothrow) std::vector<std::vector<bool>>(GetWidth(), std::vector<bool>(GetHeight())); }
        virtual ~MonochromePixelMap() { delete pixels; }
        virtual bool GetPixel(int x, int y, int &r, int &g, int &b) const;
        virtual bool SetPixel(int x, int y, int r, int g, int b);
    private:
        std::vector<std::vector<bool>>* pixels;
};

}

#endif
