#ifndef LOADER_H
#define LOADER_H
#include <istream>
#include "environment.h"

namespace lisp {

bool Load(std::istream* istream, Environment* environment, bool verbose = false);

}

#endif