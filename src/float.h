#ifndef FLOAT_H
#define FLOAT_H
#include "number.h"

namespace lisp {

class Float : public Number {
    public:
        Float(double v) { value = v; }
        virtual ~Float() { }
        virtual ExpressionType GetType() { return ExpressionType::FLOAT; }
        virtual double GetDouble() { return value; }
    private:
        double value;
};

}

#endif
