#ifndef NUMBER_H
#define NUMBER_H
#include "expression.h"

namespace lisp {

class Number : public Expression {
    public:
        Number() { }
        virtual ~Number() { }
        virtual double GetDouble() = 0;
};

}

#endif
