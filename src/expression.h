#ifndef EXPRESSION_H
#define EXPRESSION_H
#include "reference_counted.h"

namespace lisp {

enum class ExpressionType {
    LIST,
    SYMBOL,
    INTEGER,
    RATIO,
    FLOAT,
    STRING,
    COMBINATOR,
    LAMBDA,
    MACRO,
    TASK,
    STREAM,
    GPIO,
    DISPLAY,
    WLAN,
    SOCKET,
    BROKER
};

class Expression : public ReferenceCounted {
    public:
        Expression() { }
        virtual ~Expression() { }
        virtual ExpressionType GetType() = 0;
        bool HasType(ExpressionType type) { return GetType() == type; }
};

bool IsNilOrList(Expression* expr);

bool IsList(Expression* expr);

bool IsNilOrSymbol(Expression* expr);

bool IsSymbol(Expression* expr);

bool IsInteger(Expression* expr);

bool IsRational(Expression* expr);

bool IsFloat(Expression* expr);

bool IsNumber(Expression* expr);

bool IsString(Expression* expr);

bool IsFunction(Expression* expr);

bool IsTask(Expression* expr);

bool IsStream(Expression* expr);

bool IsGpio(Expression* expr);

bool IsDisplay(Expression* expr);

bool IsWlan(Expression* expr);

bool IsSocket(Expression* expr);

bool IsBroker(Expression* expr);

}

#endif
