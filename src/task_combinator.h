#ifndef TASK_COMBINATOR_H
#define TASK_COMBINATOR_H
#include "combinator.h"
#include "unary_combinator.h"
#include "binary_combinator.h"
#include "ternary_combinator.h"
#include "quaternary_combinator.h"

namespace lisp {

class GetMonotonicTime0 : public Combinator {
    public:
        GetMonotonicTime0() { }
        virtual ~GetMonotonicTime0() { }
        virtual int GetParameterCount() { return 0; }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env);
};

class Now1 : public UnaryCombinator {
    public:
        Now1() { }
        virtual ~Now1() { }
        virtual bool MustBeQuoted(int argument_index) { return true; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class After2 : public BinaryCombinator {
    public:
        After2() { }
        virtual ~After2() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index == 1; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class WhenThen2 : public BinaryCombinator {
    public:
        WhenThen2() { }
        virtual ~WhenThen2() { }
        virtual bool MustBeQuoted(int argument_index) { return true; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class WhenForThen3 : public TernaryCombinator {
    public:
        WhenForThen3() { }
        virtual ~WhenForThen3() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index != 1; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class PriorityNow2 : public BinaryCombinator {
    public:
        PriorityNow2() { }
        virtual ~PriorityNow2() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index == 1; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class PriorityAfter3 : public TernaryCombinator {
    public:
        PriorityAfter3() { }
        virtual ~PriorityAfter3() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index == 2; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class PriorityWhenThen3 : public TernaryCombinator {
    public:
        PriorityWhenThen3() { }
        virtual ~PriorityWhenThen3() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index > 0; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class PriorityWhenForThen4 : public QuaternaryCombinator {
    public:
        PriorityWhenForThen4() { }
        virtual ~PriorityWhenForThen4() { }
        virtual bool MustBeQuoted(int argument_index) { return argument_index == 1 || argument_index == 3; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env);
};

class OnlyOneOfList1 : public UnaryCombinator {
    public:
        OnlyOneOfList1() { }
        virtual ~OnlyOneOfList1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class RunTasks1 : public UnaryCombinator {
    public:
        RunTasks1() { }
        virtual ~RunTasks1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class DrainTasks0 : public Combinator {
    public:
        DrainTasks0() { }
        virtual ~DrainTasks0() { }
        virtual int GetParameterCount() { return 0; }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
