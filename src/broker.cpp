#include <new>
#include "broker.h"
#include "cooperative_multitasking.h"
#include "mqtt.h"

namespace lisp {

Subscription::Subscription(std::string t, uint16_t i, long prio, Symbol* p, Expression* c, Environment* e) {
    topic_filer = t;
    packet_id = i;
    priority = prio;
    parameter = Copy(p);
    continuation = Copy(c);
    environment = Copy(e);
}

Subscription::~Subscription() {
    Delete(parameter);
    Delete(continuation);
    Delete(environment);
}

bool Subscription::PushTask(Expression* value) const {
    auto child = new(std::nothrow) SmallEnvironment(environment);
    auto success = child && child->AddPair(parameter, value) && tasks.Now(priority, continuation, child);
    Delete(child);
    return success;
}

Broker::Broker(Socket* s) {
    socket = Copy(s);
    alive = true;
    connected = false;
    packet_id = 0;
}

Broker::~Broker() {
    if (alive && connected) {
        auto request = new(std::nothrow) mqtt::DisconnectRequest();
        if (request) {
            request->WriteToSocket(socket->GetFd());
        }
        Delete(request);
    }
    Delete(socket);
    for (Subscription* subscription : subscriptions) {
        delete subscription;
    }
    subscriptions.clear();
}

uint16_t Broker::NextPacketId() {
    packet_id++;
    if (packet_id == 0) {
        packet_id = 1;
    }
    return packet_id;
}

bool Broker::AddSubscription(std::string t, uint16_t i, long prio, Symbol* p, Expression* c, Environment* e) {
    auto subscription = new(std::nothrow) Subscription(t, i, prio, p, c, e);
    if (subscription) {
        subscriptions.push_back(subscription);
        return true;
    }
    return false;
}

void Broker::RemoveSubscription(uint16_t i) {
    subscriptions.remove_if([i](Subscription* s) {
        if (s->HasPacketId(i)) {
            delete s;
            return true;
        }
        return false;
    });
}

Subscription* Broker::FindSubscription(std::string topic_name) const {
    for (auto subscription : subscriptions) {
        if (subscription->MatchesTopic(topic_name)) {
            return subscription;
        }
    }
    return nullptr;
}

}
