#ifndef QUATERNARY_COMBINATOR_H
#define QUATERNARY_COMBINATOR_H
#include "combinator.h"

namespace lisp {

class QuaternaryCombinator : public Combinator {
    public:
        QuaternaryCombinator() { }
        virtual ~QuaternaryCombinator() { }
        virtual int GetParameterCount() { return 4; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env) = 0;
        virtual bool Perform(Action* &cstack, List* &sstack, Environment* env) {
            bool success = false;
            if (sstack) {
                auto v1 = Pop(sstack);
                if (sstack) {
                    auto v2 = Pop(sstack);
                    if (sstack) {
                        auto v3 = Pop(sstack);
                        if (sstack){
                            auto v4 = Pop(sstack);
                            success = ProcessValues(v1, v2, v3, v4, cstack, sstack, env);
                            Delete(v4);
                        }
                        Delete(v3);
                    }
                    Delete(v2);
                }
                Delete(v1);
            }
            return success;
        }
};

}

#endif
