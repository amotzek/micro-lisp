#ifndef FILE_COMBINATOR_H
#define FILE_COMBINATOR_H
#include <string>
#include "unary_combinator.h"
#include "binary_combinator.h"

namespace lisp {

class Open1 : public UnaryCombinator {
    public:
        Open1() { }
        virtual ~Open1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class WriteChar2 : public BinaryCombinator {
    public:
        WriteChar2() { }
        virtual ~WriteChar2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Close1 : public UnaryCombinator {
    public:
        Close1() { }
        virtual ~Close1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Load1 : public UnaryCombinator {
    public:
        Load1() { }
        virtual ~Load1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class DeleteFile1 : public UnaryCombinator {
    public:
        DeleteFile1() { }
        virtual ~DeleteFile1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
