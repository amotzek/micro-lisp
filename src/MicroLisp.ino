#include "Arduino.h"
#include "arduino_sal.h"

void setup() {
    Serial.begin(115200);
    while (!Serial) {
        delay(2000);
    }
    HardwareSetup();
    LoadSetup();
}

void loop() {
    if (Serial.available()) {
        ReadEvalPrintConsumeChar(Serial.read());
    } else if (HasTask(100l)) {
        RunTask();
    } else {
        delay(100);
    }
}
