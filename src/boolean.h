#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "list.h"

namespace lisp {

bool PushTrue(List* &sstack);

bool PushFalse(List* &sstack);

}

#endif
