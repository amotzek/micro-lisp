#ifndef DISPLAY_COMBINATOR_H
#define DISPLAY_COMBINATOR_H
#include "unary_combinator.h"
#include "binary_combinator.h"
#include "ternary_combinator.h"
#include "quaternary_combinator.h"

namespace lisp {

class CreateDisplay2 : public BinaryCombinator {
    public:
        CreateDisplay2() { }
        virtual ~CreateDisplay2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class GetBounds1 : public UnaryCombinator {
    public:
        GetBounds1() { }
        virtual ~GetBounds1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class Fill2 : public BinaryCombinator {
    public:
        Fill2() { }
        virtual ~Fill2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class GetPixel3 : public TernaryCombinator {
    public:
        GetPixel3() { }
        virtual ~GetPixel3() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class SetPixel4 : public QuaternaryCombinator {
    public:
        SetPixel4() { }
        virtual ~SetPixel4() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env);
};

class IsPenDown1 : public UnaryCombinator {
    public:
        IsPenDown1() { }
        virtual ~IsPenDown1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class IsPenUp1 : public UnaryCombinator {
    public:
        IsPenUp1() { }
        virtual ~IsPenUp1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class PenDown1 : public UnaryCombinator {
    public:
        PenDown1() { }
        virtual ~PenDown1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class PenUp1 : public UnaryCombinator {
    public:
        PenUp1() { }
        virtual ~PenUp1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

class SetPenColor2 : public BinaryCombinator {
    public:
        SetPenColor2() { }
        virtual ~SetPenColor2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class SetHeading2 : public BinaryCombinator {
    public:
        SetHeading2() { }
        virtual ~SetHeading2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Towards3 : public TernaryCombinator {
    public:
        Towards3() { }
        virtual ~Towards3() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class Left2 : public BinaryCombinator {
    public:
        Left2() { }
        virtual ~Left2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Right2 : public BinaryCombinator {
    public:
        Right2() { }
        virtual ~Right2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class SetPosition3 : public TernaryCombinator {
    public:
        SetPosition3() { }
        virtual ~SetPosition3() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class Forward2 : public BinaryCombinator {
    public:
        Forward2() { }
        virtual ~Forward2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Back2 : public BinaryCombinator {
    public:
        Back2() { }
        virtual ~Back2() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env);
};

class Label3 : public TernaryCombinator {
    public:
        Label3() { }
        virtual ~Label3() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env);
};

class Flush1 : public UnaryCombinator {
    public:
        Flush1() { }
        virtual ~Flush1() { }
        virtual bool MustBeQuoted(int argument_index) { return false; }
        virtual bool ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env);
};

}

#endif
