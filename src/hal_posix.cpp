#ifndef ARDUINO
#include <new>
#include <typeinfo>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <unistd.h>
#include "time.h"
#include "reference_counted.h"

namespace lisp {

std::string GetTypeName(ReferenceCounted* instance) {
    return typeid(*instance).name();
}

}

namespace hal {

long GetMonotonicTime() {
    timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0) {
        return 1000l * ts.tv_sec + (ts.tv_nsec / 1000000ul);
    }
    return 0l;
}

void Sleep(long ms) {
    if (ms > 0l) {
        timespec ts;
        ts.tv_sec = ms / 1000l;
        ts.tv_nsec = 1000000 * (ms % 1000);
        nanosleep(&ts, nullptr);
    }
}

std::istream* CreateFileStream(std::string filename) {
    return new(std::nothrow) std::ifstream(filename);
}

bool AppendTextFile(std::string filename, std::string text) {
    std::ofstream file;
    file.open(filename, std::ios_base::app); // Append
    file << text;
    return file.good();
}

bool DeleteFile(std::string filename) {
    return remove(filename.c_str()) == 0;
}

int CreateSocket(std::string hostname, uint16_t port) {
    // https://stackoverflow.com/questions/52727565/client-in-c-use-gethostbyname-or-getaddrinfo
    struct addrinfo hints = {}, *addrs;
    std::string port_str = std::to_string(port);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    if (getaddrinfo(hostname.c_str(), port_str.c_str(), &hints, &addrs) != 0) {
        return -1;
    }
    for (struct addrinfo *addr = addrs; addr != NULL; addr = addr->ai_next) {
        int fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
        if (fd < 0) {
            continue;
        }
        if (connect(fd, addr->ai_addr, addr->ai_addrlen) == 0) {
            freeaddrinfo(addrs);
            return fd;
        }
        close(fd);
    }
    freeaddrinfo(addrs);
    return -1;
}

bool WriteSocket(int fd, uint8_t data) {
    return write(fd, &data, 1) == 1;
}

bool WriteSocket(int fd, const uint8_t *buffer, size_t size) {
    auto res = write(fd, buffer, size);
    if (res < 0) {
        return false;
    }
    return (size_t) res == size;
}

bool AvailableReadSocket(int fd, int size) {
    // https://stackoverflow.com/questions/14047802/how-to-check-amount-of-data-available-for-a-socket-in-c-and-linux
    int count = 0;
    if (ioctl(fd, FIONREAD, &count) < 0) {
        return false;
    }
    return count >= size;
}

int ReadSocket(int fd) {
    uint8_t data[4];
    auto res = read(fd, data, 1);
    if (res <= 0) {
        return -1;
    }
    return data[0];
}

bool ReadSocket(int fd, uint8_t *buffer, size_t size) {
    // https://man7.org/linux/man-pages/man2/read.2.html
    off_t offset = 0;
    while (size > 0) {
        auto res = read(fd, buffer + offset, size);
        if (res <= 0) {
            return false;
        }
        size -= res;
        offset += res;
    }
    return true;
}

void CloseSocket(int fd) {
    close(fd);
}

std::string GetEnv(std::string name) {
    char *pval = getenv(name.c_str());
    if (pval) {
        return std::string(pval);
    }
    return std::string("");
}

}

#endif
