#include "text_file_stream.h"
#include "hal.h"

namespace lisp {

bool TextFileStream::WriteChar(char c) {
    buffer.push_back(c);
    if (buffer.length() >= 4096) {
        return Flush();
    }
    return true;
}

bool TextFileStream::WriteString(const std::string &str) {
    for (char c : str) {
        if (!WriteChar(c)) {
            return false;
        }
    }
    return true;
}

bool TextFileStream::Flush() {
    auto success = hal::AppendTextFile(name, buffer);
    buffer.clear();
    return success;
}

bool TextFileStream::Close() {
    return Flush();
}

}
