#include <new>
#include "display_combinator.h"
#include "rational.h"
#include "display.h"
#include "string.h"
#include "pixel_maps.h"
#include "boolean.h"

namespace lisp {

bool IsPositiveInteger(Expression* expr) {
    return IsInteger(expr) && ((Rational*) expr)->GetNumerator() > 0;
}

bool GetRedGreenBlue(List* rgb, int &r, int &g, int &b) {
    auto elem1 = rgb->GetFirst();
    rgb = rgb->GetRest();
    if (rgb && IsInteger(elem1)) {
        auto elem2 = rgb->GetFirst();
        rgb = rgb->GetRest();
        if (rgb && IsInteger(elem2)) {
            auto elem3 = rgb->GetFirst();
            if (IsInteger(elem3)) {
                auto red = (Rational*) elem1;
                auto green = (Rational*) elem2;
                auto blue = (Rational*) elem3;
                r = red->GetNumerator();
                g = green->GetNumerator();
                b = blue->GetNumerator();
                return true;
            }
        }
    }
    return false;
}

PixelMap* CreateMonochromePixelMap(Expression* expr) {
    if (IsList(expr)) {
        auto args = (List*) expr;
        Expression* w = args->GetFirst();
        args = args->GetRest();
        if (args) {
            Expression* h = args->GetFirst();
            if (IsPositiveInteger(w) && IsPositiveInteger(h)) {
                return new(std::nothrow) MonochromePixelMap(((Rational*) w)->GetNumerator(), ((Rational*) h)->GetNumerator());
            }
        }
    }
    return nullptr;
}

PixelMap* CreateBounds() {
    return new(std::nothrow) Bounds();
}

bool CreateDisplay2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsSymbol(expr1)) {
        auto display_type = (Symbol*) expr1;
        PixelMap* pixels = nullptr;
        if (display_type->HasName("monochrome")) {
            pixels = CreateMonochromePixelMap(expr2);
        } else if (display_type->HasName("bounds")) {
            pixels = CreateBounds();
        } else if (display_type->HasName("ili9341")) {
            pixels = ili9341::CreatePixelMap(expr2);
        } else if (display_type->HasName("jd79653")) {
            pixels = jd79653::CreatePixelMap(expr2);
        }
        if (pixels) {
            pixels->Init();
            auto display = new(std::nothrow) Display(pixels);
            if (display) {
                auto success = Push(display, sstack);
                Delete(display);
                return success;
            }
            delete pixels;
        }
    }
    return false;
}

bool GetBounds1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr)) {
        auto display = (Display*) expr;
        int x, y;
        display->GetOrigin(x, y);
        auto origin_x = new(std::nothrow) Rational(x, 1l);
        auto origin_y = new(std::nothrow) Rational(y, 1l);
        auto width = new(std::nothrow) Rational(display->GetWidth(), 1l);
        auto height = new(std::nothrow) Rational(display->GetHeight(), 1l);
        auto success = false;
        if (origin_x && origin_y && width && height) {
            List* bounds = nullptr;
            success = Push(height, bounds) && Push(width, bounds) && Push(origin_y, bounds) && Push(origin_x, bounds) && Push(bounds, sstack);
            Delete(bounds);
        }
        Delete(origin_x);
        Delete(origin_y);
        Delete(width);
        Delete(height);
        return success;
    }
    return false;
}

bool Fill2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsList(expr2)) {
        auto display = (Display*) expr1;
        auto rgb = (List*) expr2;
        int red, green, blue;
        if (GetRedGreenBlue(rgb, red, green, blue)) {
            if (display->Fill(red, green, blue)) {
                return PushTrue(sstack);
            }
            return PushFalse(sstack);
        }
    }
    return false;
}

bool GetPixel3::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsInteger(expr2) && IsInteger(expr3)) {
        auto display = (Display*) expr1;
        auto x = (Rational*) expr2;
        auto y = (Rational*) expr3;
        int r, g, b;
        if (display->GetPixel(x->GetNumerator(), y->GetNumerator(), r, g, b)) {
            auto success = false;
            auto red = new(std::nothrow) Rational(r, 1l);
            auto green = new(std::nothrow) Rational(g, 1l);
            auto blue = new(std::nothrow) Rational(b, 1l);
            if (red && green && blue) {
                List* list = nullptr;
                success = Push(blue, list) && Push(green, list) && Push(red, list) & Push(list, sstack);
                Delete(list);
            }
            Delete(red);
            Delete(green);
            Delete(blue);
            return success;
        }
        return PushFalse(sstack);
    }
    return false;
}

bool SetPixel4::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Expression* expr4, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsInteger(expr2) && IsInteger(expr3) && IsList(expr4)) {
        auto display = (Display*) expr1;
        auto x = (Rational*) expr2;
        auto y = (Rational*) expr3;
        auto rgb = (List*) expr4;
        int red, green, blue;
        if (GetRedGreenBlue(rgb, red, green, blue)) {
            if (display->SetPixel(x->GetNumerator(), y->GetNumerator(), red, green, blue)) {
                return PushTrue(sstack);
            }
            return PushFalse(sstack);
        }
    }
    return false;
}

bool IsPenDown1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr)) {
        auto display = (Display*) expr;
        if (display->IsPenDown()) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool IsPenUp1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr)) {
        auto display = (Display*) expr;
        if (display->IsPenUp()) {
            return PushTrue(sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool PenDown1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr)) {
        auto display = (Display*) expr;
        display->PenDown();
        return Push(display, sstack);
    }
    return false;
}

bool PenUp1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr)) {
        auto display = (Display*) expr;
        display->PenUp();
        return Push(display, sstack);
    }
    return false;
}

bool SetPenColor2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsList(expr2)) {
        auto display = (Display*) expr1;
        auto rgb = (List*) expr2;
        int red, green, blue;
        if (GetRedGreenBlue(rgb, red, green, blue)) {
            display->SetPenColor(red, green, blue);
            return Push(display, sstack);
        }
    }
    return false;
}

bool SetHeading2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsNumber(expr2)) {
        auto display = (Display*) expr1;
        auto heading = (Number*) expr2;
        display->SetHeading(heading->GetDouble());
        return Push(display, sstack);
    }
    return false;
}

bool Towards3::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsInteger(expr2) && IsInteger(expr3)) {
        auto display = (Display*) expr1;
        auto x = (Rational*) expr2;
        auto y = (Rational*) expr3;
        if (display->Towards(x->GetNumerator(), y->GetNumerator())) {
            return Push(display, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Left2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsNumber(expr2)) {
        auto display = (Display*) expr1;
        auto angle = (Number*) expr2;
        display->Left(angle->GetDouble());
        return Push(display, sstack);
    }
    return false;
}

bool Right2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsNumber(expr2)) {
        auto display = (Display*) expr1;
        auto angle = (Number*) expr2;
        display->Right(angle->GetDouble());
        return Push(display, sstack);
    }
    return false;
}

bool SetPosition3::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsInteger(expr2) && IsInteger(expr3)) {
        auto display = (Display*) expr1;
        auto x = (Rational*) expr2;
        auto y = (Rational*) expr3;
        if (display->SetPosition(x->GetNumerator(), y->GetNumerator())) {
            return Push(display, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Forward2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsNumber(expr2)) {
        auto display = (Display*) expr1;
        auto distance = (Number*) expr2;
        if (display->Forward(distance->GetDouble())) {
            return Push(display, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Back2::ProcessValues(Expression* expr1, Expression* expr2, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsNumber(expr2)) {
        auto display = (Display*) expr1;
        auto distance = (Number*) expr2;
        if (display->Back(distance->GetDouble())) {
            return Push(display, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Label3::ProcessValues(Expression* expr1, Expression* expr2, Expression* expr3, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr1) && IsString(expr2) && IsInteger(expr3)) {
        auto display = (Display*) expr1;
        auto label = (String*) expr2;
        auto fontsize = (Rational*) expr3;
        if (display->Label(label->GetString(), fontsize->GetNumerator())) {
            return Push(display, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

bool Flush1::ProcessValue(Expression* expr, Action* &cstack, List* &sstack, Environment* env) {
    if (IsDisplay(expr)) {
        auto display = (Display*) expr;
        if (display->Flush()) {
            return Push(display, sstack);
        }
        return PushFalse(sstack);
    }
    return false;
}

}
