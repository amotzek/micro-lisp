#ifndef LIST_H
#define LIST_H
#include "expression.h"

namespace lisp {

class List : public Expression {
    public:
        List(Expression* f, List* r) { first = Copy(f); rest = Copy(r); }
        virtual ~List();
        virtual ExpressionType GetType() { return ExpressionType::LIST; }
        void SetFirst(Expression* f) { Delete(first); first = Copy(f); }
        void SetRest(List* r) { auto s = rest; rest = Copy(r); Delete(s); }
        Expression* GetFirst() { return first; }
        List* GetRest() { return rest; }
    private:
        Expression* first;
        List* rest;
};

bool Cons(Expression* first, List* rest, List* &list);

bool Pair(Expression* first, Expression* second, List* &list);

bool Push(Expression* first, List* &sstack);

Expression* Pop(List* &sstack);

bool Reverse(List* list, List* &reversed_list);

int Length(List* list);

}

#endif
